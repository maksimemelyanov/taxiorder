using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderCore.Application.Contracts;

public class AutoBrandDto : IEntity<int>
{
    public int Id { get; set; }
    
    public string Name { get; set; } = null!;
}