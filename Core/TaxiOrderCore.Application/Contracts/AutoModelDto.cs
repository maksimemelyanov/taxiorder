using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderCore.Application.Contracts;

public class AutoModelDto : IEntity<int>
{
    public int Id { get; set; }
    
    public int AutoBrandId { get; set; }
    
    public string Name { get; set; } = null!;
}