using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderCore.Application.Contracts;

public class ColorDto : IEntity<int>
{
    public int Id { get; set; }
    
    public string Name { get; set; } = null!;
    
    public string HexValue { get; set; } = null!;
}