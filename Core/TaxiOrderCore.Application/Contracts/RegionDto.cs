﻿using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderCore.Application.Contracts;

public class RegionDto : IEntity<int>
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;
}