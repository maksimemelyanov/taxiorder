﻿using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderCore.Application.Contracts;

public class TariffTypeDto : IEntity<int>
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public int AutoCategoryId { get; set; }
}