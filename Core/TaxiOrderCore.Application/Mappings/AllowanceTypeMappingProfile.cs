﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderCore.Application.Mappings;

public class AllowanceTypeMappingProfile : Profile
{
    public AllowanceTypeMappingProfile()
    {
        CreateMap<AllowanceTypeDto, AllowanceType>();
        CreateMap<AllowanceType, AllowanceTypeDto>();
    }
}