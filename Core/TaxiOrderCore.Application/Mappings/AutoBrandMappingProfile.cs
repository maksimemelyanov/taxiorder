using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderCore.Application.Mappings;

public class AutoBrandMappingProfile : Profile
{
    public AutoBrandMappingProfile()
    {
        CreateMap<AutoBrandDto, AutoBrand>();
        CreateMap<AutoBrand, AutoBrandDto>();
    }
}