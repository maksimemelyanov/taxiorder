﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderCore.Application.Mappings;

public class AutoCategoryMappingProfile : Profile
{
    public AutoCategoryMappingProfile()
    {
        CreateMap<AutoCategoryDto, AutoCategory>();
        CreateMap<AutoCategory, AutoCategoryDto>();
    }
}