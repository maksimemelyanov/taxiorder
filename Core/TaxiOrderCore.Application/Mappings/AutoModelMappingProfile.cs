using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderCore.Application.Mappings;

public class AutoModelMappingProfile : Profile
{
    public AutoModelMappingProfile()
    {
        CreateMap<AutoModelDto, AutoModel>();
        CreateMap<AutoModel, AutoModelDto>();
    }
}