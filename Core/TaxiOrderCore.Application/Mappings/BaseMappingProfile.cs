﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderCore.Application.Mappings;

public class BaseMappingProfile : Profile
{
    public BaseMappingProfile()
    {
        CreateMap<BaseDto, Base>();
        CreateMap<Base, BaseDto>();
    }
}