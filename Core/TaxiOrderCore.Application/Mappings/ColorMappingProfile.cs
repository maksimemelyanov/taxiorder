using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderCore.Application.Mappings;

public class ColorMappingProfile : Profile
{
    public ColorMappingProfile()
    {
        CreateMap<ColorDto, Color>();
        CreateMap<Color, ColorDto>();
    }
}