﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderCore.Application.Mappings;

public class DriverTariffTypeMappingProfile : Profile
{
    public DriverTariffTypeMappingProfile()
    {
        CreateMap<DriverTariffTypeDto, DriverTariffType>();
        CreateMap<DriverTariffType, DriverTariffTypeDto>();
    }
}