﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderCore.Application.Mappings
{
    public class OrderStatusMappingProfile : Profile
    {
        public OrderStatusMappingProfile()
        {
            CreateMap<OrderStatusDto, OrderStatus>();
            CreateMap<OrderStatus, OrderStatusDto>();
        }
    }
}
