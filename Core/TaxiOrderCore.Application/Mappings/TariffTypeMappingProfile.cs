﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderCore.Application.Mappings;

public class TariffTypeMappingProfile : Profile
{
    public TariffTypeMappingProfile()
    {
        CreateMap<TariffTypeDto, TariffType>();
        CreateMap<TariffType, TariffTypeDto>();
    }
}