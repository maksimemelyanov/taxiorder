using System.ComponentModel;

namespace TaxiOrderCore.Application;

public enum OrderStatus
{
    [Description("Новый")]
    NewOrder,
    [Description("Водитель назначен")]
    DriverAssigned,
    [Description("Водитель приехал")]
    DriverArrived,
    [Description("Клиент вышел")]
    ClientOut,
    [Description("Водитель начал исполнение")]
    StartedExecution,
    [Description("Водитель закончил заказ")]
    Finish
}