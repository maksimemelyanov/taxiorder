﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderCore.Application.Services.Implementations;

public class CrudReadService<TDto, TEntity, TPrimaryKey> : ICrudReadService<TDto, TPrimaryKey>
    where TDto : class
    where TEntity : IEntity<TPrimaryKey>
{
    private readonly IMapper _mapper;
    private readonly IReadRepository<TEntity, TPrimaryKey> _repository;
    private readonly ILogger<CrudReadService<TDto, TEntity, TPrimaryKey>> _logger;
    public CrudReadService(IMapper mapper, IReadRepository<TEntity, TPrimaryKey> repository,
        ILogger<CrudReadService<TDto, TEntity, TPrimaryKey>> logger)
    {
        _mapper = mapper;
        _repository = repository;
        _logger = logger;
    }

    public async Task<ICollection<TDto>> GetAll()
    {
        try
        {
            var entities = await _repository.GetAllAsync(CancellationToken.None, true);
            return _mapper.Map<ICollection<TEntity>, ICollection<TDto>>(entities);
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "Произошла ошибка при получении списка сущностей");
            throw;
        }
    }

    public async Task<TDto> GetById(TPrimaryKey id)
    {
        try
        {
            var entity = await _repository.GetAsync(id);
            return _mapper.Map<TDto>(entity);
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "Произошла ошибка при получении сущности {@id}", id);
            throw;
        }
    }
}