﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderCore.Application.Services.Implementations;

public class CrudService<TDto, TEntity, TPrimaryKey> : ICrudService<TDto, TPrimaryKey>
    where TDto : class
    where TEntity : IEntity<TPrimaryKey>
{
    private readonly IMapper _mapper;
    private readonly IRepository<TEntity, TPrimaryKey> _repository;
    private readonly ILogger<CrudService<TDto, TEntity, TPrimaryKey>> _logger;

    public CrudService(IMapper mapper, IRepository<TEntity, TPrimaryKey> repository,
        ILogger<CrudService<TDto, TEntity, TPrimaryKey>> logger)
    {
        _mapper = mapper;
        _repository = repository;
        _logger = logger;
    }

    public async Task<ICollection<TDto>> GetAll()
    {
        try
        {
            var entities = await _repository.GetAllAsync(CancellationToken.None, true);
            return _mapper.Map<ICollection<TEntity>, ICollection<TDto>>(entities);
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "Произошла ошибка при получении списка сущностей");
            throw;
        }
    }

    public async Task<TDto> GetById(TPrimaryKey id)
    {
        try
        {
            var entity = await _repository.GetAsync(id);
            return _mapper.Map<TDto>(entity);
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "Произошла ошибка при получении сущности {@id}", id);
            throw;
        }
    }

    public async Task<TPrimaryKey> Create(TDto dto)
    {
        try
        {
            var entity = _mapper.Map<TDto, TEntity>(dto);
            var result = await _repository.AddAsync(entity);
            await _repository.SaveChangesAsync();
            return result.Id;
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "Произошла ошибка при создании сущности {@dto}", dto);
            throw;
        }
    }

    public async Task Update(TPrimaryKey id, TDto dto)
    {
        try
        {
            var entity = _mapper.Map<TDto, TEntity>(dto);
            entity.Id = id;
            _repository.Update(entity);
            await _repository.SaveChangesAsync();
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "Произошла ошибка при изменении сущности {@id} {@dto}", id, dto);
            throw;
        }
    }

    public async Task Delete(TPrimaryKey id)
    {
        try
        {
            var result = _repository.Delete(id);
            if (result)
            {
                await _repository.SaveChangesAsync();
            }
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "Произошла ошибка при удалении сущности {@id}", id);
            throw;
        }
    }
}