﻿namespace TaxiOrderCore.Application.Services.Interfaces;

public interface ICrudReadService<TDto, in TPrimaryKey> where TDto : class
{
    public Task<ICollection<TDto>> GetAll();

    public Task<TDto> GetById(TPrimaryKey id);
}