﻿namespace TaxiOrderCore.Application.Services.Interfaces;

public interface ICrudService<TDto, TPrimaryKey> : ICrudReadService<TDto, TPrimaryKey> where TDto : class
{
    public Task<TPrimaryKey> Create(TDto dto);

    public Task Update(TPrimaryKey id, TDto dto);

    public Task Delete(TPrimaryKey id);
}