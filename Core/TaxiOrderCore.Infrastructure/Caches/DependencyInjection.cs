﻿using Microsoft.Extensions.DependencyInjection;
using TaxiOrderCore.Infrastructure.Caches.Interfaces;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderCore.Infrastructure.Caches;

public static class DependencyInjection
{
    public static IServiceCollection AddCache<TKey, TEntity, TImplementation>(this IServiceCollection serviceCollection)
        where TEntity : class, IEntity<TKey>
        where TImplementation : class, ICache<TEntity, TKey>, ICache
    {
        serviceCollection
            .AddSingleton<TImplementation>()
            .AddSingleton<ICache>(provider => provider.GetRequiredService<TImplementation>())
            .AddSingleton<ICache<TEntity, TKey>>(provider => provider.GetRequiredService<TImplementation>());

        return serviceCollection;
    }
}