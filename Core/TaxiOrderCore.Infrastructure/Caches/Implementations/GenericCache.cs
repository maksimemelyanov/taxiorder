﻿using System.Collections.Concurrent;
using TaxiOrderCore.Infrastructure.Caches.Interfaces;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderCore.Infrastructure.Caches.Implementations;

public abstract class GenericCache<TEntity, TPrimaryKey> : ICache<TEntity, TPrimaryKey>
    where TEntity : class, IEntity<TPrimaryKey> where TPrimaryKey : notnull
{
    protected const string InitCacheError = "Произошла ошибка при инициализации кэша";

    protected ConcurrentDictionary<TPrimaryKey, TEntity> Cache = new();

    protected GenericCache()
    {
    }

    public virtual async Task Init()
    {
        await Task.Delay(1);
    }

    public virtual TEntity? FindByKey(TPrimaryKey key)
    {
        Cache.TryGetValue(key, out var result);
        return result;
    }

    public virtual IEnumerable<TEntity> GetAll()
    {
        return Cache.Any()
            ? new List<TEntity>()
            : Cache.Values.ToList();
    }
}