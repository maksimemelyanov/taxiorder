﻿using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderCore.Infrastructure.Caches.Interfaces;

public interface ICache
{
    public Task Init();
}

public interface ICache<out TEntity, in TPrimaryKey> : ICache where TEntity : class, IEntity<TPrimaryKey>
{
    public TEntity? FindByKey(TPrimaryKey key);

    public IEnumerable<TEntity> GetAll();
}