using Microsoft.Extensions.DependencyInjection;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderCore.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructureCore(this IServiceCollection serviceCollection)
    {
        serviceCollection
            .AddTransient<IReadRepository<AllowanceType, int>, AllowanceTypeRepository>()
            .AddTransient<IReadRepository<AutoCategory, int>, AutoCategoryRepository>()
            .AddTransient<IReadRepository<Base, int>, BaseRepository>()
            .AddTransient<IReadRepository<DriverTariffType, int>, DriverTariffTypeRepository>()
            .AddTransient<IReadRepository<Region, int>, RegionRepository>()
            .AddTransient<IReadRepository<TariffType, int>, TariffTypeRepository>()
            .AddTransient<IReadRepository<OrderStatus, int>, OrderStatusRepository>()
            .AddTransient<IReadRepository<Color, int>, ColorRepository>()
            .AddTransient<IReadRepository<AutoModel, int>, AutoModelRepository>()
            .AddTransient<IReadRepository<AutoBrand, int>, AutoBrandRepository>()
            .AddTransient<IReadRepository<TariffType, int>, TariffTypeRepository>();

        return serviceCollection;
    }
}