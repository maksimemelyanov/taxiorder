﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TaxiOrderCore.Infrastructure.Entities;

[Table("auto_category", Schema = "main")]
public class AutoCategory : IEntity<int>
{
    [Column("id")]
    public int Id { get; set; }

    [Column("name")]
    public string Name { get; set; } = null!;
}