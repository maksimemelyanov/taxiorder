using System.ComponentModel.DataAnnotations.Schema;

namespace TaxiOrderCore.Infrastructure.Entities;

[Table("auto_model", Schema = "main")]
public class AutoModel : IEntity<int>
{
    [Column("id")]
    public int Id { get; set; }

    [Column("id_auto_brand")]
    public int AutoBrandId { get; set; }
    
    [Column("name")]
    public string Name { get; set; } = null!;
}