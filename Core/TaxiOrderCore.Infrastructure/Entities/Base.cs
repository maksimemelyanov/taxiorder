﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TaxiOrderCore.Infrastructure.Entities;

[Table("base", Schema = "main")]
public class Base : IEntity<int>
{
    [Column("id")]
    public int Id { get; set; }

    [Column("name")]
    public string Name { get; set; } = null!;

    [Column("id_region")]
    public int RegionId { get; set; }

    [Column("is_active")]
    public bool IsActive { get; set; }
}