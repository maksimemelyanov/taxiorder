using System.ComponentModel.DataAnnotations.Schema;

namespace TaxiOrderCore.Infrastructure.Entities;

[Table("color", Schema = "main")]
public class Color : IEntity<int>
{
    [Column("id")]
    public int Id { get; set; }

    [Column("name")]
    public string Name { get; set; } = null!;

    [Column("hex_value")] 
    public string HexValue { get; set; } = null!;
}