namespace TaxiOrderCore.Infrastructure.Entities;

/// <summary>
///     Интерфейс сущности с идентификатором
/// </summary>
/// <typeparam name="TPrimaryKey">Тип первичного ключа</typeparam>
public interface IEntity<TPrimaryKey>
{
    /// <summary>
    ///     Идентификатор
    /// </summary>
    TPrimaryKey Id { get; set; }
}