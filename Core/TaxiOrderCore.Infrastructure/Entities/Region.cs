﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TaxiOrderCore.Infrastructure.Entities;

[Table("region", Schema = "main")]
public class Region : IEntity<int>
{
    [Column("id")]
    public int Id { get; set; }

    [Column("name")]
    public string Name { get; set; } = null!;
}