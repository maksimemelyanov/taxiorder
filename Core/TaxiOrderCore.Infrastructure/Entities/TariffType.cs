﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TaxiOrderCore.Infrastructure.Entities;

[Table("tariff_type", Schema = "main")]
public class TariffType : IEntity<int>
{
    [Column("id")]
    public int Id { get; set; }

    [Column("name")]
    public string Name { get; set; } = null!;

    [Column("id_auto_category")]
    public int AutoCategoryId { get; set; }
}