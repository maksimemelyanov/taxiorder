﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderCore.Infrastructure.Repositories.Implementations;

public class AllowanceTypeRepository : ReadRepository<AllowanceType, int>, IReadRepository<AllowanceType, int>
{
    public AllowanceTypeRepository(DbContext context) : base(context)
    {
    }
}