using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderCore.Infrastructure.Repositories.Implementations;

public class AutoBrandRepository : ReadRepository<AutoBrand, int>, IReadRepository<AutoBrand, int>
{
    public AutoBrandRepository(DbContext context) : base(context)
    {
    }
}