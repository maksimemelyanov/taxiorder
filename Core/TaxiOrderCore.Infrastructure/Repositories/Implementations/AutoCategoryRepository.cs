﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderCore.Infrastructure.Repositories.Implementations;

public class AutoCategoryRepository : ReadRepository<AutoCategory, int>, IReadRepository<AutoCategory, int>
{
    public AutoCategoryRepository(DbContext context) : base(context)
    {
    }
}