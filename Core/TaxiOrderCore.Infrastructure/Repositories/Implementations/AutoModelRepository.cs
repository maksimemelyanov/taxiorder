using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderCore.Infrastructure.Repositories.Implementations;

public class AutoModelRepository : ReadRepository<AutoModel, int>, IReadRepository<AutoModel, int>
{
    public AutoModelRepository(DbContext context) : base(context)
    {
    }
}