﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderCore.Infrastructure.Repositories.Implementations;

public class BaseRepository : ReadRepository<Base, int>, IReadRepository<Base, int>
{
    public BaseRepository(DbContext context) : base(context)
    {
    }
}