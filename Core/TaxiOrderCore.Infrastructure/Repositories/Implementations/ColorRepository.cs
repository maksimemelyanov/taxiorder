using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderCore.Infrastructure.Repositories.Implementations;

public class ColorRepository : ReadRepository<Color, int>, IReadRepository<Color, int>
{
    public ColorRepository(DbContext context) : base(context)
    {
    }
}