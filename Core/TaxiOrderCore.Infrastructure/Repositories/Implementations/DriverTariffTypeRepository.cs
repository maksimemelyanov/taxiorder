﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderCore.Infrastructure.Repositories.Implementations;

public class DriverTariffTypeRepository : ReadRepository<DriverTariffType, int>, IReadRepository<DriverTariffType, int>
{
    public DriverTariffTypeRepository(DbContext context) : base(context)
    {
    }
}