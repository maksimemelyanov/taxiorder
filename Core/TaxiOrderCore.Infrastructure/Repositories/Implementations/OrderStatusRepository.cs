﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderCore.Infrastructure.Repositories.Implementations
{
    public class OrderStatusRepository : ReadRepository<OrderStatus, int>, IReadRepository<OrderStatus, int>
    {
        public OrderStatusRepository(DbContext context) : base(context)
        {
        }
    }
}
