﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderCore.Infrastructure.Repositories.Implementations;

/// <summary>
///     Репозиторий для чтения
/// </summary>
/// <typeparam name="T">Тип сущности</typeparam>
/// <typeparam name="TPrimaryKey">Тип первичного ключа</typeparam>
public abstract class ReadRepository<T, TPrimaryKey> : IReadRepository<T, TPrimaryKey>
    where T : class, IEntity<TPrimaryKey>
{
    /// <summary>
    ///     Контекст
    /// </summary>
    protected readonly DbContext Context;

    /// <summary>
    ///     Набор данных
    /// </summary>
    protected DbSet<T> EntitySet;

    /// <summary>
    ///     Конструктор
    /// </summary>
    /// <param name="context">Контекст</param>
    protected ReadRepository(DbContext context)
    {
        Context = context;
        EntitySet = Context.Set<T>();
    }

    /// <summary>
    ///     Запросить все сущности в базе
    /// </summary>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <param name="asNoTracking">Вызвать с AsNoTracking</param>
    /// <returns>Список сущностей</returns>
    public async Task<ICollection<T>> GetAllAsync(CancellationToken cancellationToken, bool asNoTracking = false)
    {
        return await (asNoTracking ? EntitySet.AsNoTracking() : EntitySet).ToListAsync(cancellationToken);
    }

    /// <summary>
    ///     Получить сущность по ID
    /// </summary>
    /// <param name="id">ID сущности</param>
    /// <returns>сущность</returns>
    public virtual async Task<T?> GetAsync(TPrimaryKey id)
    {
        return await EntitySet.FindAsync(id);
    }
}