﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderCore.Infrastructure.Repositories.Implementations;

public class RegionRepository : ReadRepository<Region, int>, IReadRepository<Region, int>
{
    public RegionRepository(DbContext context) : base(context)
    {
    }
}