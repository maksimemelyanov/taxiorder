﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderCore.Infrastructure.Repositories.Implementations;

public class TariffTypeRepository : ReadRepository<TariffType, int>, IReadRepository<TariffType, int>
{
    public TariffTypeRepository(DbContext context) : base(context)
    {
    }
}