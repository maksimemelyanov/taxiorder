﻿using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderCore.Infrastructure.Repositories.Interfaces;

/// <summary>
///     Интерфейс репозитория, предназначенного для чтения
/// </summary>
/// <typeparam name="T">Тип Entity для репозитория</typeparam>
/// <typeparam name="TPrimaryKey">Тип первичного ключа</typeparam>
public interface IReadRepository<T, in TPrimaryKey> : IRepository
    where T : IEntity<TPrimaryKey>
{
    /// <summary>
    ///     Запросить все сущности в базе
    /// </summary>
    /// <param name="cancellationToken">Токен отмены</param>
    /// <param name="asNoTracking">Вызвать с AsNoTracking</param>
    /// <returns>Список сущностей</returns>
    Task<ICollection<T>> GetAllAsync(CancellationToken cancellationToken, bool asNoTracking = false);

    /// <summary>
    ///     Получить сущность по ID
    /// </summary>
    /// <param name="id">ID сущности</param>
    /// <returns>сущность</returns>
    Task<T?> GetAsync(TPrimaryKey id);
}