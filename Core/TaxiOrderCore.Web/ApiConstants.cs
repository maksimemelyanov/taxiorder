﻿namespace TaxiOrderCore.Web;

public static class ApiConstants
{
    public const string TariffId = "id-tariff";

    public const string DriverTariffId = "id-driver-tariff";

    public const string StartLatitude = "start-latitude";

    public const string StartLongitude = "start-longitude";

    public const string EndLatitude = "end-latitude";

    public const string EndLongitude = "end-longitude";

    public const string BaseId = "id-base";

    public const string AddressName = "address-name";

    public const string ClientId = "id-client";

    public const string StartAddress = "start-address";

    public const string EndAddress = "end-address";

    public const string AllowanceId = "id-allowance";

    public const string DriverId = "id-driver";

    public const string AutoId = "id-auto";
    
    public const string OrderId = "id-order";
    
    public const string Status = "status";
}