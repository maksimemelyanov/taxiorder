﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TaxiOrderCore.Application.Services.Interfaces;

namespace TaxiOrderCore.Web.Controllers;

[ApiController]
[Route("api/[controller]")]
public class GenericCrudController<TDto, TModel, TPrimaryKey> : ControllerBase
    where TDto : class
    where TModel : class
{
    private readonly IMapper _mapper;
    private readonly ICrudService<TDto, TPrimaryKey> _service;

    public GenericCrudController(ICrudService<TDto, TPrimaryKey> service, IMapper mapper)
    {
        _service = service;
        _mapper = mapper;
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Get(TPrimaryKey id)
    {
        return Ok(_mapper.Map<TModel>(await _service.GetById(id)));
    }

    [HttpPost]
    public async Task<IActionResult> Add(TModel model)
    {
        return Ok(await _service.Create(_mapper.Map<TDto>(model)));
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> Edit(TPrimaryKey id, TModel model)
    {
        await _service.Update(id, _mapper.Map<TDto>(model));
        return Ok();
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> Delete(TPrimaryKey id)
    {
        await _service.Delete(id);
        return Ok();
    }

    [HttpGet("list")]
    public async Task<IActionResult> GetList()
    {
        return Ok(_mapper.Map<ICollection<TModel>>(await _service.GetAll()));
    }
}