﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TaxiOrderCore.Application.Services.Interfaces;

namespace TaxiOrderCore.Web.Controllers;

[ApiController]
[Route("api/[controller]")]
public class GenericCrudReadController<TDto, TModel, TPrimaryKey> : ControllerBase
    where TDto : class
    where TModel : class
{
    private readonly IMapper _mapper;
    private readonly ICrudReadService<TDto, TPrimaryKey> _service;

    public GenericCrudReadController(ICrudReadService<TDto, TPrimaryKey> service, IMapper mapper)
    {
        _service = service;
        _mapper = mapper;
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> Get(TPrimaryKey id)
    {
        return Ok(_mapper.Map<TModel>(await _service.GetById(id)));
    }

    [HttpGet("list")]
    public async Task<IActionResult> GetList()
    {
        return Ok(_mapper.Map<ICollection<TModel>>(await _service.GetAll()));
    }
}