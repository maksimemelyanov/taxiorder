﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderCore.Web.Mapping;

public class AllowanceTypeMappingProfile : Profile
{
    public AllowanceTypeMappingProfile()
    {
        CreateMap<AllowanceTypeDto, AllowanceTypeModel>();
        CreateMap<AllowanceTypeModel, AllowanceTypeDto>();
    }
}