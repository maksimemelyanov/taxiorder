using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderCore.Web.Mapping;

public class AutoBrandMappingProfile : Profile
{
    public AutoBrandMappingProfile()
    {
        CreateMap<AutoBrandDto, AutoBrandModel>();
        CreateMap<AutoBrandModel, AutoBrandDto>();
    }
}