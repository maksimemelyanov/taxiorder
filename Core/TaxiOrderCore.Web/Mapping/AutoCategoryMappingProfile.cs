﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderCore.Web.Mapping;

public class AutoCategoryMappingProfile : Profile
{
    public AutoCategoryMappingProfile()
    {
        CreateMap<AutoCategoryDto, AutoCategoryModel>();
        CreateMap<AutoCategoryModel, AutoCategoryDto>();
    }
}