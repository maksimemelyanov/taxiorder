using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderCore.Web.Mapping;

public class AutoModelMappingProfile : Profile
{
    public AutoModelMappingProfile()
    {
        CreateMap<AutoModelDto, AutoModelModel>();
        CreateMap<AutoModelModel, AutoModelDto>();
    }
}