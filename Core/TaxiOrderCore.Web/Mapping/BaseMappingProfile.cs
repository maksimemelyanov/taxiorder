﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderCore.Web.Mapping;

public class BaseMappingProfile : Profile
{
    public BaseMappingProfile()
    {
        CreateMap<BaseDto, BaseModel>();
        CreateMap<BaseModel, BaseDto>();
    }
}