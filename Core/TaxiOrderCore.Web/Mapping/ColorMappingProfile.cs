using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderCore.Web.Mapping;

public class ColorMappingProfile : Profile
{
    public ColorMappingProfile()
    {
        CreateMap<ColorDto, ColorModel>();
        CreateMap<ColorModel, ColorDto>();
    }
}