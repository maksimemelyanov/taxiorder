﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderCore.Web.Mapping;

public class DriverTariffTypeMappingProfile : Profile
{
    public DriverTariffTypeMappingProfile()
    {
        CreateMap<DriverTariffTypeDto, DriverTariffTypeModel>();
        CreateMap<DriverTariffTypeModel, DriverTariffTypeDto>();
    }
}