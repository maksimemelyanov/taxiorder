﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderCore.Web.Mapping;

public class RegionMappingProfile : Profile
{
    public RegionMappingProfile()
    {
        CreateMap<RegionDto, RegionModel>();
        CreateMap<RegionModel, RegionDto>();
    }
}