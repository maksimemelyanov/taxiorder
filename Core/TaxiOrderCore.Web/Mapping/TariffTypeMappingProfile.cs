﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderCore.Web.Mapping;

public class TariffTypeMappingProfile : Profile
{
    public TariffTypeMappingProfile()
    {
        CreateMap<TariffTypeDto, TariffTypeModel>();
        CreateMap<TariffTypeModel, TariffTypeDto>();
    }
}