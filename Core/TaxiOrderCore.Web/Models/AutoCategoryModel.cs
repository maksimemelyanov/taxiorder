﻿namespace TaxiOrderCore.Web.Models;

public class AutoCategoryModel
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;
}