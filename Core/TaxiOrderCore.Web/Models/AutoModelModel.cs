namespace TaxiOrderCore.Web.Models;

public class AutoModelModel
{
    public int Id { get; set; }
    
    public int AutoBrandId { get; set; }
    
    public string Name { get; set; } = null!;
}