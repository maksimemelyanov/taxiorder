﻿namespace TaxiOrderCore.Web.Models;

public class BaseModel
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public int RegionId { get; set; }

    public bool IsActive { get; set; }
}