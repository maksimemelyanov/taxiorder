﻿namespace TaxiOrderCore.Web.Models
{
    public class OrderStatusModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
