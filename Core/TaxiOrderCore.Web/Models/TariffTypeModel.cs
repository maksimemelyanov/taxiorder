﻿namespace TaxiOrderCore.Web.Models;

public class TariffTypeModel
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public int AutoCategoryId { get; set; }
}