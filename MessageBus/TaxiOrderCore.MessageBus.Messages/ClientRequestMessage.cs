﻿using TaxiOrderCore.Application;

namespace TaxiOrderCore.MessageBus.Messages
{
    public class ClientRequestMessage : Message
    {
        public int OrderId { get; set; }

        public string DriverInfo { get; set; } = null!;

        public OrderStatus Status { get; set; } = OrderStatus.NewOrder!;
    }
}
