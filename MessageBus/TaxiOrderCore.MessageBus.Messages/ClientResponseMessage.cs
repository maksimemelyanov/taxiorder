﻿using TaxiOrderCore.Application;

namespace TaxiOrderCore.MessageBus.Messages
{
    public class ClientResponseMessage : Message
    {
        public int OrderId { get; set; }

        public OrderStatus Status { get; set; } = OrderStatus.NewOrder!;

        public string Info { get; set; }
    }
}
