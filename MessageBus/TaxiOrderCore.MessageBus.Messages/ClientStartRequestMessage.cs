﻿namespace TaxiOrderCore.MessageBus.Messages
{
    public class ClientStartRequestMessage : Message
    {
        public int ClientId { get; set; }

        public string StartAddress { get; set; }

        public string EndAddress { get; set; }

        public int TariffId { get; set; }

        public double ClientPrice { get; set; }

        public double DriverPrice { get; set; }

        public int? AllowanceId { get; set; }
        public int BaseId { get; set; }
    }
}
