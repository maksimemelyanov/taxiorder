﻿using TaxiOrderCore.Application;

namespace TaxiOrderCore.MessageBus.Messages;

public class FindDriverRequestMessage : Message
{
    public int OrderId { get; set; }

    public OrderStatus Status { get; set; }
    
    public int BaseId { get; set; }
}