﻿using TaxiOrderCore.Application;

namespace TaxiOrderCore.MessageBus.Messages;

public class FindDriverResponseMessage : Message
{
    public int OrderId { get; set; }

    public OrderStatus Status { get; set; }

    public int DriverBaseAutoId { get; set; }

    public int DriverTariffId { get; set; }

    public string DriverInfo { get; set; } = null!;
}