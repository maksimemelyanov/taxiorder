﻿using Microsoft.Extensions.Logging;
using TaxiOrderCore.MessageBus.Bus;
using TaxiOrderCore.MessageBus.Messages;
using TaxiOrderCore.MessageBus.RabbitMQ.Connection;
using TaxiOrderCore.MessageBus.Subscriptions;
using Polly;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using System;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace TaxiOrderCore.MessageBus.RabbitMQ.Bus
{
	public class RabbitMQMessageBus : IMessageBus
	{
		private readonly string _exchangeName;
		private readonly string _queueName;
		private readonly int _publishRetryCount = 5;
		private readonly TimeSpan _subscribeRetryTime = TimeSpan.FromSeconds(5);

		private readonly IPersistentConnection _persistentConnection;
		private readonly IMessageBusSubscriptionManager _subscriptionsManager;
		private readonly IServiceProvider _serviceProvider;

		private readonly ILogger<RabbitMQMessageBus> _logger;

		private IModel _consumerChannel;

		public RabbitMQMessageBus(
			IPersistentConnection persistentConnection,
			IMessageBusSubscriptionManager subscriptionsManager,
			IServiceProvider serviceProvider,
			ILogger<RabbitMQMessageBus> logger,
			string brokerName,
			string queueName)
		{
			_persistentConnection = persistentConnection ?? throw new ArgumentNullException(nameof(persistentConnection));
			_subscriptionsManager = subscriptionsManager ?? throw new ArgumentNullException(nameof(subscriptionsManager));
			_serviceProvider = serviceProvider;
			_logger = logger;
			_exchangeName = brokerName ?? throw new ArgumentNullException(nameof(brokerName));
			_queueName = queueName ?? throw new ArgumentNullException(nameof(queueName));

			ConfigureMessageBroker();
		}

		public void Publish<TMessage>(TMessage message)
			where TMessage : Message
		{
			if (!_persistentConnection.IsConnected)
			{
				_persistentConnection.TryConnect();
			}

			var policy = Policy
				.Handle<BrokerUnreachableException>()
				.Or<SocketException>()
				.WaitAndRetry(_publishRetryCount, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (exception, timeSpan) =>
				{
					_logger.LogWarning(exception, "Could not publish event #{MessageId} after {Timeout} seconds: {ExceptionMessage}.", message.Id, $"{timeSpan.TotalSeconds:n1}", exception.Message);
				});

			var messageName = message.GetType().Name;

			_logger.LogTrace("Creating RabbitMQ channel to publish message #{MessageId} ({MessageName})...", message.Id, messageName);

			using (var channel = _persistentConnection.CreateModel())
			{
				_logger.LogTrace("Declaring RabbitMQ exchange to publish message #{MessageId}...", message.Id);

				channel.ExchangeDeclare(exchange: _exchangeName, type: "direct");

				var messageBody = JsonSerializer.Serialize(message);
				var messageBytes = Encoding.UTF8.GetBytes(messageBody);

				policy.Execute(() =>
				{
					var properties = channel.CreateBasicProperties();
					properties.DeliveryMode = (byte)DeliveryMode.Persistent;

                    LoggerExtensions.LogTrace(_logger, (string)"Publishing event to RabbitMQ with ID #{EventId}...", message.Id);

					channel.BasicPublish(
						exchange: _exchangeName,
						routingKey: messageName,
						mandatory: true,
						basicProperties: properties,
						body: (ReadOnlyMemory<byte>)messageBytes);

                    LoggerExtensions.LogTrace(_logger, (string)"Published event with ID #{EventId}.", message.Id);
				});
			}
		}

		public void Subscribe<TMessage, TMessageHandler>()
			where TMessage : Message
			where TMessageHandler : IMessageHandler<TMessage>
		{
			var messageName = _subscriptionsManager.GetMessageIdentifier<TMessage>();
			var messageHandlerName = typeof(TMessageHandler).Name;

			AddQueueBindForMessageSubscription(messageName);

			_logger.LogInformation("Subscribing to message {MessageName} with {MessageHandler}...", messageName, messageHandlerName);

			_subscriptionsManager.AddSubscription<TMessage, TMessageHandler>();
			StartBasicConsume();

			_logger.LogInformation("Subscribed to message {MessageName} with {MessageHandler}.", messageName, messageHandlerName);
		}

		public void Unsubscribe<TMessage, TMessageHandler>()
			where TMessage : Message
			where TMessageHandler : IMessageHandler<TMessage>
		{
			var messageName = _subscriptionsManager.GetMessageIdentifier<TMessage>();

			_logger.LogInformation("Unsubscribing from event {EventName}...", messageName);

			_subscriptionsManager.RemoveSubscription<TMessage, TMessageHandler>();

			_logger.LogInformation("Unsubscribed from message {MessageName}.", messageName);
		}

		private void ConfigureMessageBroker()
		{
			_consumerChannel = CreateConsumerChannel();
			_subscriptionsManager.OnMessageRemoved += SubscriptionManager_OnMessageRemoved;
			_persistentConnection.OnReconnectedAfterConnectionFailure += PersistentConnection_OnReconnectedAfterConnectionFailure;
		}

		private IModel CreateConsumerChannel()
		{
			if (!_persistentConnection.IsConnected)
			{
				_persistentConnection.TryConnect();
			}

			_logger.LogTrace("Creating RabbitMQ consumer channel...");

			var channel = _persistentConnection.CreateModel();

			channel.ExchangeDeclare(exchange: _exchangeName, type: "direct");
			channel.QueueDeclare
			(
				queue: _queueName,
				durable: true,
				exclusive: false,
				autoDelete: false,
				arguments: null
			);

			channel.CallbackException += (sender, ea) =>
			{
				_logger.LogWarning(ea.Exception, "Recreating RabbitMQ consumer channel...");
				DoCreateConsumerChannel();
			};

			_logger.LogTrace("Created RabbitMQ consumer channel.");


			return channel;
		}

		private void StartBasicConsume()
		{
			_logger.LogTrace("Starting RabbitMQ basic consume...");

			if (_consumerChannel == null)
			{
				_logger.LogError("Could not start basic consume because consumer channel is null.");
				return;
			}

			var consumer = new AsyncEventingBasicConsumer(_consumerChannel);
			consumer.Received += Consumer_Received;

			_consumerChannel.BasicConsume
			(
				queue: _queueName,
				autoAck: false,
				consumer: consumer
			);

			_logger.LogTrace("Started RabbitMQ basic consume.");
		}

		private async Task Consumer_Received(object sender, BasicDeliverEventArgs eventArgs)
		{
			var messageName = eventArgs.RoutingKey;
			var message = Encoding.UTF8.GetString(eventArgs.Body.Span);

			bool isAcknowledged = false;

			try
			{
				await ProcessMessage(messageName, message);

				_consumerChannel.BasicAck(eventArgs.DeliveryTag, multiple: false);
				isAcknowledged = true;
			}
			catch (Exception ex)
			{
				_logger.LogWarning(ex, "Error processing the following message: {Message}.", message);
			}
			finally
			{
				if (!isAcknowledged)
				{
					await TryEnqueueMessageAgainAsync(eventArgs);
				}
			}
		}

		private async Task TryEnqueueMessageAgainAsync(BasicDeliverEventArgs eventArgs)
		{
			try
			{
				_logger.LogWarning("Adding message to queue again with {Time} seconds delay...", $"{_subscribeRetryTime.TotalSeconds:n1}");

				await Task.Delay(_subscribeRetryTime);
				_consumerChannel.BasicNack(eventArgs.DeliveryTag, false, true);

				_logger.LogTrace("Message added to queue again.");
			}
			catch (Exception ex)
			{
				_logger.LogError("Could not enqueue message again: {Error}.", ex.Message);
			}
		}

		private async Task ProcessMessage(string messageName, string message)
		{
			_logger.LogTrace("Processing RabbitMQ message: {MessageName}...", messageName);

			if (!_subscriptionsManager.HasSubscriptionsForMessage(messageName))
			{
				_logger.LogTrace("There are no subscriptions for this message.");
				return;
			}

			var subscriptions = _subscriptionsManager.GetHandlersForMessage(messageName);
			foreach (var subscription in subscriptions)
			{
				var handler = _serviceProvider.GetService(subscription.HandlerType);
				if (handler == null)
				{
					_logger.LogWarning("There are no handlers for the following message: {MessageName}", messageName);
					continue;
				}

				var messageType = _subscriptionsManager.GetMessageTypeByName(messageName);

				var messageContent = JsonSerializer.Deserialize(message, messageType);
				var messageHandlerType = typeof(IMessageHandler<>).MakeGenericType(messageType);
				await Task.Yield();
                await (Task)messageHandlerType.GetMethod(nameof(IMessageHandler<Message>.HandleAsync)).Invoke(handler, new object[] { messageContent });
            }

			_logger.LogTrace("Processed event {EventName}.", messageName);
		}

		private void SubscriptionManager_OnMessageRemoved(object sender, string eventName)
		{
			if (!_persistentConnection.IsConnected)
			{
				_persistentConnection.TryConnect();
			}

			using (var channel = _persistentConnection.CreateModel())
			{
				channel.QueueUnbind(queue: _queueName, exchange: _exchangeName, routingKey: eventName);

				if (_subscriptionsManager.IsEmpty)
				{
					_consumerChannel.Close();
				}
			}
		}

		private void AddQueueBindForMessageSubscription(string messageName)
		{
			var containsKey = _subscriptionsManager.HasSubscriptionsForMessage(messageName);
			if (containsKey)
			{
				return;
			}

			if (!_persistentConnection.IsConnected)
			{
				_persistentConnection.TryConnect();
			}

			using (var channel = _persistentConnection.CreateModel())
			{
				channel.QueueBind(queue: _queueName, exchange: _exchangeName, routingKey: messageName);
			}
		}

		private void PersistentConnection_OnReconnectedAfterConnectionFailure(object sender, EventArgs e)
		{
			DoCreateConsumerChannel();
			RecreateSubscriptions();
		}

		private void DoCreateConsumerChannel()
		{
			_consumerChannel.Dispose();
			_consumerChannel = CreateConsumerChannel();
			StartBasicConsume();
		}

		private void RecreateSubscriptions()
		{
			var subscriptions = _subscriptionsManager.GetAllSubscriptions();
			_subscriptionsManager.Clear();

			Type eventBusType = this.GetType();
			MethodInfo genericSubscribe;

			foreach (var entry in subscriptions)
			{
				foreach (var subscription in entry.Value)
				{
					genericSubscribe = eventBusType.GetMethod( "Subscribe").MakeGenericMethod(subscription.MessageType, subscription.HandlerType);
					genericSubscribe.Invoke(this, null);
				}
			}
		}
	}
}
