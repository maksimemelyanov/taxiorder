﻿using RabbitMQ.Client;
using System;

namespace TaxiOrderCore.MessageBus.RabbitMQ.Connection
{
	public interface IPersistentConnection
	{
		event EventHandler OnReconnectedAfterConnectionFailure;
		bool IsConnected { get; }

		bool TryConnect();
		IModel CreateModel();
	}
}
