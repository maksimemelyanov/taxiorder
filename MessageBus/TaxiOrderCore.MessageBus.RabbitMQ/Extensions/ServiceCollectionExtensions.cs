﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TaxiOrderCore.MessageBus.Bus;
using TaxiOrderCore.MessageBus.RabbitMQ.Bus;
using TaxiOrderCore.MessageBus.RabbitMQ.Connection;
using TaxiOrderCore.MessageBus.Subscriptions;
using RabbitMQ.Client;
using System;

namespace TaxiOrderCore.MessageBus.RabbitMQ.Extensions
{
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Adds an event bus that uses RabbitMQ to deliver messages.
        /// </summary>
        /// <param name="services">Service collection.</param>
        /// <param name="host">URL to connect to RabbitMQ.</param>
        /// <param name="exchangeName">Broker name. This represents the exchange name.</param>
        /// <param name="queueName">Messa queue name, to track on RabbitMQ.</param>
        /// <param name="timeoutBeforeReconnecting">The amount of time in seconds the application will wait after trying to reconnect to RabbitMQ.</param>
        public static void AddRabbitMQMessageBus(this IServiceCollection services, string host, string exchangeName, string queueName, int timeoutBeforeReconnecting = 15)
        {
            services.AddSingleton<IMessageBusSubscriptionManager, InMemoryMessageBusSubscriptionManager>();
            services.AddSingleton<IPersistentConnection, RabbitMQPersistentConnection>(factory =>
            {
                var connectionFactory = new ConnectionFactory
                {
                    Uri = new Uri(host),
                    DispatchConsumersAsync = true,
                    AmqpUriSslProtocols = System.Security.Authentication.SslProtocols.None,
                    Ssl = new() { Enabled = false }
                };

                var logger = factory.GetService<ILogger<RabbitMQPersistentConnection>>();
                return new RabbitMQPersistentConnection(connectionFactory, logger, timeoutBeforeReconnecting);
            });

            services.AddSingleton<IMessageBus, RabbitMQMessageBus>(factory =>
            {
                var persistentConnection = factory.GetService<IPersistentConnection>();
                var subscriptionManager = factory.GetService<IMessageBusSubscriptionManager>();
                var logger = factory.GetService<ILogger<RabbitMQMessageBus>>();

                return new RabbitMQMessageBus(persistentConnection, subscriptionManager, factory, logger, exchangeName, queueName);
            });
        }
    }
}
