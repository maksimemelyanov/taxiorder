﻿using TaxiOrderCore.MessageBus.Messages;

namespace TaxiOrderCore.MessageBus.Bus
{
	public interface IMessageBus
	{
		void Publish<TMessage>(TMessage @message)
			where TMessage : Message;

		void Subscribe<TMessage, TMessageHandler>()
			where TMessage : Message
			where TMessageHandler : IMessageHandler<TMessage>;

		void Unsubscribe<TMessage, TMessageHandler>()
			where TMessage : Message
			where TMessageHandler : IMessageHandler<TMessage>;
	}
}
