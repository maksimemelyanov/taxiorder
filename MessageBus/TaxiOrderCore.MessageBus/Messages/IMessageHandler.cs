﻿namespace TaxiOrderCore.MessageBus.Messages
{
	public interface IMessageHandler<in TMesssage>
		where TMesssage : Message
	{
		Task HandleAsync(TMesssage @message);
	}
}
