﻿namespace TaxiOrderCore.MessageBus.Messages
{
	public abstract class Message
	{
		public Guid Id { get; set; } = Guid.NewGuid();
		public DateTime CreatedAt { get; set; } = DateTime.Now;
	}
}
