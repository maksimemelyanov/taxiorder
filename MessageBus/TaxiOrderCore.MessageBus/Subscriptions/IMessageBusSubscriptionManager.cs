﻿using TaxiOrderCore.MessageBus.Messages;

namespace TaxiOrderCore.MessageBus.Subscriptions
{
	public interface IMessageBusSubscriptionManager
	{
		event EventHandler<string> OnMessageRemoved;

		bool IsEmpty { get; }
		bool HasSubscriptionsForMessage(string messageName);

		string GetMessageIdentifier<TMessage>();
		Type GetMessageTypeByName(string messageName);
		IEnumerable<Subscription> GetHandlersForMessage(string messageName);
		Dictionary<string, List<Subscription>> GetAllSubscriptions();

		void AddSubscription<TMessage, TMessageHanler>()
			where TMessage : Message
			where TMessageHanler : IMessageHandler<TMessage>;

		void RemoveSubscription<TMessage, TMessageHandler>()
			where TMessage : Message
			where TMessageHandler : IMessageHandler<TMessage>;

		void Clear();
	}
}
