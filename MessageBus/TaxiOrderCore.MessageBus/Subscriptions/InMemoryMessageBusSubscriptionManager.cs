﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiOrderCore.MessageBus.Messages;

namespace TaxiOrderCore.MessageBus.Subscriptions
{
    public class InMemoryMessageBusSubscriptionManager : IMessageBusSubscriptionManager
    {
		private readonly Dictionary<string, List<Subscription>> _handlers = new Dictionary<string, List<Subscription>>();
		private readonly List<Type> _messageTypes = new List<Type>();

		public event EventHandler<string> OnMessageRemoved;

		public string GetMessageIdentifier<TMessage>() => typeof(TMessage).Name;

		public Type GetMessageTypeByName(string messageName) => _messageTypes.SingleOrDefault(t => t.Name == messageName);

		public IEnumerable<Subscription> GetHandlersForMessage(string messageName) => _handlers[messageName];

		public Dictionary<string, List<Subscription>> GetAllSubscriptions() => new Dictionary<string, List<Subscription>>(_handlers);

		public void AddSubscription<TMessage, TMessageHandler>()
			where TMessage : Message
			where TMessageHandler : IMessageHandler<TMessage>
		{
			var eventName = GetMessageIdentifier<TMessage>();

			DoAddSubscription(typeof(TMessage), typeof(TMessageHandler), eventName);

			if (!_messageTypes.Contains(typeof(TMessage)))
			{
				_messageTypes.Add(typeof(TMessage));
			}
		}

		public void RemoveSubscription<TMessage, TMessageHandler>()
			where TMessageHandler : IMessageHandler<TMessage>
			where TMessage : Message
		{
			var handlerToRemove = FindSubscriptionToRemove<TMessage, TMessageHandler>();
			var messageName = GetMessageIdentifier<TMessage>();
			DoRemoveHandler(messageName, handlerToRemove);
		}

		public void Clear()
		{
			_handlers.Clear();
			_messageTypes.Clear();
		}

		public bool IsEmpty => !_handlers.Keys.Any();

		public bool HasSubscriptionsForMessage(string messageName) => _handlers.ContainsKey(messageName);

		private void DoAddSubscription(Type messageType, Type handlerType, string messageName)
		{
			if (!HasSubscriptionsForMessage(messageName))
			{
				_handlers.Add(messageName, new List<Subscription>());
			}

			if (_handlers[messageName].Any(s => s.HandlerType == handlerType))
			{
				throw new ArgumentException($"Handler Type {handlerType.Name} already registered for '{messageName}'", nameof(handlerType));
			}

			_handlers[messageName].Add(new Subscription(messageType, handlerType));
		}

		private void DoRemoveHandler(string messageName, Subscription subscriptionToRemove)
		{
			if (subscriptionToRemove == null)
			{
				return;
			}

			_handlers[messageName].Remove(subscriptionToRemove);
			if (_handlers[messageName].Any())
			{
				return;
			}

			_handlers.Remove(messageName);
			var messageType = _messageTypes.SingleOrDefault(e => e.Name == messageName);
			if (messageType != null)
			{
				_messageTypes.Remove(messageType);
			}

			RaiseOnMessageRemoved(messageName);
		}

		private void RaiseOnMessageRemoved(string messageName)
		{
			var handler = OnMessageRemoved;
			handler?.Invoke(this, messageName);
		}

		private Subscription FindSubscriptionToRemove<TMessage, TMessageHandler>()
			 where TMessage : Message
			 where TMessageHandler : IMessageHandler<TMessage>
		{
			var messageName = GetMessageIdentifier<TMessage>();
			return DoFindSubscriptionToRemove(messageName, typeof(TMessageHandler));
		}

		private Subscription DoFindSubscriptionToRemove(string messageName, Type handlerType)
		{
			if (!HasSubscriptionsForMessage(messageName))
			{
				return null;
			}

			return _handlers[messageName].SingleOrDefault(s => s.HandlerType == handlerType);

		}
	}
}
