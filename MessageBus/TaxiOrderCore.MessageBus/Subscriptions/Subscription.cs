﻿namespace TaxiOrderCore.MessageBus.Subscriptions
{
	public class Subscription
	{
		public Type MessageType { get; private set; }
		public Type HandlerType { get; private set; }

		public Subscription(Type messageType, Type handlerType)
		{
			MessageType = messageType;
			HandlerType = handlerType;
		}
	}
}
