﻿using System.Collections.Concurrent;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TaxiOrderCore.Infrastructure.Caches.Implementations;
using TaxiOrderAddress.Infrastructure.Context;
using TaxiOrderAddress.Infrastructure.Entities;
using TaxiOrderAddress.Infrastructure.Repositories.Implementations;

namespace TaxiOrderAddress.Infrastructure.Caches.Implementations;

public class StreetTypeCache : GenericCache<StreetType, int>
{
    private readonly IServiceProvider _provider;
    private readonly ILogger<GenericCache<StreetType, int>> _logger;

    public StreetTypeCache(ILogger<GenericCache<StreetType, int>> logger, IServiceProvider provider)
    {
        _provider = provider;
        _logger = logger;
    }

    public override async Task Init()
    {
        try
        {
            var scope = _provider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            await using var context = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
            var repository = new StreetTypeReadRepository(context);
            var result = await repository.GetAllAsync(CancellationToken.None);
            Cache = new ConcurrentDictionary<int, StreetType>(result.ToDictionary(item => item.Id));
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, InitCacheError);
            Cache = new ConcurrentDictionary<int, StreetType>();
            throw;
        }
    }
}