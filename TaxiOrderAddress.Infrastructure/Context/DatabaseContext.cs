﻿using Microsoft.EntityFrameworkCore;

using TaxiOrderAddress.Infrastructure.Entities;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderAddress.Infrastructure.Context;

    public class DatabaseContext:DbContext
    {
    public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
    {
    }


    public DbSet<Base> Bases { get; set; }
    public DbSet<House> House { get; set; }
    public DbSet<Place> Place { get; set; }
    public DbSet<PlaceType> PlaceType { get; set; }
    public DbSet<Region> Regions { get; set; }
    public DbSet<ShortAddress> ShortAddress { get; set; }
    public DbSet<Street> Street { get; set; }
    public DbSet<StreetType> StreetType { get; set; }


    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
    }
}

