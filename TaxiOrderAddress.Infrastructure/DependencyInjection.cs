using Microsoft.Extensions.DependencyInjection;
using TaxiOrderCore.Infrastructure;
using TaxiOrderCore.Infrastructure.Caches;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderAddress.Infrastructure.Caches.Implementations;
using TaxiOrderAddress.Infrastructure.Entities;
using TaxiOrderAddress.Infrastructure.Repositories.Implementations;
using TaxiOrderAddress.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderAddress.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddInfrastructureCore();
        serviceCollection
            .AddTransient<IRepository<House, int>, HouseRepository>()
            .AddTransient<IRepository<Place, int>, PlaceRepository>()
            .AddTransient<IRepository<PlaceType, int>, PlaceTypeRepository>()
            .AddTransient<IRepository<ShortAddress, int>, ShortAddressRepository>()
            .AddTransient<IRepository<Street, int>, StreetRepository>()
            .AddTransient<IRepository<StreetType, int>, StreetTypeRepository>()
            .AddTransient<IReadRepository<House, int>, HouseReadRepository>()
            .AddTransient<IReadRepository<Place, int>, PlaceReadRepository>()
            .AddTransient<IReadRepository<PlaceType, int>, PlaceTypeReadRepository>()
            .AddTransient<IReadRepository<ShortAddress, int>, ShortAddressReadRepository>()
            .AddTransient<IReadRepository<Street, int>, StreetReadRepository>()
            .AddTransient<IReadRepository<StreetType, int>, StreetTypeReadRepository>()
            .AddTransient<IAddressRepository,AddressRepository>();
        
        serviceCollection.AddCache<int, House, HouseCache>();
        serviceCollection.AddCache<int, Place, PlaceCache>();
        serviceCollection.AddCache<int, PlaceType, PlaceTypeCache>();
        serviceCollection.AddCache<int, ShortAddress, ShortAddressCache>();
        serviceCollection.AddCache<int, Street, StreetCache>();
        serviceCollection.AddCache<int, StreetType, StreetTypeCache>();

        return serviceCollection;
    }
}