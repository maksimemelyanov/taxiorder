﻿using System.ComponentModel.DataAnnotations.Schema;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderAddress.Infrastructure.Entities;
[Table("house", Schema = "main")]
public class House : IEntity<int>
{
    [Column("id")]
    public int Id { get ; set ; }

    [Column("name")]
    public string Name { get; set; } = null!;

    [Column("id_street")]
    public int StreetId { get; set; }

    [Column("longitude")]
    public double Longitude { get; set; }

    [Column("latitude")]
    public double Latitude { get; set; }
}

