﻿using System.ComponentModel.DataAnnotations.Schema;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderAddress.Infrastructure.Entities;
[Table("place", Schema = "main")]
public class Place : IEntity<int>
{
    [Column("id")]
    public int Id { get; set; }
    [Column("name")]
    public string Name { get; set; } = null!;

    [Column("id_region")]
    public int RegionId { get; set; }

    [Column("id_place_type")]
    public int PlaceTypeId { get; set; }

    [Column("is_have_street")]
    public bool IsHaveStreet { get; set; }

    [Column("longitude")]
    public double Longitude { get; set; }

    [Column("latitude")]
    public double Latitude { get; set; }
}
