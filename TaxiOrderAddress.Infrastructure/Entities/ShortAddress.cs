﻿using System.ComponentModel.DataAnnotations.Schema;
using TaxiOrderCore.Infrastructure.Entities;


namespace TaxiOrderAddress.Infrastructure.Entities;
[Table("short_address", Schema = "main")]
public class ShortAddress : IEntity<int>
{
    [Column("id")]
    public int Id { get; set; }

    [Column("name")]
    public string Name { get; set; } = null!;

    [Column("id_place")]
    public int PlaceId { get; set; }

    [Column("longitude")]
    public double Longitude { get; set; }

    [Column("latitude")]
    public double Latitude { get; set; }
}
