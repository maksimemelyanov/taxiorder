﻿using System.ComponentModel.DataAnnotations.Schema;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderAddress.Infrastructure.Entities;

[Table("street", Schema = "main")]
public class Street : IEntity<int>
{
    [Column("id")]
    public int Id { get; set; }

    [Column("name")]
    public string Name { get; set; } = null!;

    [Column("id_place")]
    public int PlaceId { get; set; }

    [Column("id_street_type")]
    public int StreetTypeId { get; set; }
}
