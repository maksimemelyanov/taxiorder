﻿using System.ComponentModel.DataAnnotations.Schema;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderAddress.Infrastructure.Entities;
[Table("street_type", Schema = "main")]
public class StreetType : IEntity<int>
{
    [Column("id")]
    public int Id { get; set; }

    [Column("name")]
    public string Name { get; set; } = null!;
}
