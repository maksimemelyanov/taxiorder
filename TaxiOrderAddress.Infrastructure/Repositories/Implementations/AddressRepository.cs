﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiOrderAddress.Infrastructure.Entities;
using TaxiOrderAddress.Infrastructure.Repositories.Interfaces;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderAddress.Infrastructure.Repositories.Implementations
{
    public class AddressRepository: IAddressRepository
    {
        private DbContext _context;
        public AddressRepository(DbContext context) {
            _context = context;
        }

        public async Task<Region> GetRegion(int baseId)
        { 
            var baseTaxi = await _context.Set<Base>().FirstOrDefaultAsync(b=>b.Id == baseId);
            return await _context.Set<Region>().FirstOrDefaultAsync(p => p.Id == baseTaxi.RegionId);
        }

        public async Task<ICollection<Place>> GetPlacesAsync(int regionId)
        {
            return await _context.Set<Place>().Where(p => p.RegionId == regionId).ToArrayAsync();
        }

        public async Task<ICollection<Street>> GetStreetsAsync(string streetName, List<int> placeIds, int? streetTypeId)
        {
            List<Street> streets= new List<Street>();
            while (streetName != "" && streets.Count == 0)
            {
                var streetsContainsName = await _context.Set<Street>().Where(s => (streetTypeId == null || s.StreetTypeId == streetTypeId) 
                    && placeIds.Contains(s.PlaceId) && s.Name.ToLower().Contains(streetName)).ToListAsync();
                streets.AddRange(streetsContainsName);
                streetName = streetName.Remove(streetName.Length - 1);
            }
            return streets.Take(5).ToList();
        }

        public async Task<ICollection<House>> GetHousesAsync(string? houseName, List<int> streetIds)
        {
            List<House> houses = new List<House>();
            if (houseName != null)
            {
                while (houseName != "" && houses.Count < 5)
                {
                    var housesContainsName = await _context.Set<House>().Where(s => streetIds.Contains(s.StreetId) && s.Name.ToLower().Contains(houseName)).ToListAsync();
                    houses.AddRange(housesContainsName);
                    houseName = houseName.Remove(houseName.Length - 1);
                }
                
            }
            if (houses.Count == 0)
            {
                houses = await _context.Set<House>().Where(s => streetIds.Contains(s.StreetId)).ToListAsync();
            }
            return houses.Take(10).ToList();
        }

        public async Task<int?> GetStreetTypeByNameAsync(List<string> addressParts)
        {
            if (addressParts == null || addressParts.Count < 2)
            {
                return null;
            }

            var type = addressParts[0].Replace(".", "");
            var streetTypesInDB = await _context.Set<StreetType>().ToListAsync();
            var streetType = streetTypesInDB.Where(st => st.Name.StartsWith(type, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            if (streetType == null)
            {
                return null;
            }
            addressParts.RemoveAt(0);

            return streetType.Id;
        }

        public async Task<int?> GetPlaceTypeByNameAsync(List<string> addressParts)
        {
            if (addressParts == null || addressParts.Count < 2)
            {
                return null;
            }

            var type = addressParts[0].Replace(".","");
            var placeTypesInDB = await _context.Set<PlaceType>().ToListAsync();
            var placeType = placeTypesInDB.Where(pt => pt.Name.StartsWith(type, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            if (placeType == null) 
            {
                return null;
            }

            var streetTypesInDB = await _context.Set<StreetType>().ToListAsync();
            var streetType = streetTypesInDB.Where(pt => pt.Name.StartsWith(type, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            if (streetType != null)
            {
                if (addressParts.Count >= 4)
                {
                    addressParts.RemoveAt(0);
                    return placeType.Id;
                }
                else
                {
                    return null;
                }
            }
            addressParts.RemoveAt(0);
            return placeType.Id;
        }

        public async Task<int?> GetPlaceByNameAsync(List<string> addressParts, int regionId, int? placeTypeId) 
        {
            if (addressParts == null || placeTypeId == null)
            {
                return null;
            }
            var placesInDB = await _context.Set<Place>().Where(p=>p.RegionId == regionId && (placeTypeId == null || p.PlaceTypeId==placeTypeId)).ToListAsync();
            var places = new List<Place>();
            var placeName = addressParts[0];
            while (places.Count==0 && placeName.Length != 0)
            {
                places = placesInDB.Where(p=>p.Name.StartsWith(placeName, StringComparison.OrdinalIgnoreCase)).ToList();
                placeName = placeName.Remove(placeName.Length - 1);

            }
            if (places.Count > 0 && addressParts.Count()>1) {
                placeName = addressParts[1];
                if (places.Where(p=>p.Name.Contains(placeName, StringComparison.OrdinalIgnoreCase)).Count() > 0)
                {
                    addressParts.RemoveAt(0);
                    addressParts.RemoveAt(0);
                    return places.First(p => p.Name.Contains(placeName, StringComparison.OrdinalIgnoreCase)).Id;
                }
            }
            if (places.Count() > 0)
            {
                addressParts.RemoveAt(0);
                return places[0].Id;
            }

            return null;
        }

        public async Task<ICollection<PlaceType>> GetPlaceTypesAsync(List<int> placeIds)
        {
            return await _context.Set<PlaceType>().Where(pt => placeIds.Contains(pt.Id)).ToListAsync();
        }

        public async Task<ICollection<StreetType>> GetStreetTypesAsync(List<int> streetIds)
        {
            return await _context.Set<StreetType>().Where(st => streetIds.Contains(st.Id)).ToListAsync();
        }

        public async Task<ICollection<ShortAddress>> GetShortAddressAsync(string addressName, List<int> placeIds)
        {
            List<ShortAddress> addresses = new List<ShortAddress>();
            while (addressName != "" && addresses.Count == 0)
            {
                var streetsContainsName = await _context.Set<ShortAddress>().Where(s => placeIds.Contains(s.PlaceId) && s.Name.ToLower().Contains(addressName)).ToListAsync();
                addresses.AddRange(streetsContainsName);
                addressName = addressName.Remove(addressName.Length - 1);
            }
            return addresses.Take(5).ToList();
        }
    }
}
