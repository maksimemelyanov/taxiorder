﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderAddress.Infrastructure.Entities;

namespace TaxiOrderAddress.Infrastructure.Repositories.Implementations;

public class HouseReadRepository : ReadRepository<House, int>, IReadRepository<House, int>
{
    public HouseReadRepository(DbContext context) : base(context)
    {
    }
}