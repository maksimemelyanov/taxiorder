﻿
using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderAddress.Infrastructure.Entities;

namespace TaxiOrderAddress.Infrastructure.Repositories.Implementations;


public class HouseRepository : Repository<House, int>, IRepository<House, int>
{
    public HouseRepository(DbContext context) : base(context)
    {
    }
}