﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderAddress.Infrastructure.Entities;

namespace TaxiOrderAddress.Infrastructure.Repositories.Implementations;

public class PlaceReadRepository : ReadRepository<Place, int>, IReadRepository<Place, int>
{
    public PlaceReadRepository(DbContext context) : base(context)
    {
    }

}