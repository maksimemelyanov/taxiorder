﻿
using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderAddress.Infrastructure.Entities;

namespace TaxiOrderAddress.Infrastructure.Repositories.Implementations;


public class PlaceRepository : Repository<Place, int>, IRepository<Place, int>
{
    public PlaceRepository(DbContext context) : base(context)
    {
    }
}