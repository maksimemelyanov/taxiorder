﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderAddress.Infrastructure.Entities;

namespace TaxiOrderAddress.Infrastructure.Repositories.Implementations;

public class PlaceTypeReadRepository : ReadRepository<PlaceType, int>, IReadRepository<PlaceType, int>
{
    public PlaceTypeReadRepository(DbContext context) : base(context)
    {
    }
}