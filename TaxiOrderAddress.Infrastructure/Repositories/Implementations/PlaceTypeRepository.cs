﻿
using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderAddress.Infrastructure.Entities;

namespace TaxiOrderAddress.Infrastructure.Repositories.Implementations;


public class PlaceTypeRepository : Repository<PlaceType, int>, IRepository<PlaceType, int>
{
    public PlaceTypeRepository(DbContext context) : base(context)
    {
    }
}