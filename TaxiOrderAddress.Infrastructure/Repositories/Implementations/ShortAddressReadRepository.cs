﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderAddress.Infrastructure.Entities;

namespace TaxiOrderAddress.Infrastructure.Repositories.Implementations;

public class ShortAddressReadRepository : ReadRepository<ShortAddress, int>, IReadRepository<ShortAddress, int>
{
    public ShortAddressReadRepository(DbContext context) : base(context)
    {
    }
}