﻿
using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderAddress.Infrastructure.Entities;

namespace TaxiOrderAddress.Infrastructure.Repositories.Implementations;


public class ShortAddressRepository : Repository<ShortAddress, int>, IRepository<ShortAddress, int>
{
    public ShortAddressRepository(DbContext context) : base(context)
    {
    }
}