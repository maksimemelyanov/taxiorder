﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderAddress.Infrastructure.Entities;
using System.Security.Cryptography.X509Certificates;

namespace TaxiOrderAddress.Infrastructure.Repositories.Implementations;

public class StreetReadRepository : ReadRepository<Street, int>, IReadRepository<Street, int>
{
    public StreetReadRepository(DbContext context) : base(context)
    {
        
    }
}