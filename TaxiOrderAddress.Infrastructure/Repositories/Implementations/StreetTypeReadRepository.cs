﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderAddress.Infrastructure.Entities;

namespace TaxiOrderAddress.Infrastructure.Repositories.Implementations;

public class StreetTypeReadRepository : ReadRepository<StreetType, int>, IReadRepository<StreetType, int>
{
    public StreetTypeReadRepository(DbContext context) : base(context)
    {
    }
}