﻿
using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderAddress.Infrastructure.Entities;

namespace TaxiOrderAddress.Infrastructure.Repositories.Implementations;


public class StreetTypeRepository : Repository<StreetType, int>, IRepository<StreetType, int>
{
    public StreetTypeRepository(DbContext context) : base(context)
    {
    }
}