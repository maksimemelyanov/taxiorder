﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiOrderAddress.Infrastructure.Entities;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderAddress.Infrastructure.Repositories.Interfaces
{
    public interface IAddressRepository
    {
        Task<Region> GetRegion(int id);
        /// <summary>
        ///     Найти все места в базе содержащие в названии строку поиска и относящиеся у указанному региону
        /// </summary>
        /// <param name="regionId">Регион к которому относится место</param>
        /// <returns>Список мест</returns>
        Task<ICollection<Place>> GetPlacesAsync(int regionId);
        Task<ICollection<Street>> GetStreetsAsync(string placeName, List<int> placeIds, int? streetTypeId);
        Task<ICollection<House>> GetHousesAsync(string houseName, List<int> streetIds);
        Task<int?> GetPlaceTypeByNameAsync( List<string> addressParts);
        Task<int?> GetPlaceByNameAsync(List<string> addressParts, int regionId, int? placeTypeId);
        Task<int?> GetStreetTypeByNameAsync(List<string> addressParts);        
        Task<ICollection<PlaceType>> GetPlaceTypesAsync(List<int> placeIds);
        Task<ICollection<StreetType>> GetStreetTypesAsync(List<int> streetIds);
        Task<ICollection<ShortAddress>> GetShortAddressAsync(string streetName, List<int> list);
        
    }
}
