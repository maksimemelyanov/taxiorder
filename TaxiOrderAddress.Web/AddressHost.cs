using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Web.Mapping;
using TaxiOrderAddress.Application;
using TaxiOrderAddress.Application.Services.InitCaches.Implementations;
using TaxiOrderAddress.Infrastructure;
using TaxiOrderAddress.Infrastructure.Context;
using TaxiOrderAddress.Web.Mappings;



namespace TaxiOrderAddress.Web;

public class AddressHost
{
    private const string ConfigurationFileName = "appsettings.json";
    private readonly WebApplicationBuilder _builder;

    private AddressHost(string[] args)
    {
        _builder = WebApplication.CreateBuilder(args);
    }

    public static AddressHost CreateHost(string[] args)
    {
        return new AddressHost(args);
    }

    public AddressHost ConfigureHost()
    {
        _builder.Host
            .ConfigureAppConfiguration(builder =>
            {
                builder.AddJsonFile(ConfigurationFileName);
                builder.AddUserSecrets<AddressHost>(true, true);
            });

        return this;
    }

    public AddressHost ConfigureSwaggerService()
    {
        _builder.Services.AddEndpointsApiExplorer();
        _builder.Services.AddSwaggerGen();
        return this;
    }

    public AddressHost ConfigureServices()
    {
        _builder.Services.AddControllers();
        _builder.Services
            .AddScoped(typeof(DbContext), typeof(DatabaseContext))
            .AddDbContext<DatabaseContext>(x => { x.UseNpgsql(_builder.Configuration.GetConnectionString("db")); })
            .AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()))
            .AddInfrastructure()
            .AddApplication();

        _builder.Services.AddHostedService<InitCachesService>();
        return this;
    }
    
    private MapperConfiguration GetMapperConfiguration()
    {
        var configuration = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<TaxiOrderAddress.Application.Mappings.HouseMappingProfile>();
            cfg.AddProfile<HouseMappingProfile>();
            cfg.AddProfile<TaxiOrderAddress.Application.Mappings.PlaceMappingProfile>();
            cfg.AddProfile<PlaceMappingProfile>();
            cfg.AddProfile<TaxiOrderAddress.Application.Mappings.PlaceTypeMappingProfile>();
            cfg.AddProfile<PlaceTypeMappingProfile>();
            cfg.AddProfile<TaxiOrderAddress.Application.Mappings.ShortAddressMappingProfile>();
            cfg.AddProfile<ShortAddressMappingProfile>();
            cfg.AddProfile<TaxiOrderAddress.Application.Mappings.StreetMappingProfile>();
            cfg.AddProfile<StreetMappingProfile>();
            cfg.AddProfile<TaxiOrderAddress.Application.Mappings.StreetTypeMappingProfile>();
            cfg.AddProfile<StreetTypeMappingProfile>();            
            cfg.AddProfile<TaxiOrderCore.Application.Mappings.BaseMappingProfile>();
            cfg.AddProfile<BaseMappingProfile>();            
            cfg.AddProfile<TaxiOrderCore.Application.Mappings.RegionMappingProfile>();
            cfg.AddProfile<RegionMappingProfile>();
            cfg.AddProfile<BaseAddressMappingProfile>();
            cfg.AddProfile<PlaceToPointMappingProfile>();
            cfg.AddProfile<StreetToPointMappingProfile>();
            cfg.AddProfile<PointMappingProfile>();
        });
        configuration.AssertConfigurationIsValid();
        return configuration;
    }

    public async Task RunHost()
    {
        var webApplication = _builder.Build();

        var useSwagger = _builder.Configuration.GetValue<bool>("useSwagger");
        if (useSwagger)
            webApplication
                .UseSwagger()
                .UseSwaggerUI();

        webApplication
            .UseRouting()
            .UseHttpsRedirection()
            .UseRequestLocalization()
            .UseSwagger()
            .UseSwaggerUI()
            .UseAuthorization();

        webApplication.UseEndpoints(endpoints =>
        {
            endpoints
                .MapControllers();
        });

        await webApplication.RunAsync();
    }
}