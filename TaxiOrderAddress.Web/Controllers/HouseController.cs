﻿using AutoMapper;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Web.Models;

namespace TaxiOrderTariff.Web.Controllers;

public class HouseController : GenericCrudController<HouseDto, HouseModel, int>
{
    public HouseController(ICrudService<HouseDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}