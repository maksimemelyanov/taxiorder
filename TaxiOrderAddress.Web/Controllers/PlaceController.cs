﻿using AutoMapper;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Web.Models;

namespace TaxiOrderTariff.Web.Controllers;

public class PlaceController : GenericCrudController<PlaceDto, PlaceModel, int>
{
    public PlaceController(ICrudService<PlaceDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}