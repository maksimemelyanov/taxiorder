﻿using AutoMapper;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Web.Models;

namespace TaxiOrderTariff.Web.Controllers;

public class PlaceTypeController : GenericCrudController<PlaceTypeDto, PlaceTypeModel, int>
{
    public PlaceTypeController(ICrudService<PlaceTypeDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}