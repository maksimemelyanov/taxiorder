﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using TaxiOrderAddress.Web.Models;
using TaxiOrderCore.Web;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Application.Services.PointService.Interfaces;

namespace TaxiOrderAddress.Web.Controllers;

[ApiController]
[Route("api/point")]
public class PointController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly IPointProvider _provider;

    public PointController( IMapper mapper, IPointProvider provider)
    {
        _mapper = mapper;
        _provider = provider;
    }

    [HttpGet("calculation")]
    public async Task<IEnumerable<PointModel>> Get([FromQuery(Name = ApiConstants.BaseId)][BindRequired] int baseId,
        [FromQuery(Name = ApiConstants.AddressName)][BindRequired] string addressName)
    {
        var points = await _provider.GetPoints(baseId, addressName);
        return _mapper.Map<IEnumerable<PointModel>>(points);
        //return _provider.GetPoints(baseId, addressName);
        
    }

    
}