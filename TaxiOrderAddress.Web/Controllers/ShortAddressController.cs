﻿using AutoMapper;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Web.Models;

namespace TaxiOrderTariff.Web.Controllers;

public class ShortAddressController : GenericCrudController<ShortAddressDto, ShortAddressModel, int>
{
    public ShortAddressController(ICrudService<ShortAddressDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}