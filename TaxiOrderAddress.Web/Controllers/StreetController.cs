﻿using AutoMapper;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Web.Models;

namespace TaxiOrderTariff.Web.Controllers;

public class StreetController : GenericCrudController<StreetDto, StreetModel, int>
{
    public StreetController(ICrudService<StreetDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}