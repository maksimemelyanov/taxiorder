﻿using AutoMapper;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Web.Models;

namespace TaxiOrderTariff.Web.Controllers;

public class StreetTypeController : GenericCrudController<StreetTypeDto, StreetTypeModel, int>
{
    public StreetTypeController(ICrudService<StreetTypeDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}