namespace TaxiOrderAddress.Web;

public class EntryPoint
{
    public static async Task Main(string[] args)
    {
        var host = AddressHost.CreateHost(args);
        host.ConfigureHost()
            .ConfigureSwaggerService()
            .ConfigureServices();

        await host.RunHost();
    }
}