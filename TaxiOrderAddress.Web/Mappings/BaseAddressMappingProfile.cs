﻿using AutoMapper;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Web.Models;

namespace TaxiOrderAddress.Web.Mappings;

public class BaseAddressMappingProfile : Profile
{
    public BaseAddressMappingProfile()
    {
        CreateMap<Application.Contracts.BaseAddressDto, BaseAddressModel>();
        CreateMap<BaseAddressModel, Application.Contracts.BaseAddressDto>();
    }
}