﻿using AutoMapper;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Web.Models;

namespace TaxiOrderAddress.Web.Mappings;

public class HouseMappingProfile : Profile
{
    public HouseMappingProfile()
    {
        CreateMap<HouseDto, HouseModel>();
        CreateMap<HouseModel, HouseDto>();
    }
}