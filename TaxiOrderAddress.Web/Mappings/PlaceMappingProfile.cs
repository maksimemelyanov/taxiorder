﻿using AutoMapper;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Web.Models;

namespace TaxiOrderAddress.Web.Mappings;

public class PlaceMappingProfile : Profile
{
    public PlaceMappingProfile()
    {
        CreateMap<PlaceDto, PlaceModel>();
        CreateMap<PlaceModel, PlaceDto>();
    }
}