﻿using AutoMapper;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Web.Models;

namespace TaxiOrderAddress.Web.Mappings;

public class PlaceToPointMappingProfile : Profile
{
    public PlaceToPointMappingProfile()
    {
        CreateMap<Application.Contracts.PlaceToPointDto, PlaceToPointModel>();
        CreateMap<PlaceToPointModel, Application.Contracts.PlaceToPointDto>();
    }
}