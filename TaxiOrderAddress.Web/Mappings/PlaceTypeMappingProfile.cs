﻿using AutoMapper;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Web.Models;

namespace TaxiOrderAddress.Web.Mappings;

public class PlaceTypeMappingProfile : Profile
{
    public PlaceTypeMappingProfile()
    {
        CreateMap<PlaceTypeDto, PlaceTypeModel>();
        CreateMap<PlaceTypeModel, PlaceTypeDto>();
    }
}