﻿using AutoMapper;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Web.Models;

namespace TaxiOrderAddress.Web.Mappings;

public class PointMappingProfile : Profile
{
    public PointMappingProfile()
    {
        CreateMap<PointDto, PointModel>();
        CreateMap<PointModel, PointDto>();
    }
}