﻿using AutoMapper;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Web.Models;

namespace TaxiOrderAddress.Web.Mappings;

public class StreetMappingProfile : Profile
{
    public StreetMappingProfile()
    {
        CreateMap<StreetDto, StreetModel>();
        CreateMap<StreetModel, StreetDto>();
    }
}