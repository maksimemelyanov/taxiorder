﻿using AutoMapper;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Web.Models;

namespace TaxiOrderAddress.Web.Mappings;

public class ShortAddressMappingProfile : Profile
{
    public ShortAddressMappingProfile()
    {
        CreateMap<ShortAddressDto, ShortAddressModel>();
        CreateMap<ShortAddressModel, ShortAddressDto>();
    }
}