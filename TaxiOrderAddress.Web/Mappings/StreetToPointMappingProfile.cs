﻿using AutoMapper;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Web.Models;

namespace TaxiOrderAddress.Web.Mappings;

public class StreetToPointMappingProfile : Profile
{
    public StreetToPointMappingProfile()
    {
        CreateMap<Application.Contracts.StreetToPointDto, StreetToPointModel>();
        CreateMap<StreetToPointModel, Application.Contracts.StreetToPointDto>();
    }
}