﻿using AutoMapper;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Web.Models;

namespace TaxiOrderAddress.Web.Mappings;

public class StreetTypeMappingProfile : Profile
{
    public StreetTypeMappingProfile()
    {
        CreateMap<StreetTypeDto, StreetTypeModel>();
        CreateMap<StreetTypeModel, StreetTypeDto>();
    }
}