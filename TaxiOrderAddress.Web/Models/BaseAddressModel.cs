﻿namespace TaxiOrderAddress.Web.Models
{
    public class BaseAddressModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }   
}
