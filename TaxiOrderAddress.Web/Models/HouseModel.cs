﻿namespace TaxiOrderAddress.Web.Models;
public class HouseModel
{
    public int Id { get ; set ; }

    public string Name { get; set; } = null!;

    public int StreetId { get; set; }

    public int Longitude { get; set; }

    public int Latitude { get; set; }
}

