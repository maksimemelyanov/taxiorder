﻿namespace TaxiOrderAddress.Web.Models
{
    public class PointModel
    {

        public BaseAddressModel Region { get; set; }
        public PlaceToPointModel Place { get; set; }
        public BaseAddressModel? Address { get; set; }
        public StreetToPointModel? Street { get; set; }
        public BaseAddressModel? House { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
