﻿namespace TaxiOrderAddress.Web.Models;
public class ShortAddressModel
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public int PlaceId { get; set; }

    public int Longitude { get; set; }

    public int Latitude { get; set; }
}
