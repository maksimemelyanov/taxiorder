﻿namespace TaxiOrderAddress.Web.Models;

public class StreetModel
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public int PlaceId { get; set; }

    public int StreetTypeId { get; set; }
}
