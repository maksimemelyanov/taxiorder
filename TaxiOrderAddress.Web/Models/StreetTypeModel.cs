﻿namespace TaxiOrderAddress.Application.Contracts;

public class StreetTypeModel
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;
}
