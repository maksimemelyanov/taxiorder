﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxiOrderClient.Application.Contracts
{
    public class ClientOrderDto
    {
        public int ClientId { get; set; }

        public string StartAddress { get; set; }

        public string EndAddress { get; set; }

        public int TariffId { get; set; }

        public double ClientPrice { get; set; }

        public double DriverPrice { get; set; }

        public int? AllowanceId { get; set; }
    }
}
