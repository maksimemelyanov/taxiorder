using Microsoft.Extensions.DependencyInjection;
using TaxiOrderCore.Application.Services.Implementations;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderClient.Application.Contracts;
using TaxiOrderClient.Infrastructure.Entities;
using TaxiOrderClient.Application.Services.ClientOrder.Implementations;
using TaxiOrderClient.Application.Services.ClientOrder.Interfaces;
using TaxiOrderCore.MessageBus.Bus;

namespace TaxiOrderClient.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplication(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddTransient<ICrudService<ClientDto, int>, CrudService<ClientDto, Client, int>>();
        serviceCollection.AddTransient<IClientOrderProvider, ClientOrderProvider>();        

        return serviceCollection;
    }
}