﻿using AutoMapper;
using TaxiOrderClient.Application.Contracts;
using TaxiOrderClient.Infrastructure.Entities;

namespace TaxiOrderClient.Application.Mappings;

public class ClientMappingProfile : Profile
{
    public ClientMappingProfile()
    {
        CreateMap<ClientDto, Client>();
        CreateMap<Client, ClientDto>();
    }
}