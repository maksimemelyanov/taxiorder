﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Net.Http.Headers;
using System.Web;
using TaxiOrderClient.Application.Contracts;
using TaxiOrderClient.Application.Services.ClientOrder.Interfaces;

namespace TaxiOrderClient.Application.Services.ClientOrder.Implementations
{
    public class ClientOrderProvider : IClientOrderProvider
    {
        public ClientOrderDto MessageToOrderService(int idClient, string startAddress, string endAddress, int idTariff, int idAllowance)
        {
            var order = new ClientOrderDto();

            order.ClientId = idClient;
            order.StartAddress = startAddress;
            order.EndAddress = endAddress;
            order.TariffId = idTariff;
            order.AllowanceId = idAllowance;

            int BaseId = 3;
            int DriverTariffId = 1;

            string jsonRespStartAddress;

            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), $@"http://host.docker.internal:9004/api/point/calculation?id-base={BaseId}&address-name={HttpUtility.UrlEncode(order.StartAddress)}"))
                {
                    //request.Headers.TryAddWithoutValidation("accept", "text/plain");
                    var response = httpClient.Send(request);
                    jsonRespStartAddress = response.Content.ReadAsStringAsync().Result;
                }
            }

            double startLatitude = 0;
            double startLongitude = 0;
            JArray arrayStart = JArray.Parse(jsonRespStartAddress);

            foreach (JObject obj in arrayStart.Take(1))
            {
                startLatitude = (double)obj["latitude"];
                startLongitude = (double)obj["longitude"];
            }

            string jsonRespEndAddress;

            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), $@"http://host.docker.internal:9004/api/point/calculation?id-base={BaseId}&address-name={HttpUtility.UrlEncode(order.EndAddress)}"))
                {
                    //request.Headers.TryAddWithoutValidation("accept", "text/plain");


                    var response = httpClient.Send(request);
                    jsonRespEndAddress = response.Content.ReadAsStringAsync().Result;

                }
            }

            double endLatitude = 0;
            double endLongitude = 0;

            JArray arrayEnd = JArray.Parse(jsonRespEndAddress);

            foreach (JObject obj in arrayEnd.Take(1))
            {
                endLatitude = (double)obj["latitude"];
                endLongitude = (double)obj["longitude"];
            }

            string jsonRespTariff;

            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), $@"http://host.docker.internal:9001/api/price/calculation?id-tariff={order.TariffId}&start-latitude={startLatitude.ToString(CultureInfo.InvariantCulture)}&start-longitude={startLongitude.ToString(CultureInfo.InvariantCulture)}&end-latitude={endLatitude.ToString(CultureInfo.InvariantCulture)}&end-longitude={endLongitude.ToString(CultureInfo.InvariantCulture)}&id-driver-tariff={DriverTariffId}"))
                {
                    request.Headers.TryAddWithoutValidation("accept", "text/plain");

                    request.Content = new StringContent("[ \n  "+"{ \n    " + $"\"id\": {idAllowance}, \n"+"    \"count\": 1 \n  } \n]");
                    request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
                    
                    var response = httpClient.Send(request);
                    jsonRespTariff = response.Content.ReadAsStringAsync().Result;

                }
            }

            JObject arrayTariff = JObject.Parse(jsonRespTariff);

            order.ClientPrice = (double)arrayTariff["clientPrice"];
            order.DriverPrice = (double)arrayTariff["driverPrice"];
            
            return order;           
        }
    }
}
