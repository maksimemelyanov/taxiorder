﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiOrderClient.Application.Contracts;

namespace TaxiOrderClient.Application.Services.ClientOrder.Interfaces
{
    public interface IClientOrderProvider
    {
        public ClientOrderDto MessageToOrderService(int idClient, string startAddress, string endAddress, int idTariff, int idAllowance);
    }
}
