﻿namespace TaxiOrderClient.Application.Services.InitCaches.Configurations;

public class CacheSettings
{
    public TimeSpan ReloadTimeout { get; set; } = TimeSpan.FromMinutes(1);
}