﻿using System.Collections.Concurrent;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TaxiOrderCore.Infrastructure.Caches.Implementations;
using TaxiOrderClient.Infrastructure.Context;
using TaxiOrderClient.Infrastructure.Entities;
using TaxiOrderClient.Infrastructure.Repositories.Implementations;

namespace TaxiOrderClient.Infrastructure.Caches.Implementations;

public class ClientCache : GenericCache<Client, int>
{
    private readonly IServiceProvider _provider;
    private readonly ILogger<GenericCache<Client, int>> _logger;

    public ClientCache(ILogger<GenericCache<Client, int>> logger, IServiceProvider provider)
    {
        _provider = provider;
        _logger = logger;
    }

    public override async Task Init()
    {
        try
        {
            var scope = _provider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            await using var context = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
            var repository = new ClientReadRepository(context); 
            var result = await repository.GetAllAsync(CancellationToken.None);
            Cache = new ConcurrentDictionary<int, Client>(result.ToDictionary(item => item.Id));
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, InitCacheError);
            Cache = new ConcurrentDictionary<int, Client>();
            throw;
        }
    }
}