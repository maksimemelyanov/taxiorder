using Microsoft.Extensions.DependencyInjection;
using TaxiOrderCore.Infrastructure;
using TaxiOrderCore.Infrastructure.Caches;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderClient.Infrastructure.Caches.Implementations;
using TaxiOrderClient.Infrastructure.Entities;
using TaxiOrderClient.Infrastructure.Repositories.Implementations;

namespace TaxiOrderClient.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddInfrastructureCore();
        serviceCollection
            .AddTransient<IRepository<Client, int>, ClientRepository>()
            .AddTransient<IReadRepository<Client, int>, ClientReadRepository>();
        
        serviceCollection.AddCache<int, Client, ClientCache>();

        return serviceCollection;
    }
}