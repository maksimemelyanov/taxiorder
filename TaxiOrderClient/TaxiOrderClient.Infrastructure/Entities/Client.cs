﻿using System.ComponentModel.DataAnnotations.Schema;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderClient.Infrastructure.Entities;

[Table("client", Schema = "main")]
public class Client : IEntity<int>
{
    [Column("id")]
    public int Id { get; set; }

    [Column("name")]
    public string Name { get; set; } = null!;

    [Column("first_name")]
    public string FirstName { get; set; } = null!;

    [Column("last_name")]
    public string LastName { get; set; } = null!;

    [Column("patronymic")]
    public string Patronymic { get; set; } = null!;

    [Column("phone")]
    public string Phone { get; set; } = null!;

    [Column("birthdate")]
    public DateTime Birthdate { get; set; }

    [Column("is_man")]
    public bool IsMan { get; set; }

    [Column("email")]
    public string Email { get; set; } = null!;
}