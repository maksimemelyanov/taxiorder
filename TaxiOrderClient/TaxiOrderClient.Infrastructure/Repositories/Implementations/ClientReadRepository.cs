﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderClient.Infrastructure.Entities;

namespace TaxiOrderClient.Infrastructure.Repositories.Implementations;

public class ClientReadRepository : ReadRepository<Client, int>, IReadRepository<Client, int>
{
    public ClientReadRepository(DbContext context) : base(context)
    {
    }
}