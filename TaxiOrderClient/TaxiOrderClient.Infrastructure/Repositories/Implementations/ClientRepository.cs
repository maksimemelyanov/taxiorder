﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderClient.Infrastructure.Entities;

namespace TaxiOrderClient.Infrastructure.Repositories.Implementations;

public class ClientRepository : Repository<Client, int>, IRepository<Client, int>
{
    public ClientRepository(DbContext context) : base(context)
    {
    }
}