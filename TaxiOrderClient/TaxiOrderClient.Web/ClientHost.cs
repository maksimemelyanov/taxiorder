using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TaxiOrderClient.Application;
using TaxiOrderClient.Application.Services.InitCaches.Implementations;
using TaxiOrderClient.Infrastructure;
using TaxiOrderClient.Infrastructure.Context;
using TaxiOrderClient.Web.Controllers;
using TaxiOrderClient.Web.Handlers;
using TaxiOrderClient.Web.Mappings;
using TaxiOrderCore.MessageBus.Bus;
using TaxiOrderCore.MessageBus.Messages;
using TaxiOrderCore.MessageBus.RabbitMQ.Extensions;

namespace TaxiOrderClient.Web;

public class ClientHost
{
    private const string ConfigurationFileName = "appsettings.json";
    private readonly WebApplicationBuilder _builder;

    private ClientHost(string[] args)
    {
        _builder = WebApplication.CreateBuilder(args);
    }

    public static ClientHost CreateHost(string[] args)
    {
        return new ClientHost(args);
    }

    public ClientHost ConfigureHost()
    {
        _builder.Host
            .ConfigureAppConfiguration(builder =>
            {
                builder.AddJsonFile(ConfigurationFileName);
                builder.AddUserSecrets<ClientHost>(true, true);
            });

        return this;
    }

    public ClientHost ConfigureSwaggerService()
    {
        _builder.Services.AddEndpointsApiExplorer();
        _builder.Services.AddSwaggerGen();
        return this;
    }

    public ClientHost ConfigureServices()
    {
        _builder.Services.AddControllers();
        _builder.Services
            .AddScoped(typeof(DbContext), typeof(DatabaseContext))
            .AddDbContext<DatabaseContext>(x => { x.UseNpgsql(_builder.Configuration.GetConnectionString("db")); })
            .AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()))
            .AddInfrastructure()
            .AddApplication();

        _builder.Services.AddHostedService<InitCachesService>();
        return this;
    }

    public ClientHost ConfigureMessageBusDependencies()
    {
        var rabbitMQSection = _builder.Configuration.GetSection("RabbitMQ");
        _builder.Services.AddRabbitMQMessageBus
        (
            host: rabbitMQSection["Host"],
            exchangeName: rabbitMQSection["Exchange"],
            queueName: rabbitMQSection["Queue"],
            timeoutBeforeReconnecting: 15
        );

        // ����������� ���������
        _builder.Services.AddTransient<ClientStartRequestMessageHandler>();
        _builder.Services.AddTransient<ClientResponseMessageHandler>();

        return this;
    }

    private void ConfigureMessageBusHandlers(IApplicationBuilder app)
    {
        var messageBus = app.ApplicationServices.GetRequiredService<IMessageBus>();

        // ������������� ��������� � �������� ���������
        messageBus.Subscribe<ClientStartRequestMessage, ClientStartRequestMessageHandler>();
        messageBus.Subscribe<ClientResponseMessage, ClientResponseMessageHandler>();
    }

    private MapperConfiguration GetMapperConfiguration()
    {
        var configuration = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<TaxiOrderClient.Application.Mappings.ClientMappingProfile>();
            cfg.AddProfile<ClientMappingProfile>();
            cfg.AddProfile<ClientOrderMappingProfile>();
        });
        configuration.AssertConfigurationIsValid();
        return configuration;
    }

    public async Task RunHost()
    {
        var webApplication = _builder.Build();

        var useSwagger = _builder.Configuration.GetValue<bool>("useSwagger");
        if (useSwagger)
            webApplication
                .UseSwagger()
                .UseSwaggerUI();

        webApplication
            .UseRouting()
            .UseHttpsRedirection()
            .UseRequestLocalization()
            .UseSwagger()
            .UseSwaggerUI()
            .UseAuthorization();

        webApplication.UseEndpoints(endpoints =>
        {
            endpoints
                .MapControllers();
        });


        ConfigureMessageBusHandlers(webApplication);
        await webApplication.RunAsync();
    }
}