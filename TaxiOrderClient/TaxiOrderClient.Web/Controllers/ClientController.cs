﻿using AutoMapper;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderClient.Application.Contracts;
using TaxiOrderClient.Web.Models;

namespace TaxiOrderClient.Web.Controllers;

public class ClientController : GenericCrudController<ClientDto, ClientModel, int>
{
    public ClientController(ICrudService<ClientDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}