﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using TaxiOrderClient.Web.Models;
using TaxiOrderCore.Web;
using TaxiOrderClient.Application.Services.ClientOrder.Interfaces;
using TaxiOrderClient.Web.Mappings;
using TaxiOrderCore.MessageBus.Messages;
using TaxiOrderCore.MessageBus.Bus;
using Microsoft.Extensions.Logging;
using TaxiOrderClient.Web.Handlers;

namespace TaxiOrderClient.Web.Controllers
{

    [ApiController]
    [Route("api/client")]
    public class ClientOrderController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IClientOrderProvider _provider;
        private readonly IMessageBus _messageBus;

        public ClientOrderController(IMapper mapper, IClientOrderProvider provider, IMessageBus messageBus)
        {
            _mapper = mapper;
            _provider = provider;
            _messageBus = messageBus;
        }

        [HttpGet("clientOrder")]
        public ClientOrderModel Get([FromQuery(Name = ApiConstants.ClientId)][BindRequired] int clientId,
        [FromQuery(Name = ApiConstants.StartAddress)][BindRequired] string startAddress,
        [FromQuery(Name = ApiConstants.EndAddress)][BindRequired] string endAddress,
        [FromQuery(Name = ApiConstants.TariffId)][BindRequired] int tariffId,
        [FromQuery(Name = ApiConstants.AllowanceId)] int allowanceId,
        [FromQuery(Name = ApiConstants.BaseId)] int baseId)
        {
            var message = new ClientStartRequestMessage
            {
                ClientId = _provider.MessageToOrderService(clientId, startAddress, endAddress, tariffId, allowanceId).ClientId,
                TariffId = _provider.MessageToOrderService(clientId, startAddress, endAddress, tariffId, allowanceId).TariffId,
                ClientPrice = _provider.MessageToOrderService(clientId, startAddress, endAddress, tariffId, allowanceId).ClientPrice,
                DriverPrice = _provider.MessageToOrderService(clientId, startAddress, endAddress, tariffId, allowanceId).DriverPrice,
                StartAddress = _provider.MessageToOrderService(clientId, startAddress, endAddress, tariffId, allowanceId).StartAddress,
                EndAddress = _provider.MessageToOrderService(clientId, startAddress, endAddress, tariffId, allowanceId).EndAddress,
                AllowanceId = _provider.MessageToOrderService(clientId, startAddress, endAddress, tariffId, allowanceId).AllowanceId,
                BaseId = baseId
            };
            _messageBus.Publish(message);
            
            return _mapper.Map<ClientOrderModel>(_provider.MessageToOrderService(clientId, startAddress, endAddress, tariffId, allowanceId));
        }
    }
}
