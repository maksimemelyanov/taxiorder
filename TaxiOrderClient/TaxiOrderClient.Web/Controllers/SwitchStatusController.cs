﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using TaxiOrderCore.Application;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderCore.MessageBus.Bus;
using TaxiOrderCore.MessageBus.Messages;
using TaxiOrderCore.Web;


namespace TaxiOrderClient.Web.Controllers
{
    [ApiController]
    [Route("api/client")]
    public class SwitchStatusController : ControllerBase
    {
        private readonly IMessageBus _messageBus;

        public SwitchStatusController(IMessageBus messageBus)
        {
            _messageBus = messageBus;
        }

        [HttpPut("client-out")]
        public IActionResult ClientOut(
        [FromQuery(Name = ApiConstants.OrderId)] [BindRequired]
        int orderId)
        {
            //var result = new ClientResponseMessage
            //{
            //    OrderId = orderId,
            //    Status = TaxiOrderCore.Application.OrderStatus.ClientOut
            //};
            //_messageBus.Publish(result);
            return Ok();
        }

    }
}
