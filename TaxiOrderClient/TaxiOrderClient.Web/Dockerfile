#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["TaxiOrderClient/TaxiOrderClient.Web/TaxiOrderClient.Web.csproj", "TaxiOrderClient/TaxiOrderClient.Web/"]
COPY ["Core/TaxiOrderCore.Web/TaxiOrderCore.Web.csproj", "Core/TaxiOrderCore.Web/"]
COPY ["Core/TaxiOrderCore.Application/TaxiOrderCore.Application.csproj", "Core/TaxiOrderCore.Application/"]
COPY ["Core/TaxiOrderCore.Infrastructure/TaxiOrderCore.Infrastructure.csproj", "Core/TaxiOrderCore.Infrastructure/"]
COPY ["TaxiOrderClient/TaxiOrderClient.Infrastructure/TaxiOrderClient.Infrastructure.csproj", "TaxiOrderClient/TaxiOrderClient.Infrastructure/"]
COPY ["TaxiOrderClient/TaxiOrderClient.Application/TaxiOrderClient.Application.csproj", "TaxiOrderClient/TaxiOrderClient.Application/"]
RUN dotnet restore "TaxiOrderClient/TaxiOrderClient.Web/TaxiOrderClient.Web.csproj"
COPY . .
WORKDIR "/src/TaxiOrderClient/TaxiOrderClient.Web"
RUN dotnet build "TaxiOrderClient.Web.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "TaxiOrderClient.Web.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "TaxiOrderClient.Web.dll"]