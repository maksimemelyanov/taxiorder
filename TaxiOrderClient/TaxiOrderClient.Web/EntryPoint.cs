namespace TaxiOrderClient.Web;

public class EntryPoint
{
    public static async Task Main(string[] args)
    {
        var host = ClientHost.CreateHost(args);
        host.ConfigureHost()
            .ConfigureSwaggerService()
            .ConfigureServices()
            .ConfigureMessageBusDependencies();

        await host.RunHost();
    }
}