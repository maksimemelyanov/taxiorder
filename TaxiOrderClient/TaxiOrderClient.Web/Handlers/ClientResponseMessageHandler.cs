﻿using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderCore.MessageBus.Messages;
using TaxiOrderCore.Application;

namespace TaxiOrderClient.Web.Handlers
{
    public class ClientResponseMessageHandler : IMessageHandler<ClientResponseMessage>
    {
        private readonly ILogger<ClientResponseMessageHandler> _logger;

        public ClientResponseMessageHandler(ILogger<ClientResponseMessageHandler> logger)
        {
            _logger = logger;
        }

        public Task HandleAsync(ClientResponseMessage message)
        {
            _logger.LogInformation($"Заказ {message.OrderId}: Обновление статуса на {message.Status}. Информация: {message.Info}");
            return Task.CompletedTask;
        }
    }
}
