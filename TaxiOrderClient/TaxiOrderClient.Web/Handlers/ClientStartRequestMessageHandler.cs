﻿using TaxiOrderCore.MessageBus.Bus;
using TaxiOrderCore.MessageBus.Messages;

namespace TaxiOrderClient.Web.Handlers
{
    public class ClientStartRequestMessageHandler : IMessageHandler<ClientStartRequestMessage>
    {
        private readonly ILogger<ClientStartRequestMessageHandler> _logger;

        public ClientStartRequestMessageHandler(ILogger<ClientStartRequestMessageHandler> logger)
        {
            _logger = logger;
        }

        public Task HandleAsync(ClientStartRequestMessage message) 
        {
            _logger.LogInformation("Заказ создан");
            return Task.CompletedTask;
        }
    }
}
