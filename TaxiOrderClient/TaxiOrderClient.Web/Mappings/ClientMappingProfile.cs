﻿using AutoMapper;
using TaxiOrderClient.Application.Contracts;
using TaxiOrderClient.Web.Models;

namespace TaxiOrderClient.Web.Mappings;

public class ClientMappingProfile : Profile
{
    public ClientMappingProfile()
    {
        CreateMap<ClientDto, ClientModel>();
        CreateMap<ClientModel, ClientDto>();
    }
}