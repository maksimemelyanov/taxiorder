﻿using AutoMapper;
using TaxiOrderClient.Application.Contracts;
using TaxiOrderClient.Web.Models;

namespace TaxiOrderClient.Web.Mappings;

public class ClientOrderMappingProfile : Profile
{
    public ClientOrderMappingProfile()
    {
        CreateMap<ClientOrderDto, ClientOrderModel>();
        CreateMap<ClientOrderModel, ClientOrderDto>();
    }
}
