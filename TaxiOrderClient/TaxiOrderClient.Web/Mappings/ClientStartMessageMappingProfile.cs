﻿using AutoMapper;
using TaxiOrderClient.Application.Contracts;
using TaxiOrderClient.Web.Models;
using TaxiOrderCore.MessageBus.Messages;

namespace TaxiOrderClient.Web.Mappings;

public class ClientStartMessageMappingProfile : Profile
{
    public ClientStartMessageMappingProfile()
    {
        CreateMap<ClientOrderDto, ClientStartRequestMessage>();
        CreateMap<ClientStartRequestMessage, ClientOrderDto>();
    }
}
