﻿using AutoMapper;
using TaxiOrderClient.Application.Contracts;
using TaxiOrderClient.Web.Models;
using TaxiOrderCore.MessageBus.Messages;

namespace TaxiOrderClient.Web.Mappings
{
    public class ClientStartRequestMessageMappingProfile : Profile
    {
        public ClientStartRequestMessageMappingProfile()
        {
            CreateMap<ClientOrderDto, ClientStartRequestMessage>();
            CreateMap<ClientStartRequestMessage, ClientOrderDto>();
        }
    }
}
