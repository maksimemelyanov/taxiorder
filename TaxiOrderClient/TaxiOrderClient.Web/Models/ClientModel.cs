﻿namespace TaxiOrderClient.Web.Models;

public class ClientModel
{
    public int? Id { get; set; }

    public string Name { get; set; } = null!;

    public string FirstName { get; set; } = null!;

    public string LastName { get; set; } = null!;

    public string Patronymic { get; set; } = null!;

    public string Phone { get; set; } = null!;

    public DateTime Birthdate { get; set; }

    public bool IsMan { get; set; }

    public string Email { get; set; } = null!;
}