using TaxiOrderDriver.Application.Caches.Interfaces;
using TaxiOrderDriver.Application.Caches.Models;

namespace TaxiOrderDriver.Application.Caches.Implementations;

public class OrderCache : IOrderCache
{

    private static readonly Lazy<OrderCache> Lazy = new(() => new OrderCache());
    private readonly Dictionary<int, Order> _orders = new();
    private readonly ReaderWriterLockSlim _readerWriterLockSlim = new ();

    //Singleton Pattern
    public static OrderCache GetInstance()
    {
        return Lazy.Value;
    }

    public IEnumerable<Order> GetAll()
    {
        _readerWriterLockSlim.EnterReadLock();
        try
        {
            return _orders.Values;
        }
        finally
        {
            _readerWriterLockSlim.ExitReadLock();
        }
    }

    public bool FindByKey(int idOrder, out Order order)
    {
        _readerWriterLockSlim.EnterReadLock();
        try
        {
            var result = _orders.TryGetValue(idOrder, out order!);
            return result;
        }
        finally
        {
            _readerWriterLockSlim.ExitReadLock();
        }
    }

    public void AddOrUpdate(Order order)
    {
        _readerWriterLockSlim.EnterWriteLock();
        try
        {
            _orders[order.OrderId] = order;
        }
        finally
        {
            _readerWriterLockSlim.ExitWriteLock();
        }
    }

    public void Remove(int idOrder)
    {
        _readerWriterLockSlim.EnterWriteLock();
        try
        {
            _orders.Remove(idOrder);
        }
        finally
        {
            _readerWriterLockSlim.ExitWriteLock();
        }
    }
}