﻿using Microsoft.Extensions.Logging;
using TaxiOrderDriver.Application.Caches.Interfaces;
using TaxiOrderDriver.Application.Caches.Models;

namespace TaxiOrderDriver.Application.Caches.Implementations;

public class OrderDecoratorCache : IOrderCache
{
    private readonly ILogger<OrderDecoratorCache> _logger;
    private readonly OrderCache _orderCache;

    public OrderDecoratorCache(ILogger<OrderDecoratorCache> logger)
    {
        _logger = logger;
        _orderCache = OrderCache.GetInstance();
    }

    public IEnumerable<Order> GetAll()
    {
        _logger.LogInformation("OrderDecoratorCache -> GetAll");
        return _orderCache.GetAll();
    }

    public bool FindByKey(int idOrder, out Order order)
    {
        _logger.LogInformation("OrderDecoratorCache -> FindByKey");
        return _orderCache.FindByKey(idOrder, out order);
    }

    public void AddOrUpdate(Order order)
    {
        _logger.LogInformation("OrderDecoratorCache -> AddOrUpdate");
        _orderCache.AddOrUpdate(order);
    }

    public void Remove(int idOrder)
    {
        _logger.LogInformation("OrderDecoratorCache -> Remove");
        _orderCache.Remove(idOrder);
    }
}