using TaxiOrderDriver.Application.Caches.Models;

namespace TaxiOrderDriver.Application.Caches.Interfaces;

public interface IOrderCache
{
    IEnumerable<Order> GetAll();

    bool FindByKey(int idOrder, out Order order);
    
    void AddOrUpdate(Order order);

    void Remove(int idOrder);
}