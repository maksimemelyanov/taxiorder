using TaxiOrderCore.Application;
using TaxiOrderDriver.Application.Contracts;

namespace TaxiOrderDriver.Application.Caches.Models;

public class Order
{
    public int OrderId { get; set; }

    public OrderStatus Status { get; set; }

    public DriverBaseAutoDto DriverBaseAuto { get; set; } = null!;

    public string DriverInfo { get; set; } = null!;
    
    public DriverDto? Driver { get; set; }
}