﻿using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderDriver.Application.Contracts;

public class AutoDto : IEntity<int>
{
    public int Id { get; set; }
    
    public int AutoModelId { get; set; }
    
    public int ColorId { get; set; }
    
    public int AutoCategoryId { get; set; }
    
    public string StateNumber { get; set; } = null!;
    
    public int CreateYear { get; set; }
    
    public int SeatCount { get; set; }
    
    public string Vin { get; set; } = null!;
    
    public int DriverId { get; set; }
}