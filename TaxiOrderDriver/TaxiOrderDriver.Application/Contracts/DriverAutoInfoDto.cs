namespace TaxiOrderDriver.Application.Contracts;

public class DriverAutoInfoDto
{
    public string Driver { get; set; }
    
    public string AutoBrand { get; set; }
    
    public string AutoModel { get; set; }
    
    public string Color { get; set; }
    
    public string StateNumber { get; set; }
}