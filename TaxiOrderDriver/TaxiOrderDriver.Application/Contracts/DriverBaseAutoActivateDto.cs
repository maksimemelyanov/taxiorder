﻿namespace TaxiOrderDriver.Application.Contracts;

public class DriverBaseAutoActivateDto
{
    public int? Result { get; set; }
}