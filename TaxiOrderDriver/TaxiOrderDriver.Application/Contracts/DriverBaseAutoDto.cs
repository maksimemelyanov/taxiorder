﻿using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderDriver.Application.Contracts;

public class DriverBaseAutoDto : IEntity<int>
{
    public int Id { get; set; }
    
    public int DriverId { get; set; }
    
    public int BaseId { get; set; }
    
    public bool IsActive { get; set; }
    
    public int DriverTariffId { get; set; }
    
    public int AutoId { get; set; }
}