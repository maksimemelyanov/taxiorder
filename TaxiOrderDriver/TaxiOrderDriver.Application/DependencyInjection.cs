using Microsoft.Extensions.DependencyInjection;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Application.Services.Implementations;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderDriver.Application.Caches.Implementations;
using TaxiOrderDriver.Application.Caches.Interfaces;
using TaxiOrderDriver.Application.Contracts;
using TaxiOrderDriver.Application.Services.FindDriver.Implementations;
using TaxiOrderDriver.Application.Services.FindDriver.Interfaces;
using TaxiOrderDriver.Application.Services.Implementations;
using TaxiOrderDriver.Application.Services.Interfaces;
using TaxiOrderDriver.Infrastructure.Entities;

namespace TaxiOrderDriver.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplication(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddTransient<ICrudService<DriverBaseAutoDto, int>, CrudService<DriverBaseAutoDto, DriverBaseAuto, int>>();
        serviceCollection.AddTransient<ICrudService<DriverDto, int>, CrudService<DriverDto, Driver, int>>();
        serviceCollection.AddTransient<ICrudService<AutoDto, int>, CrudService<AutoDto, Auto, int>>();
        serviceCollection.AddTransient<ICrudReadService<DriverDto, int>, CrudReadService<DriverDto, Driver, int>>();
        serviceCollection.AddTransient<ICrudReadService<RegionDto, int>, CrudReadService<RegionDto, Region, int>>();
        serviceCollection.AddTransient<ICrudReadService<BaseDto, int>, CrudReadService<BaseDto, Base, int>>();
        serviceCollection.AddTransient<ICrudReadService<AutoBrandDto, int>, CrudReadService<AutoBrandDto, AutoBrand, int>>();
        serviceCollection.AddTransient<ICrudReadService<AutoCategoryDto, int>, CrudReadService<AutoCategoryDto, AutoCategory, int>>();
        serviceCollection.AddTransient<ICrudReadService<ColorDto, int>, CrudReadService<ColorDto, Color, int>>();
        serviceCollection.AddTransient<ICrudReadService<AutoModelDto, int>, CrudReadService<AutoModelDto, AutoModel, int>>();
        serviceCollection.AddTransient<IDriverBaseAutoActivateService, DriverBaseAutoActivateService>();
        serviceCollection.AddTransient<IDriverAutoInfoService, DriverAutoInfoService>();
        serviceCollection.AddSingleton<IOrderCache, OrderDecoratorCache>();

        serviceCollection.AddTransient<IFindDriverStrategy, FindDriverNewOrderStrategy>();
        serviceCollection.AddTransient<IFindDriverStrategy, FindDriverArrivedStrategy>();
        serviceCollection.AddTransient<IFindDriverStrategy, FindDriverStartedExecutionStrategy>();
        serviceCollection.AddTransient<IFindDriverStrategy, FindDriverFinishStrategy>();
        serviceCollection.AddSingleton<IFindDriverService, FindDriverService>();

        return serviceCollection;
    }
}