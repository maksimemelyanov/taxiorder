﻿using AutoMapper;
using TaxiOrderDriver.Application.Contracts;
using TaxiOrderDriver.Infrastructure.Entities;

namespace TaxiOrderDriver.Application.Mappings;


public class AutoMappingProfile : Profile
{
    public AutoMappingProfile()
    {
        CreateMap<AutoDto, Auto>();
        CreateMap<Auto, AutoDto>();
    }
}