using AutoMapper;
using TaxiOrderDriver.Application.Contracts;
using TaxiOrderDriver.Infrastructure.Entities;

namespace TaxiOrderDriver.Application.Mappings;

public class DriverAutoInfoMappingProfile : Profile
{
    public DriverAutoInfoMappingProfile()
    {
        CreateMap<DriverAutoInfoDto, DriverAutoInfo>();
        CreateMap<DriverAutoInfo, DriverAutoInfoDto>();
    }
}