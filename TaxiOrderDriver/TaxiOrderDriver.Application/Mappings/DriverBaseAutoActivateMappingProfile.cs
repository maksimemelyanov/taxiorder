﻿using AutoMapper;
using TaxiOrderDriver.Application.Contracts;
using TaxiOrderDriver.Infrastructure.Entities;

namespace TaxiOrderDriver.Application.Mappings;

public class DriverBaseAutoActivateMappingProfile : Profile
{
    public DriverBaseAutoActivateMappingProfile()
    {
        CreateMap<DriverBaseAutoActivateDto, DriverBaseAutoActivate>();
        CreateMap<DriverBaseAutoActivate, DriverBaseAutoActivateDto>();
    }
}