﻿using AutoMapper;
using TaxiOrderDriver.Application.Contracts;
using TaxiOrderDriver.Infrastructure.Entities;

namespace TaxiOrderDriver.Application.Mappings;

public class DriverBaseAutoMappingProfile : Profile
{
    public DriverBaseAutoMappingProfile()
    {
        CreateMap<DriverBaseAutoDto, DriverBaseAuto>();
        CreateMap<DriverBaseAuto, DriverBaseAutoDto>();
    }
}