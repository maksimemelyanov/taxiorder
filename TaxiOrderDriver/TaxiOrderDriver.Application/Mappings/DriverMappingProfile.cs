﻿using AutoMapper;
using TaxiOrderDriver.Application.Contracts;
using TaxiOrderDriver.Infrastructure.Entities;

namespace TaxiOrderDriver.Application.Mappings;

public class DriverMappingProfile : Profile
{
    public DriverMappingProfile()
    {
        CreateMap<DriverDto, Driver>();
        CreateMap<Driver, DriverDto>();
    }
}