using Microsoft.Extensions.Logging;
using TaxiOrderCore.Application;
using TaxiOrderCore.MessageBus.Messages;
using TaxiOrderDriver.Application.Services.FindDriver.Interfaces;

namespace TaxiOrderDriver.Application.Services.FindDriver.Implementations;

public class FindDriverArrivedStrategy : IFindDriverStrategy
{
    private readonly IFindDriverService _driverService;
    private readonly ILogger<FindDriverArrivedStrategy> _logger;

    public FindDriverArrivedStrategy(IFindDriverService driverService,
        ILogger<FindDriverArrivedStrategy> logger)
    {
        _driverService = driverService;
        _logger = logger;
    }

    public bool IsValid(OrderStatus status)
    {
        return status == OrderStatus.DriverArrived;
    }

    public Task DriverAssignment(FindDriverRequestMessage message)
    {
        _logger.LogInformation($"Обновление статуса на {OrderStatus.DriverArrived}");
        _driverService.DriverAssignment(message, OrderStatus.DriverArrived);
        return Task.CompletedTask;
    }
}