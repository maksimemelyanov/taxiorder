using Microsoft.Extensions.Logging;
using TaxiOrderCore.Application;
using TaxiOrderCore.MessageBus.Messages;
using TaxiOrderDriver.Application.Services.FindDriver.Interfaces;

namespace TaxiOrderDriver.Application.Services.FindDriver.Implementations;

public class FindDriverFinishStrategy : IFindDriverStrategy
{
    private readonly IFindDriverService _driverService;
    private readonly ILogger<FindDriverFinishStrategy> _logger;

    public FindDriverFinishStrategy(IFindDriverService driverService, ILogger<FindDriverFinishStrategy> logger)
    {
        _driverService = driverService;
        _logger = logger;
    }

    public bool IsValid(OrderStatus status)
    {
        return status == OrderStatus.Finish;
    }

    public Task DriverAssignment(FindDriverRequestMessage message)
    {
        _logger.LogInformation($"Обновление статуса на {OrderStatus.Finish}");
        _driverService.DriverAssignment(message, OrderStatus.Finish);
        return Task.CompletedTask;
    }
}