using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TaxiOrderCore.Application;
using TaxiOrderCore.MessageBus.Bus;
using TaxiOrderCore.MessageBus.Messages;
using TaxiOrderDriver.Application.Caches.Interfaces;
using TaxiOrderDriver.Application.Caches.Models;
using TaxiOrderDriver.Application.Contracts;
using TaxiOrderDriver.Application.Services.FindDriver.Interfaces;
using TaxiOrderDriver.Application.Services.Interfaces;
using TaxiOrderDriver.Infrastructure.Context;
using TaxiOrderDriver.Infrastructure.Repositories.Implementations;

namespace TaxiOrderDriver.Application.Services.FindDriver.Implementations;

public class FindDriverNewOrderStrategy : IFindDriverStrategy
{
    private readonly ILogger<FindDriverNewOrderStrategy> _logger;
    private readonly IMapper _mapper;
    private readonly IMessageBus _messageBus;
    private readonly IOrderCache _orderCache;
    private readonly IServiceProvider _serviceProvider;
    private readonly IDriverAutoInfoService _driverAutoInfoService;

    public FindDriverNewOrderStrategy(ILogger<FindDriverNewOrderStrategy> logger, IMapper mapper,
        IMessageBus messageBus, IOrderCache orderCache, IServiceProvider serviceProvider, IDriverAutoInfoService driverAutoInfoService)
    {
        _logger = logger;
        _mapper = mapper;
        _messageBus = messageBus;
        _orderCache = orderCache;
        _serviceProvider = serviceProvider;
        _driverAutoInfoService = driverAutoInfoService;
    }

    public bool IsValid(OrderStatus status)
    {
        return status == OrderStatus.NewOrder;
    }

    public async Task DriverAssignment(FindDriverRequestMessage message)
    {
        _logger.LogInformation($"Поиск водителя на заказ {message.OrderId}");
        
        var scope = _serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
        var driverService = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
        var driverBaseAutoRepository = new DriverBaseAutoRepository(driverService);

        var drivers = await driverBaseAutoRepository.GetAllAsync(CancellationToken.None);
        var selectedDriver = drivers.FirstOrDefault(_ => _.BaseId.Equals(message.BaseId) && _.IsActive);
        if (selectedDriver != null)
        {
            var selectedDriverDto = _mapper.Map<DriverBaseAutoDto>(selectedDriver);
            var driverAutoInfo = _driverAutoInfoService.GetDriverAutoInfo(selectedDriver.Id);
            if (driverAutoInfo != null)
            {
                var driverRepository = new DriverRepository(driverService);
                var driverInfo = await driverRepository.GetAsync(selectedDriver.DriverId);
                var driverDto = _mapper.Map<DriverDto>(driverInfo);
                var findDriverResponseMessage = new FindDriverResponseMessage
                {
                    OrderId = message.OrderId,
                    Status = OrderStatus.NewOrder,
                    DriverBaseAutoId = selectedDriver.Id,
                    DriverTariffId = selectedDriver.DriverTariffId,
                    DriverInfo =
                        $"К вам приедет {driverDto?.FirstName} на {driverAutoInfo.AutoBrand} {driverAutoInfo.AutoModel} цвет {driverAutoInfo.Color} номер {driverAutoInfo.StateNumber}"
                };
                var result = new Order
                {
                    OrderId = message.OrderId,
                    Status = message.Status,
                    DriverBaseAuto = selectedDriverDto,
                    DriverInfo = findDriverResponseMessage.DriverInfo,
                    Driver = driverDto
                };

                _orderCache.AddOrUpdate(result);
                _messageBus.Publish(findDriverResponseMessage);
                _logger.LogInformation($"Найден водитель id: {driverDto?.Id} имя: {driverDto?.FirstName}. Id заказа {message.OrderId}");
            }
        }
        else
        {
            _logger.LogError($"Не удалось найти свободного водителя для заказа {message.OrderId}");
        }
    }
}