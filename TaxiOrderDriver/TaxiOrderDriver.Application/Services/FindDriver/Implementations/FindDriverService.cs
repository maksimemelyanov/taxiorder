using Microsoft.Extensions.Logging;
using TaxiOrderCore.Application;
using TaxiOrderCore.MessageBus.Bus;
using TaxiOrderCore.MessageBus.Messages;
using TaxiOrderDriver.Application.Caches.Interfaces;
using TaxiOrderDriver.Application.Services.FindDriver.Interfaces;

namespace TaxiOrderDriver.Application.Services.FindDriver.Implementations;

public class FindDriverService : IFindDriverService
{
    private readonly ILogger<FindDriverService> _logger;
    private readonly IMessageBus _messageBus;
    private readonly IOrderCache _orderCache;

    public FindDriverService(ILogger<FindDriverService> logger, IMessageBus messageBus, IOrderCache orderCache)
    {
        _logger = logger;
        _messageBus = messageBus;
        _orderCache = orderCache;
    }

    public void DriverAssignment(FindDriverRequestMessage message, OrderStatus status)
    {
        if (!_orderCache.FindByKey(message.OrderId, out var order))
        {
            return;
        }

        _logger.LogInformation($"Обновление статуса по заказу {message.OrderId}");
        order.Status = status;
        _orderCache.AddOrUpdate(order);
        var findDriverResponseMessage = new FindDriverResponseMessage
        {
            OrderId = order.OrderId,
            Status = status,
            DriverBaseAutoId = order.DriverBaseAuto.Id,
            DriverTariffId = order.DriverBaseAuto.DriverTariffId,
            DriverInfo = order.DriverInfo
        };

        _messageBus.Publish(findDriverResponseMessage);
    }
}