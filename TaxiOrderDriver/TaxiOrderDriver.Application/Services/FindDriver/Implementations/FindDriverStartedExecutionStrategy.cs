using Microsoft.Extensions.Logging;
using TaxiOrderCore.Application;
using TaxiOrderCore.MessageBus.Messages;
using TaxiOrderDriver.Application.Services.FindDriver.Interfaces;

namespace TaxiOrderDriver.Application.Services.FindDriver.Implementations;

public class FindDriverStartedExecutionStrategy : IFindDriverStrategy
{
    private readonly IFindDriverService _driverService;
    private readonly ILogger<FindDriverStartedExecutionStrategy> _logger;

    public FindDriverStartedExecutionStrategy(IFindDriverService driverService,
        ILogger<FindDriverStartedExecutionStrategy> logger)
    {
        _driverService = driverService;
        _logger = logger;
    }

    public bool IsValid(OrderStatus status)
    {
        return status == OrderStatus.ClientOut;
    }

    public Task DriverAssignment(FindDriverRequestMessage message)
    {
        _logger.LogInformation($"Обновление статуса на {OrderStatus.StartedExecution}");
        _driverService.DriverAssignment(message, OrderStatus.StartedExecution);
        return Task.CompletedTask;
    }
}