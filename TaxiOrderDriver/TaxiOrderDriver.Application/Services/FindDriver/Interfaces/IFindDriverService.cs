using TaxiOrderCore.Application;
using TaxiOrderCore.MessageBus.Messages;

namespace TaxiOrderDriver.Application.Services.FindDriver.Interfaces;

public interface IFindDriverService
{
    void DriverAssignment(FindDriverRequestMessage message, OrderStatus status);
}