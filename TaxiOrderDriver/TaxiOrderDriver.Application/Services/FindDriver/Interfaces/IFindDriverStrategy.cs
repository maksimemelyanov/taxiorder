using TaxiOrderCore.Application;
using TaxiOrderCore.MessageBus.Messages;

namespace TaxiOrderDriver.Application.Services.FindDriver.Interfaces;

public interface IFindDriverStrategy
{
    bool IsValid(OrderStatus status);

    Task DriverAssignment(FindDriverRequestMessage message);
}