using AutoMapper;
using TaxiOrderDriver.Application.Contracts;
using TaxiOrderDriver.Application.Services.Interfaces;
using TaxiOrderDriver.Infrastructure.Entities;
using TaxiOrderDriver.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderDriver.Application.Services.Implementations;

public class DriverAutoInfoService : IDriverAutoInfoService
{
    private readonly IMapper _mapper;
    private readonly IDriverAutoInfoRepository _repository;

    public DriverAutoInfoService(IDriverAutoInfoRepository repository,
        IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    public DriverAutoInfoDto? GetDriverAutoInfo(int driverBaseAutoId)
    {
        var result = _repository.GetDriverAutoInfo(driverBaseAutoId);
        return result != null 
            ? _mapper.Map<DriverAutoInfo, DriverAutoInfoDto>(result) 
            : null;
    }
}