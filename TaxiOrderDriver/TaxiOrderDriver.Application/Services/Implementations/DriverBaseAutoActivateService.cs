﻿using AutoMapper;
using TaxiOrderDriver.Application.Contracts;
using TaxiOrderDriver.Application.Services.Interfaces;
using TaxiOrderDriver.Infrastructure.Entities;
using TaxiOrderDriver.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderDriver.Application.Services.Implementations;

public class DriverBaseAutoActivateService: IDriverBaseAutoActivateService
{
    private readonly IDriverBaseAutoActivateRepository _repository;
    private readonly IMapper _mapper;

    public DriverBaseAutoActivateService(IDriverBaseAutoActivateRepository repository,
        IMapper mapper)
    {
        _repository = repository;
        _mapper = mapper;
    }

    public DriverBaseAutoActivateDto DriverBaseAutoActivate(int driverId, int baseId, int autoId)
    {
        var result = _repository.DriverBaseAutoActivate(driverId, baseId, autoId);
        return _mapper.Map<DriverBaseAutoActivate, DriverBaseAutoActivateDto>(result);
    }
}