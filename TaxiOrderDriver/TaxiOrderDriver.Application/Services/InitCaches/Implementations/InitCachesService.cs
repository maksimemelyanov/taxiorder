﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TaxiOrderCore.Infrastructure.Caches.Interfaces;
using TaxiOrderDriver.Application.Services.InitCaches.Configurations;

namespace TaxiOrderDriver.Application.Services.InitCaches.Implementations;

public class InitCachesService : IHostedService
{
    private readonly CancellationTokenSource _cancellationTokenSource;
    private readonly ILogger<InitCachesService> _logger;
    private readonly CacheSettings _cacheSettings;
    private readonly IEnumerable<ICache> _caches;
    private Task _doWorkTask = null!;

    public InitCachesService(ILogger<InitCachesService> logger,
        IOptionsMonitor<CacheSettings> optionsMonitor, IEnumerable<ICache> caches)
    {
        _logger = logger;
        _cancellationTokenSource = new CancellationTokenSource();
        _cacheSettings = optionsMonitor.CurrentValue;
        _caches = caches;
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation($"{nameof(InitCachesService)} старт.");
        _doWorkTask = DoWorkAsync(_cancellationTokenSource.Token);
        return _doWorkTask.IsCompleted ? _doWorkTask : Task.CompletedTask;
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation($"{nameof(InitCachesService)} стоп.");
        if (_doWorkTask == null)
        {
            return;
        }
        try
        {
            _cancellationTokenSource.Cancel();
        }
        finally
        {
            await Task.WhenAny(_doWorkTask, Task.Delay(Timeout.Infinite, cancellationToken));
        }
    }

    private async Task DoWorkAsync(CancellationToken cancellationToken)
    {
        do
        {
            _logger.LogInformation("Инициализация кешей");
            try
            {
                var tasks = _caches.Select(cache => cache.Init()).ToList();
                await Task.WhenAll(tasks);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Ошибка при инициализации кешей");
            }
            await Task.Delay((int)_cacheSettings.ReloadTimeout.TotalMilliseconds, cancellationToken).ConfigureAwait(false);
        } while (!cancellationToken.IsCancellationRequested);
    }
}