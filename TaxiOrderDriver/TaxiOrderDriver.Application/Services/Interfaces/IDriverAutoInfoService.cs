using TaxiOrderDriver.Application.Contracts;

namespace TaxiOrderDriver.Application.Services.Interfaces;

public interface IDriverAutoInfoService
{
    DriverAutoInfoDto? GetDriverAutoInfo(int driverBaseAutoId);
}