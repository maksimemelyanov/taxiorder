﻿using TaxiOrderDriver.Application.Contracts;

namespace TaxiOrderDriver.Application.Services.Interfaces;

public interface IDriverBaseAutoActivateService
{
    DriverBaseAutoActivateDto DriverBaseAutoActivate(int driverId, int baseId, int autoId);
}