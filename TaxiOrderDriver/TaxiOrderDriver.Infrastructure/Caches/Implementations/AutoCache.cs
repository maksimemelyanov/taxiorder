﻿using System.Collections.Concurrent;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TaxiOrderCore.Infrastructure.Caches.Implementations;
using TaxiOrderDriver.Infrastructure.Context;
using TaxiOrderDriver.Infrastructure.Entities;
using TaxiOrderDriver.Infrastructure.Repositories.Implementations;

namespace TaxiOrderDriver.Infrastructure.Caches.Implementations;

public class AutoCache : GenericCache<Auto, int>
{
    private readonly IServiceProvider _provider;
    private readonly ILogger<GenericCache<Auto, int>> _logger;

    public AutoCache(ILogger<GenericCache<Auto, int>> logger, IServiceProvider provider)
    {
        _provider = provider;
        _logger = logger;
    }

    public override async Task Init()
    {
        try
        {
            var scope = _provider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            await using var context = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
            var repository = new AutoReadRepository(context);
            var result = await repository.GetAllAsync(CancellationToken.None);
            Cache = new ConcurrentDictionary<int, Auto>(result.ToDictionary(item => item.Id));
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, InitCacheError);
            Cache = new ConcurrentDictionary<int, Auto>();
            throw;
        }
    }
}