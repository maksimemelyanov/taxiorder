﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderDriver.Infrastructure.Entities;

namespace TaxiOrderDriver.Infrastructure.Context;

public class DatabaseContext : DbContext
{
    public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
    {
    }

    public DbSet<DriverBaseAuto> DriverBaseAutos { get; set; }

    public DbSet<Auto> Autos { get; set; }

    public DbSet<Driver> Drivers { get; set; }

    public DbSet<Color> Colors { get; set; }

    public DbSet<AutoCategory> AutoCategories { get; set; }

    public DbSet<Base> Bases { get; set; }

    public DbSet<AutoBrand> AutoBrands { get; set; }

    public DbSet<Region> Regions { get; set; }

    public DbSet<AutoModel> AutoModels { get; set; }

    public DbSet<DriverBaseAutoActivate> DriverBaseAutoActivates { get; set; }

    public DbSet<DriverAutoInfo> DriverAutoInfos { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.Entity<DriverBaseAutoActivate>().HasNoKey();
        modelBuilder.Entity<DriverAutoInfo>().HasNoKey();
    }
}