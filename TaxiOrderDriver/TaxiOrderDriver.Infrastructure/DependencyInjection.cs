using Microsoft.Extensions.DependencyInjection;
using TaxiOrderCore.Infrastructure;
using TaxiOrderCore.Infrastructure.Caches;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderDriver.Infrastructure.Caches.Implementations;
using TaxiOrderDriver.Infrastructure.Entities;
using TaxiOrderDriver.Infrastructure.Repositories.Implementations;
using TaxiOrderDriver.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderDriver.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddInfrastructureCore();
        serviceCollection
            .AddTransient<IRepository<DriverBaseAuto, int>, DriverBaseAutoRepository>()
            .AddTransient<IRepository<Auto, int>, AutoRepository>()
            .AddTransient<IRepository<Driver, int>, DriverRepository>()
            .AddTransient<IReadRepository<Auto, int>, AutoReadRepository>()
            .AddTransient<IReadRepository<DriverBaseAuto, int>, DriverBaseAutoReadRepository>()
            .AddTransient<IReadRepository<Driver, int>, DriverReadRepository>()
            .AddTransient<IDriverAutoInfoRepository, DriverAutoInfoRepository>()
            .AddTransient<IDriverBaseAutoActivateRepository, DriverBaseAutoActivateRepository>();

        serviceCollection.AddCache<int, Driver, DriverCache>();
        serviceCollection.AddCache<int, Auto, AutoCache>();
        serviceCollection.AddCache<int, DriverBaseAuto, DriverBaseAutoCache>();

        return serviceCollection;
    }
}