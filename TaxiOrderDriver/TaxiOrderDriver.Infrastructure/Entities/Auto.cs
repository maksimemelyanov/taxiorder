﻿using System.ComponentModel.DataAnnotations.Schema;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderDriver.Infrastructure.Entities;

[Table("auto", Schema = "main")]
public class Auto : IEntity<int>
{
    [Column("id")]
    public int Id { get; set; }
    
    [Column("id_auto_model")]
    public int AutoModelId { get; set; }
    
    [Column("id_color")]
    public int ColorId { get; set; }
    
    [Column("id_auto_category")]
    public int AutoCategoryId { get; set; }

    [Column("state_number")]
    public string StateNumber { get; set; } = null!;
    
    [Column("create_year")]
    public int CreateYear { get; set; }
    
    [Column("seat_count")]
    public int SeatCount { get; set; }
    
    [Column("vin")]
    public string Vin { get; set; } = null!;
    
    [Column("id_driver")]
    public int DriverId { get; set; }
}