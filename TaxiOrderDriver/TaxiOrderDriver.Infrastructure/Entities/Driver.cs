﻿using System.ComponentModel.DataAnnotations.Schema;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderDriver.Infrastructure.Entities;

/// <summary>
///     Водитель
/// </summary>
[Table("driver", Schema = "main")]
public class Driver : IEntity<int>
{
    /// <summary>
    ///     Идентификатор
    /// </summary>
    [Column("id")]
    public int Id { get; set; }
    
    /// <summary>
    ///     ФИО водителя
    /// </summary>
    [Column("name")]
    public string Name { get; set; } = null!;
    
    /// <summary>
    ///     Имя водителя
    /// </summary>
    [Column("first_name")]
    public string FirstName { get; set; } = null!;
    
    /// <summary>
    ///     Фамилия водителя
    /// </summary>
    [Column("last_name")]
    public string LastName { get; set; } = null!;
    
    /// <summary>
    ///     Отчество водителя
    /// </summary>
    [Column("patronymic")]
    public string Patronymic { get; set; } = null!;
    
    /// <summary>
    ///     Номер телефона
    /// </summary>
    [Column("phone")]
    public string Phone { get; set; } = null!;
    
    /// <summary>
    ///     Дата рождения
    /// </summary>
    [Column("birthdate")]
    public DateTime Birthdate { get; set; }
    
    /// <summary>
    ///     Пол водителя
    /// </summary>
    [Column("is_man")]
    public bool IsMan { get; set; }
    
    /// <summary>
    ///     Электронная почта
    /// </summary>
    [Column("email")]
    public string Email { get; set; } = null!;
    
    /// <summary>
    ///     Флаг не заблокированного водителя
    /// </summary>
    [Column("is_active")]
    public bool IsActive { get; set; }

    /// <summary>
    ///     Автомобиль по умолчанию
    /// </summary>
    [Column("id_active_auto")]
    public int ActiveAutoId { get; set; }
    
    /// <summary>
    ///     Подразделение по умолчанию
    /// </summary>
    [Column("id_active_base")]
    public int ActiveBaseId { get; set; }
    
    /// <summary>
    ///     Тариф водителя по умолчанию
    /// </summary>
    [Column("id_active_driver_tariff")]
    public int ActiveDriverTariffId { get; set; }
}