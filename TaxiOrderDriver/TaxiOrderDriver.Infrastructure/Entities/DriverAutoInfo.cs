using System.ComponentModel.DataAnnotations.Schema;

namespace TaxiOrderDriver.Infrastructure.Entities;

public class DriverAutoInfo
{
    [Column("first_name")]
    public string Driver { get; set; }
    
    [Column("auto_brand_name")]
    public string AutoBrand { get; set; }
    
    [Column("auto_model_name")]
    public string AutoModel { get; set; }
    
    [Column("color_name")]
    public string Color { get; set; }
    
    [Column("state_number")]
    public string StateNumber { get; set; }
}