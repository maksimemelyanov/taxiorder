﻿using System.ComponentModel.DataAnnotations.Schema;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderDriver.Infrastructure.Entities;

[Table("driver_base_auto", Schema = "main")]
public class DriverBaseAuto : IEntity<int>
{
    [Column("id")]
    public int Id { get; set; }

    [Column("id_driver")]
    public int DriverId { get; set; }

    [Column("id_base")]
    public int BaseId { get; set; }
    
    [Column("is_active")]
    public bool IsActive { get; set; }
    
    [Column("id_driver_tariff")]
    public int DriverTariffId { get; set; }
    
    [Column("id_auto")]
    public int AutoId { get; set; }
}