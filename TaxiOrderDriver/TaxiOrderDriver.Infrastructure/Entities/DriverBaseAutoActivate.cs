﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TaxiOrderDriver.Infrastructure.Entities;

public class DriverBaseAutoActivate
{
    [Column("result")]
    public int? Result { get; set; }
}