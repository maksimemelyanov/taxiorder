﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderDriver.Infrastructure.Entities;

namespace TaxiOrderDriver.Infrastructure.Repositories.Implementations;

public class AutoReadRepository : ReadRepository<Auto, int>, IReadRepository<Auto, int>
{
    public AutoReadRepository(DbContext context) : base(context)
    {
    }
}