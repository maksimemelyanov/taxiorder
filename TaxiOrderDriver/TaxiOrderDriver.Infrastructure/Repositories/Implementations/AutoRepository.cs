﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderDriver.Infrastructure.Entities;

namespace TaxiOrderDriver.Infrastructure.Repositories.Implementations;

public class AutoRepository : Repository<Auto, int>, IRepository<Auto, int>
{
    public AutoRepository(DbContext context) : base(context)
    {
    }
}