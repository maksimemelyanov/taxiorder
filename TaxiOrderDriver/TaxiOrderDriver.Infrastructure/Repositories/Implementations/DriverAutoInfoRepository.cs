using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TaxiOrderDriver.Infrastructure.Context;
using TaxiOrderDriver.Infrastructure.Entities;
using TaxiOrderDriver.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderDriver.Infrastructure.Repositories.Implementations;

public class DriverAutoInfoRepository : IDriverAutoInfoRepository
{
    private readonly IServiceProvider _serviceProvider;

    public DriverAutoInfoRepository(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public DriverAutoInfo? GetDriverAutoInfo(int driverBaseAutoId)
    {
        var scope = _serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
        var driverService = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
        var result = driverService
            .DriverAutoInfos
            .FromSqlRaw($"select * from main.get_driver_auto_info({driverBaseAutoId})")
            .FirstOrDefault();
        return result;
    }
}