﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderDriver.Infrastructure.Context;
using TaxiOrderDriver.Infrastructure.Entities;
using TaxiOrderDriver.Infrastructure.Repositories.Interfaces;

namespace TaxiOrderDriver.Infrastructure.Repositories.Implementations;

public class DriverBaseAutoActivateRepository : IDriverBaseAutoActivateRepository
{
    private readonly DbContext _context;


    public DriverBaseAutoActivateRepository(DbContext context)
    {
        _context = context;
    }

    public DriverBaseAutoActivate DriverBaseAutoActivate(int driverId, int baseId, int autoId)
    {
        var result = ((DatabaseContext) _context)
            .DriverBaseAutoActivates
            .FromSqlRaw($"select * from main.driver_base_auto_activate({driverId},{baseId},{autoId})")
            .FirstOrDefault();
        return result ?? new DriverBaseAutoActivate { Result = null };
    }
}