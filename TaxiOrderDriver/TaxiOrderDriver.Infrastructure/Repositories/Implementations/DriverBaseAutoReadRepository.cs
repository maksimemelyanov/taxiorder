using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderDriver.Infrastructure.Entities;

namespace TaxiOrderDriver.Infrastructure.Repositories.Implementations;

public class DriverBaseAutoReadRepository : ReadRepository<DriverBaseAuto, int>, IReadRepository<DriverBaseAuto, int>
{
    public DriverBaseAutoReadRepository(DbContext context) : base(context)
    {
    }
}