﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderDriver.Infrastructure.Entities;

namespace TaxiOrderDriver.Infrastructure.Repositories.Implementations;

public class DriverBaseAutoRepository : Repository<DriverBaseAuto, int>, IRepository<DriverBaseAuto, int>
{
    public DriverBaseAutoRepository(DbContext context) : base(context)
    {
    }
}