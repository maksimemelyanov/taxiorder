﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderDriver.Infrastructure.Entities;

namespace TaxiOrderDriver.Infrastructure.Repositories.Implementations;

public class DriverReadRepository : ReadRepository<Driver, int>, IReadRepository<Driver, int>
{
    public DriverReadRepository(DbContext context) : base(context)
    {
    }
}