﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderDriver.Infrastructure.Entities;

namespace TaxiOrderDriver.Infrastructure.Repositories.Implementations;

/// <summary>
///     Репозиторий водителя
/// </summary>
public class DriverRepository : Repository<Driver, int>, IRepository<Driver, int>
{
    /// <summary>
    ///     Конструктор
    /// </summary>
    /// <param name="context">Контекст</param>
    public DriverRepository(DbContext context) : base(context)
    {
    }
}