using TaxiOrderDriver.Infrastructure.Entities;

namespace TaxiOrderDriver.Infrastructure.Repositories.Interfaces;

public interface IDriverAutoInfoRepository
{
    DriverAutoInfo? GetDriverAutoInfo(int driverBaseAutoId);
}