﻿using TaxiOrderDriver.Infrastructure.Entities;

namespace TaxiOrderDriver.Infrastructure.Repositories.Interfaces;

public interface IDriverBaseAutoActivateRepository
{
    DriverBaseAutoActivate DriverBaseAutoActivate(int driverId, int baseId, int autoId);
}