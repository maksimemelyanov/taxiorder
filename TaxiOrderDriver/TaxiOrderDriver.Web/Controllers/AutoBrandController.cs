﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderDriver.Web.Controllers;

public class AutoBrandController : GenericCrudReadController<AutoBrandDto, AutoBrandModel, int>
{
    public AutoBrandController(ICrudReadService<AutoBrandDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}