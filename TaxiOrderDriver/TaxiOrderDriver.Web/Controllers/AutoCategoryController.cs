﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderDriver.Web.Controllers;

public class AutoCategoryController : GenericCrudReadController<AutoCategoryDto, AutoCategoryModel, int>
{
    public AutoCategoryController(ICrudReadService<AutoCategoryDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}