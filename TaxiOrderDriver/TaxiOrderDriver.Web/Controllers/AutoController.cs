﻿using AutoMapper;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderDriver.Application.Contracts;
using TaxiOrderDriver.Web.Models;

namespace TaxiOrderDriver.Web.Controllers;

public class AutoController : GenericCrudController<AutoDto, AutoModel, int>
{
    public AutoController(ICrudService<AutoDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}