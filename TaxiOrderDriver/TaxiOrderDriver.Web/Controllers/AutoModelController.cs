﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderDriver.Web.Controllers;

public class AutoModelController : GenericCrudReadController<AutoModelDto, AutoModelModel, int>
{
    public AutoModelController(ICrudReadService<AutoModelDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}