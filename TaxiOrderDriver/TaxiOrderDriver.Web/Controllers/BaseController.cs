﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderDriver.Web.Controllers;

public class BaseController : GenericCrudReadController<BaseDto, BaseModel, int>
{
    public BaseController(ICrudReadService<BaseDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}