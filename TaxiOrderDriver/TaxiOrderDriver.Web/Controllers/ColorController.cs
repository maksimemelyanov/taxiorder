﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderDriver.Web.Controllers;

public class ColorController : GenericCrudReadController<ColorDto, ColorModel, int>
{
    public ColorController(ICrudReadService<ColorDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}