﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderDriver.Application.Contracts;
using TaxiOrderDriver.Application.Services.Interfaces;
using TaxiOrderDriver.Web.Models;

namespace TaxiOrderDriver.Web.Controllers;

public class DriverBaseAutoController : GenericCrudController<DriverBaseAutoDto, DriverBaseAutoModel, int>
{
    private readonly IDriverBaseAutoActivateService _activateService;
    private readonly IMapper _mapper;

    public DriverBaseAutoController(ICrudService<DriverBaseAutoDto, int> service, IMapper mapper,
        IDriverBaseAutoActivateService activateService) : base(service, mapper)
    {
        _activateService = activateService;
        _mapper = mapper;
    }

    [HttpGet("activation")]
    public DriverBaseAutoActivateModel Get([FromQuery(Name = ApiConstants.DriverId)][BindRequired] int driverId,
        [FromQuery(Name = ApiConstants.BaseId)][BindRequired] int baseId,
        [FromQuery(Name = ApiConstants.AutoId)][BindRequired] int autoId)
    {
        return _mapper.Map<DriverBaseAutoActivateModel>(_activateService
            .DriverBaseAutoActivate(driverId, baseId, autoId));
    }
}