﻿using AutoMapper;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderDriver.Application.Contracts;
using TaxiOrderDriver.Web.Models;

namespace TaxiOrderDriver.Web.Controllers;

public class DriverController : GenericCrudController<DriverDto, DriverModel, int>
{
    public DriverController(ICrudService<DriverDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}