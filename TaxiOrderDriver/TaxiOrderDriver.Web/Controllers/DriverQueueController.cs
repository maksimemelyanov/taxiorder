using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using TaxiOrderCore.Application;
using TaxiOrderCore.MessageBus.Bus;
using TaxiOrderCore.MessageBus.Messages;
using TaxiOrderCore.Web;

namespace TaxiOrderDriver.Web.Controllers;

[ApiController]
[Route("api/[controller]")]
public class DriverQueueController : ControllerBase
{
    private readonly IMessageBus _messageBus;

    public DriverQueueController(IMessageBus messageBus)
    {
        _messageBus = messageBus;
    }

    [HttpPost("find-driver")]
    public IActionResult FindDriver(
        [FromQuery(Name = ApiConstants.OrderId)] [BindRequired]
        int orderId,
        [FromQuery(Name = ApiConstants.BaseId)] [BindRequired]
        int baseId)
    {
        var result = new FindDriverRequestMessage
        {
            OrderId = orderId,
            Status = OrderStatus.NewOrder,
            BaseId = baseId
        };
        _messageBus.Publish(result);
        return Ok();
    }
    
    [HttpPut("update-status")]
    public IActionResult UpdateStatus(
        [FromQuery(Name = ApiConstants.OrderId)] [BindRequired]
        int orderId,
        [FromQuery(Name = ApiConstants.BaseId)] [BindRequired]
        int baseId,
        [FromQuery(Name = ApiConstants.Status)] [BindRequired]
        OrderStatus status)
    {
        var result = new FindDriverRequestMessage
        {
            OrderId = orderId,
            Status = status,
            BaseId = baseId
        };
        _messageBus.Publish(result);
        return Ok();
    }
}