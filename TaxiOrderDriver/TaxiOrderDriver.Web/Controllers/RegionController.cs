﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderDriver.Web.Controllers;

public class RegionController : GenericCrudReadController<RegionDto, RegionModel, int>
{
    public RegionController(ICrudReadService<RegionDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}