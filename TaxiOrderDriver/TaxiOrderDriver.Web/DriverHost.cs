using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.MessageBus.Bus;
using TaxiOrderCore.MessageBus.Messages;
using TaxiOrderCore.MessageBus.RabbitMQ.Extensions;
using TaxiOrderCore.Web.Mapping;
using TaxiOrderDriver.Application;
using TaxiOrderDriver.Application.Services.InitCaches.Implementations;
using TaxiOrderDriver.Infrastructure;
using TaxiOrderDriver.Infrastructure.Context;
using TaxiOrderDriver.Web.Handlers;
using TaxiOrderDriver.Web.Mappings;

namespace TaxiOrderDriver.Web;

public class DriverHost
{
    private const string ConfigurationFileName = "appsettings.json";
    private readonly WebApplicationBuilder _builder;

    private DriverHost(string[] args)
    {
        _builder = WebApplication.CreateBuilder(args);
    }

    public static DriverHost CreateHost(string[] args)
    {
        return new DriverHost(args);
    }

    public DriverHost ConfigureHost()
    {
        _builder.Host
            .ConfigureAppConfiguration(builder =>
            {
                builder.AddJsonFile(ConfigurationFileName);
                builder.AddUserSecrets<DriverHost>(true, true);
            });

        return this;
    }

    public DriverHost ConfigureSwaggerService()
    {
        _builder.Services.AddEndpointsApiExplorer();
        _builder.Services.AddSwaggerGen();
        return this;
    }

    public DriverHost ConfigureServices()
    {
        _builder.Services.AddControllers();
        _builder.Services
            .AddScoped(typeof(DbContext), typeof(DatabaseContext))
            .AddDbContext<DatabaseContext>(x => { x.UseNpgsql(_builder.Configuration.GetConnectionString("db")); })
            .AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()))
            .AddInfrastructure()
            .AddApplication();

        _builder.Services.AddHostedService<InitCachesService>();
        return this;
    }
    
    private MapperConfiguration GetMapperConfiguration()
    {
        var configuration = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<TaxiOrderCore.Application.Mappings.AutoCategoryMappingProfile>();
            cfg.AddProfile<AutoCategoryMappingProfile>();
            cfg.AddProfile<TaxiOrderCore.Application.Mappings.BaseMappingProfile>();
            cfg.AddProfile<BaseMappingProfile>();
            cfg.AddProfile<TaxiOrderCore.Application.Mappings.RegionMappingProfile>();
            cfg.AddProfile<RegionMappingProfile>();
            cfg.AddProfile<TaxiOrderDriver.Application.Mappings.AutoMappingProfile>();
            cfg.AddProfile<AutoMappingProfile>();
            cfg.AddProfile<TaxiOrderDriver.Application.Mappings.DriverMappingProfile>();
            cfg.AddProfile<DriverMappingProfile>();
            cfg.AddProfile<TaxiOrderDriver.Application.Mappings.DriverBaseAutoMappingProfile>();
            cfg.AddProfile<DriverBaseAutoMappingProfile>();
            cfg.AddProfile<TaxiOrderCore.Application.Mappings.ColorMappingProfile>();
            cfg.AddProfile<ColorMappingProfile>();
            cfg.AddProfile<TaxiOrderCore.Application.Mappings.AutoModelMappingProfile>();
            cfg.AddProfile<AutoModelMappingProfile>();
            cfg.AddProfile<TaxiOrderCore.Application.Mappings.AutoBrandMappingProfile>();
            cfg.AddProfile<AutoBrandMappingProfile>();
            cfg.AddProfile<TaxiOrderDriver.Application.Mappings.DriverBaseAutoActivateMappingProfile>();
            cfg.AddProfile<DriverBaseAutoActivateMappingProfile>();
            cfg.AddProfile<TaxiOrderDriver.Application.Mappings.DriverAutoInfoMappingProfile>();
        });
        configuration.AssertConfigurationIsValid();
        return configuration;
    }

    public async Task RunHost()
    {
        var webApplication = _builder.Build();

        var useSwagger = _builder.Configuration.GetValue<bool>("useSwagger");
        if (useSwagger)
            webApplication
                .UseSwagger()
                .UseSwaggerUI();

        webApplication
            .UseRouting()
            .UseHttpsRedirection()
            .UseRequestLocalization()
            .UseSwagger()
            .UseSwaggerUI()
            .UseAuthorization();

        webApplication.UseEndpoints(endpoints =>
        {
            endpoints
                .MapControllers();
        });

        ConfigureMessageBusHandlers(webApplication);
        await webApplication.RunAsync();
    }

    public DriverHost ConfigureMessageBusDependencies()
    {
        var rabbitMQSection = _builder.Configuration.GetSection("RabbitMQ");
        _builder.Services.AddRabbitMQMessageBus
        (
            host: rabbitMQSection["Host"],
            exchangeName: rabbitMQSection["Exchange"],
            queueName: rabbitMQSection["Queue"],
            timeoutBeforeReconnecting: 15
        );

        _builder.Services.AddTransient<FindDriverRequestMessageHandler>();

        return this;
    }

    private void ConfigureMessageBusHandlers(IApplicationBuilder app)
    {
        var messageBus = app.ApplicationServices.GetRequiredService<IMessageBus>();

        messageBus.Subscribe<FindDriverRequestMessage, FindDriverRequestMessageHandler>();
    }
}