﻿using TaxiOrderCore.MessageBus.Messages;
using TaxiOrderDriver.Application.Services.FindDriver.Interfaces;

namespace TaxiOrderDriver.Web.Handlers;

public class FindDriverRequestMessageHandler : IMessageHandler<FindDriverRequestMessage>
{
    private readonly ILogger<FindDriverRequestMessageHandler> _logger;
    private readonly List<IFindDriverStrategy> _strategies;

    public FindDriverRequestMessageHandler(IEnumerable<IFindDriverStrategy> strategies,
        ILogger<FindDriverRequestMessageHandler> logger)
    {
        _logger = logger;
        _strategies = strategies.ToList();
    }

    public async Task HandleAsync(FindDriverRequestMessage message)
    {
        var strategy = _strategies.Find(strategy => strategy.IsValid(message.Status));
        if (strategy != null)
        {
            await strategy.DriverAssignment(message);
        }
        else
        {
            _logger.LogError("Не удалось определить статус заказа");   
        }
    }
}