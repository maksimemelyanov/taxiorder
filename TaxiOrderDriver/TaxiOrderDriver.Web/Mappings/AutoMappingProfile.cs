﻿using AutoMapper;
using TaxiOrderDriver.Application.Contracts;
using TaxiOrderDriver.Web.Models;

namespace TaxiOrderDriver.Web.Mappings;

public class AutoMappingProfile : Profile
{
    public AutoMappingProfile()
    {
        CreateMap<AutoDto, AutoModel>();
        CreateMap<AutoModel, AutoDto>();
    }
}