﻿using AutoMapper;
using TaxiOrderDriver.Application.Contracts;
using TaxiOrderDriver.Web.Models;

namespace TaxiOrderDriver.Web.Mappings;

public class DriverBaseAutoActivateMappingProfile : Profile
{
    public DriverBaseAutoActivateMappingProfile()
    {
        CreateMap<DriverBaseAutoActivateDto, DriverBaseAutoActivateModel>();
        CreateMap<DriverBaseAutoActivateModel, DriverBaseAutoActivateDto>();
    }
}