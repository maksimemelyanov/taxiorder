﻿using AutoMapper;
using TaxiOrderDriver.Application.Contracts;
using TaxiOrderDriver.Web.Models;

namespace TaxiOrderDriver.Web.Mappings;

public class DriverBaseAutoMappingProfile : Profile
{
    public DriverBaseAutoMappingProfile()
    {
        CreateMap<DriverBaseAutoDto, DriverBaseAutoModel>();
        CreateMap<DriverBaseAutoModel, DriverBaseAutoDto>();
    }
}