﻿using AutoMapper;
using TaxiOrderDriver.Application.Contracts;
using TaxiOrderDriver.Web.Models;

namespace TaxiOrderDriver.Web.Mappings;

public class DriverMappingProfile : Profile
{
    public DriverMappingProfile()
    {
        CreateMap<DriverDto, DriverModel>();
        CreateMap<DriverModel, DriverDto>();
    }
}