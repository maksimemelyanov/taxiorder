﻿namespace TaxiOrderDriver.Web.Models;

public class DriverModel
{
    public int? Id { get; set; }
    
    public string Name { get; set; } = null!;
    
    public string FirstName { get; set; } = null!;
    
    public string LastName { get; set; } = null!;
    
    public string Patronymic { get; set; } = null!;
    
    public string Phone { get; set; } = null!;
    
    public DateTime Birthdate { get; set; }
    
    public bool IsMan { get; set; }
    
    public string Email { get; set; } = null!;
    
    public bool IsActive { get; set; }
    
    public int ActiveAutoId { get; set; }
    
    public int ActiveBaseId { get; set; }
    
    public int ActiveDriverTariffId { get; set; }
}