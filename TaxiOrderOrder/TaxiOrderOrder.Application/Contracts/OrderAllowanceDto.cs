﻿using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderOrder.Application.Contracts
{
    public class OrderAllowanceDto : IEntity<int>
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int AllowanceId {get; set;}
    }
}
