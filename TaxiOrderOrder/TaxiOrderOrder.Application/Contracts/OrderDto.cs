﻿using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderOrder.Application.Contracts
{
    public class OrderDto : IEntity<int>
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public int ClientId { get; set; }
        public string Phone { get; set; }
        public int TariffId { get; set; }
        public decimal TotalSum { get; set; }
        public int OrderStatus { get; set; }
        public decimal DriverSum { get; set; }
        public string Allowance { get; set; }
        public string Addresses { get; set; }
        public string Comment { get; set; }
        public int DriverBaseAutoId { get; set; }
        public int BaseId { get; set; }
    }
}
