﻿using Microsoft.Extensions.DependencyInjection;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Application.Services.Implementations;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderOrder.Application.Contracts;
using TaxiOrderOrder.Infrastructure.Entities;

namespace TaxiOrderOrder.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ICrudService<OrderAllowanceDto, int>, CrudService<OrderAllowanceDto, OrderAllowance, int>>();
            serviceCollection.AddTransient<ICrudService<OrderDto, int>, CrudService<OrderDto, Order, int>>();
            serviceCollection.AddTransient<ICrudReadService<OrderStatusDto, int>, CrudReadService<OrderStatusDto, OrderStatus, int>>();
            serviceCollection.AddTransient<ICrudReadService<OrderAllowanceDto, int>, CrudReadService<OrderAllowanceDto, OrderAllowance, int>>();
            serviceCollection.AddTransient<ICrudReadService<OrderDto, int>, CrudReadService<OrderDto, Order, int>>();
            serviceCollection.AddTransient<ICrudReadService<AllowanceTypeDto, int>, CrudReadService<AllowanceTypeDto, AllowanceType, int>>();
            serviceCollection.AddTransient<ICrudReadService<AutoCategoryDto, int>, CrudReadService<AutoCategoryDto, AutoCategory, int>>();
            serviceCollection.AddTransient<ICrudReadService<BaseDto, int>, CrudReadService<BaseDto, Base, int>>();
            serviceCollection.AddTransient<ICrudReadService<DriverTariffTypeDto, int>, CrudReadService<DriverTariffTypeDto, DriverTariffType, int>>();
            serviceCollection.AddTransient<ICrudReadService<RegionDto, int>, CrudReadService<RegionDto, Region, int>>();
            serviceCollection.AddTransient<ICrudReadService<TariffTypeDto, int>, CrudReadService<TariffTypeDto, TariffType, int>>();
            return serviceCollection;
        }
    }
}
