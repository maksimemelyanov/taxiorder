﻿using AutoMapper;
using TaxiOrderOrder.Infrastructure.Entities;
using TaxiOrderOrder.Application.Contracts;

namespace TaxiOrderOrder.Application.Mappings
{
    public class OrderAllowanceMappingProfile : Profile
    {
        public OrderAllowanceMappingProfile()
        {
            CreateMap<OrderAllowanceDto, OrderAllowance>();
            CreateMap<OrderAllowance, OrderAllowanceDto>();
        }
    }
}
