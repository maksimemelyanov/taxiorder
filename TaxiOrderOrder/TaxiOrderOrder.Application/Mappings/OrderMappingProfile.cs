﻿using AutoMapper;
using TaxiOrderOrder.Application.Contracts;
using TaxiOrderOrder.Infrastructure.Entities;

namespace TaxiOrderOrder.Application.Mappings
{
    public class OrderMappingProfile : Profile
    {
        public OrderMappingProfile()
        {
            CreateMap<OrderDto, Order>();
            CreateMap<Order, OrderDto>();
        }
    }
}
