﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderOrder.Application.Contracts;
using TaxiOrderOrder.Application.Services.Interfaces;
using TaxiOrderOrder.Infrastructure.Entities;
using TaxiOrderOrder.Infrastructure.Repositories;
using TaxiOrderOrder.Infrastructure.Repositories.Implementations;

namespace TaxiOrderOrder.Application.Services.Implementations
{
    public class OrderService : IOrderService
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Order, int> _repository;

        public OrderService(IRepository<Order, int> repository,
            IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public int CreateOrder(OrderDto orderDto)
        {
            var order = _mapper.Map<OrderDto, Order>(orderDto);
            order = _repository.Add(order);
            _repository.SaveChangesAsync();
            return order.Id;
        }
    }
}
