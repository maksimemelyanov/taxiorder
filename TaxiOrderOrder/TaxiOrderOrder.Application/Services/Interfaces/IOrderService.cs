﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiOrderOrder.Application.Contracts;

namespace TaxiOrderOrder.Application.Services.Interfaces
{
    public interface IOrderService
    {
        public int CreateOrder(OrderDto orderDto);
    }
}
