﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Collections.Concurrent;
using TaxiOrderCore.Infrastructure.Caches.Implementations;
using TaxiOrderOrder.Infrastructure.Entities;
using TaxiOrderOrder.Infrastructure.Context;
using TaxiOrderOrder.Infrastructure.Repositories.Implementations;

namespace TaxiOrderOrder.Infrastructure.Cashes.Implementations
{
    public class OrderAllowanceCache : GenericCache<OrderAllowance, int>
    {
        private readonly IServiceProvider _provider;
        private readonly ILogger<GenericCache<OrderAllowance, int>> _logger;

        public OrderAllowanceCache(ILogger<GenericCache<OrderAllowance, int>> logger, IServiceProvider provider)
        {
            _provider = provider;
            _logger = logger;
        }

        public override async Task Init()
        {
            try
            {
                var scope = _provider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                await using var context = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
                var repository = new OrderAllowanceReadRepository(context);
                var result = await repository.GetAllAsync(CancellationToken.None);
                Cache = new ConcurrentDictionary<int, OrderAllowance>(result.ToDictionary(item => item.Id));
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, InitCacheError);
                Cache = new ConcurrentDictionary<int, OrderAllowance>();
                throw;
            }
        }
    }
}
