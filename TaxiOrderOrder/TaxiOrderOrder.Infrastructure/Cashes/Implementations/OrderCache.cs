﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Collections.Concurrent;
using TaxiOrderCore.Infrastructure.Caches.Implementations;
using TaxiOrderOrder.Infrastructure.Context;
using TaxiOrderOrder.Infrastructure.Entities;
using TaxiOrderOrder.Infrastructure.Repositories.Implementations;

namespace TaxiOrderOrder.Infrastructure.Cashes.Implementations
{
    public class OrderCache : GenericCache<Order, int>
    {
        private readonly IServiceProvider _provider;
        private readonly ILogger<GenericCache<Order, int>> _logger;

        public OrderCache(ILogger<GenericCache<Order, int>> logger, IServiceProvider provider)
        {
            _provider = provider;
            _logger = logger;
        }

        public override async Task Init()
        {
            try
            {
                var scope = _provider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                await using var context = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
                var repository = new OrderReadRepository(context);
                var result = await repository.GetAllAsync(CancellationToken.None);
                Cache = new ConcurrentDictionary<int, Order>(result.ToDictionary(item => item.Id));
            }
            catch (Exception exception)
            {
                _logger.LogError(exception, InitCacheError);
                Cache = new ConcurrentDictionary<int, Order>();
                throw;
            }
        }
    }
}
