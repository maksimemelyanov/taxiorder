﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderOrder.Infrastructure.Entities;

namespace TaxiOrderOrder.Infrastructure.Context
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<OrderAllowance> OrderAllowances { get; set; }
    }
}
