using Microsoft.Extensions.DependencyInjection;
using TaxiOrderCore.Infrastructure;
using TaxiOrderCore.Infrastructure.Caches;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderOrder.Infrastructure.Cashes.Implementations;
using TaxiOrderOrder.Infrastructure.Entities;
using TaxiOrderOrder.Infrastructure.Repositories.Implementations;
namespace TaxiOrderOrder.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddInfrastructureCore();
        serviceCollection
            .AddTransient<IRepository<Order, int>, OrderRepository>()
            .AddTransient<IRepository<OrderAllowance, int>, OrderAllowanceRepository>()
            .AddTransient<IReadRepository<Order, int>, OrderReadRepository>()
            .AddTransient<IReadRepository<OrderAllowance, int>, OrderAllowanceReadRepository>();
        
        serviceCollection.AddCache<int, Order, OrderCache>();
        serviceCollection.AddCache<int, OrderAllowance, OrderAllowanceCache>();

        return serviceCollection;
    }
}