﻿using System.ComponentModel.DataAnnotations.Schema;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderOrder.Infrastructure.Entities
{
    [Table("order", Schema = "main")]
    public class Order : IEntity<int>
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("date_create")]
        public DateTime CreateDate { get; set; }

        [Column("id_client")]
        public int ClientId { get; set; }

        [Column("phone")]
        public string? Phone { get; set; }

        [Column("id_tariff")]
        public int TariffId { get; set; }

        [Column("sum_total")]
        public decimal TotalSum { get; set; }

        [Column("id_order_status")]
        public int OrderStatus { get; set; }

        [Column("sum_driver")]
        public decimal DriverSum { get; set; }

        [Column("allowance")]
        public string? Allowance { get; set; }

        [Column("addresses")]
        public string? Addresses { get; set; }

        [Column("comment")]
        public string? Comment { get; set; }

        [Column("id_driver_base_auto")]
        public int DriverBaseAutoId { get; set; }

        [Column("id_base")]
        public int BaseId { get; set; }
    }
}
