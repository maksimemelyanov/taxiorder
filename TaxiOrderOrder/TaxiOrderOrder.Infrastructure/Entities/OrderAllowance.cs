﻿using System.ComponentModel.DataAnnotations.Schema;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderOrder.Infrastructure.Entities
{
    [Table("order_allowance", Schema = "main")]
    public class OrderAllowance : IEntity<int>
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("id_order")]
        public int OrderId { get; set; }

        [Column("id_allowance")]
        public int AllowanceId { get; set; }
    }
}
