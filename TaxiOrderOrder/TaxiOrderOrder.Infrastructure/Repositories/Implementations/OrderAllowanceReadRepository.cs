﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderOrder.Infrastructure.Entities;

namespace TaxiOrderOrder.Infrastructure.Repositories.Implementations
{
    public class OrderAllowanceReadRepository : ReadRepository<OrderAllowance, int>, IReadRepository<OrderAllowance, int>
    {
        public OrderAllowanceReadRepository(DbContext context) : base(context)
        {
        }
    }
}
