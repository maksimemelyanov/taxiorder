﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderOrder.Infrastructure.Entities;

namespace TaxiOrderOrder.Infrastructure.Repositories.Implementations
{
    public class OrderAllowanceRepository : Repository<OrderAllowance, int>, IRepository<OrderAllowance, int>
    {
        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="context">Контекст</param>
        public OrderAllowanceRepository(DbContext context) : base(context)
        {
        }
    }
}
