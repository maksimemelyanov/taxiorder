﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderOrder.Infrastructure.Entities;

namespace TaxiOrderOrder.Infrastructure.Repositories.Implementations
{
    public class OrderReadRepository : ReadRepository<Order, int>, IReadRepository<Order, int>
    {
        public OrderReadRepository(DbContext context) : base(context)
        {
        }
    }
}
