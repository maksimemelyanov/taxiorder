﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderOrder.Infrastructure.Entities;

namespace TaxiOrderOrder.Infrastructure.Repositories.Implementations
{
    public class OrderRepository : Repository<Order, int>, IRepository<Order, int>
    {
        /// <summary>
        ///     Конструктор
        /// </summary>
        /// <param name="context">Контекст</param>
        public OrderRepository(DbContext context) : base(context)
        {
        }
    }
}
