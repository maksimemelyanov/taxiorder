﻿using AutoMapper;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderOrder.Application.Contracts;
using TaxiOrderOrder.Web.Models;

namespace TaxiOrderOrder.Web.Controllers
{
    public class OrderAllowanceController : GenericCrudController<OrderAllowanceDto, OrderAllowanceModel, int>
    {
        public OrderAllowanceController(ICrudService<OrderAllowanceDto, int> service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
