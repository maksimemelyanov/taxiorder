﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.MessageBus.Bus;
using TaxiOrderCore.MessageBus.Messages;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderOrder.Application.Contracts;
using TaxiOrderOrder.Web.Models;

namespace TaxiOrderOrder.Web.Controllers
{
    public class OrderController : GenericCrudController<OrderDto, OrderModel, int>
    {
        private readonly IMessageBus _messageBus;
        public OrderController(IMessageBus messageBus, ICrudService<OrderDto, int> service, IMapper mapper) : base(service, mapper)
        {
            _messageBus = messageBus;
        }

        [HttpGet("FindDriver")]
        public async Task<IActionResult> FindDriver(int id)
        {
            _messageBus.Publish(new FindDriverRequestMessage() { OrderId = id });
            return Ok();
        }
    }
}
