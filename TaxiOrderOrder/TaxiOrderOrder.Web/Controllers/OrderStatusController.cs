﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderOrder.Web.Controllers
{
    public class OrderStatusController : GenericCrudReadController<OrderStatusDto, OrderStatusModel, int>
    {
        public OrderStatusController(ICrudReadService<OrderStatusDto, int> service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
