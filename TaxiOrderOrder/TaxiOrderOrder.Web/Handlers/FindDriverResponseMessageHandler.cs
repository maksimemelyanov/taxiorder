﻿using TaxiOrderCore.Application;
using TaxiOrderCore.MessageBus.Bus;
using TaxiOrderCore.MessageBus.Messages;
using TaxiOrderOrder.Application.Contracts;
using TaxiOrderOrder.Infrastructure.Context;
using TaxiOrderOrder.Infrastructure.Repositories.Implementations;

namespace TaxiOrderOrder.Web.Handlers
{
    public class FindDriverResponseMessageHandler : IMessageHandler<FindDriverResponseMessage>
    {
        private readonly IMessageBus _messageBus;
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger<FindDriverResponseMessageHandler> _logger;
        public FindDriverResponseMessageHandler(IMessageBus messageBus, IServiceProvider serviceProvider, ILogger<FindDriverResponseMessageHandler> logger)
        {
            _messageBus = messageBus;
            _logger = logger;
            _serviceProvider = serviceProvider;
        }

        public async Task HandleAsync(FindDriverResponseMessage message)
        {

            _logger.LogInformation($"Найден водитель c id = { message.DriverBaseAutoId} на заказ {message.OrderId}");
            try
            {
                var scope = _serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                var orderService = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
                
                var repo = new OrderRepository(orderService);
                var order = repo.GetAsync(message.OrderId).Result;
                order.OrderStatus = (int)OrderStatus.DriverAssigned;
                order.DriverBaseAutoId = message.DriverBaseAutoId;
                repo.Update(order);
                await repo.SaveChangesAsync();
                var response = new ClientResponseMessage
                {
                    OrderId = message.OrderId,
                    Status = OrderStatus.DriverAssigned,
                    Info = message.DriverInfo
                };
                _messageBus.Publish(response);
            }
            catch
            {
                throw;
            }
        }
    }
}
