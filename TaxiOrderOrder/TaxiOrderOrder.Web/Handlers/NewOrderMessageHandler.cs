﻿using AutoMapper;
using TaxiOrderCore.Application;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.MessageBus.Bus;
using TaxiOrderCore.MessageBus.Messages;
using TaxiOrderOrder.Application.Contracts;
using TaxiOrderOrder.Application.Services.Interfaces;
using TaxiOrderOrder.Infrastructure.Context;
using TaxiOrderOrder.Infrastructure.Entities;
using TaxiOrderOrder.Infrastructure.Repositories.Implementations;

namespace TaxiOrderOrder.Web.Handlers
{
    public class NewOrderMessageHandler : IMessageHandler<ClientStartRequestMessage>
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IMessageBus _messageBus;
        private readonly ILogger<NewOrderMessageHandler> _logger;
        private readonly IMapper _mapper;

        public NewOrderMessageHandler(IMessageBus messageBus, IServiceProvider serviceProvider, IMapper mapper, ILogger<NewOrderMessageHandler> logger)
        {
            _messageBus = messageBus;
            _logger = logger;
            _serviceProvider = serviceProvider;
            _mapper = mapper;
        }

        public async Task HandleAsync(ClientStartRequestMessage message)
        {
            try
            {
                var scope = _serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope();
                var orderService = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
                var dto = new OrderDto()
                {
                    Addresses = string.Join(" -> ", message.StartAddress, message.EndAddress),
                    Allowance = "",
                    BaseId = message.BaseId,
                    ClientId = message.ClientId,
                    Comment = "",
                    DriverSum = Convert.ToDecimal(message.DriverPrice),
                    CreateDate = DateTime.UtcNow,
                    OrderStatus = 9,
                    Phone = "",
                    TariffId = message.TariffId,
                    TotalSum = Convert.ToDecimal(message.ClientPrice),
                };
                var repo = new OrderRepository(orderService);
                var order = _mapper.Map<OrderDto, Order>(dto);
                order = repo.Add(order);
                await repo.SaveChangesAsync();
                var orderId = order.Id;
                _logger.LogInformation($"Создан заказ {orderId}");
                var request = new FindDriverRequestMessage
                {
                    OrderId = orderId,
                    Status = OrderStatus.NewOrder,
                    BaseId = dto.BaseId
                };
                _messageBus.Publish(request);
                _logger.LogInformation($"Отправлен запрос на поиск водителя дла заказа {orderId}");
            }
            catch
            {
                throw; 
            }

        }
    }
}
