﻿using AutoMapper;
using TaxiOrderOrder.Application.Contracts;
using TaxiOrderOrder.Web.Models;

namespace TaxiOrderOrder.Web.Mappings
{
    public class OrderAllowanceMappingProfile : Profile
    {
        public OrderAllowanceMappingProfile()
        {
            CreateMap<OrderAllowanceDto, OrderAllowanceModel>();
            CreateMap<OrderAllowanceModel, OrderAllowanceDto>();
        }
    }
}
