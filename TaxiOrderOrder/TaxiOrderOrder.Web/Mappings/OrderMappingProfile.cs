﻿using AutoMapper;
using TaxiOrderOrder.Application.Contracts;
using TaxiOrderOrder.Web.Models;

namespace TaxiOrderOrder.Web.Mappings
{
    public class OrderMappingProfile : Profile
    {
        public OrderMappingProfile()
        {
            CreateMap<OrderDto, OrderModel>();
            CreateMap<OrderModel, OrderDto>();
        }
    }
}
