﻿namespace TaxiOrderOrder.Web.Models
{
    public class OrderAllowanceModel
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int AllowanceId { get; set; }
    }
}
