﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TaxiOrderCore.Application.Services.Implementations;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.MessageBus.Bus;
using TaxiOrderCore.MessageBus.Messages;
using TaxiOrderCore.MessageBus.RabbitMQ.Extensions;
using TaxiOrderCore.Web.Mappings;
using TaxiOrderOrder.Application;
using TaxiOrderOrder.Application.Contracts;
using TaxiOrderOrder.Application.Services.Implementations;
using TaxiOrderOrder.Application.Services.InitCaches.Implementations;
using TaxiOrderOrder.Application.Services.Interfaces;
using TaxiOrderOrder.Infrastructure;
using TaxiOrderOrder.Infrastructure.Context;
using TaxiOrderOrder.Infrastructure.Entities;
using TaxiOrderOrder.Web.Handlers;
using TaxiOrderOrder.Web.Mappings;

namespace TaxiOrderOrder.Web
{
    public class OrderHost
    {
        private const string ConfigurationFileName = "appsettings.json";
        private readonly WebApplicationBuilder _builder;

        private OrderHost(string[] args)
        {
            _builder = WebApplication.CreateBuilder(args);
        }

        public static OrderHost CreateHost(string[] args)
        {
            return new OrderHost(args);
        }

        public OrderHost ConfigureHost()
        {
            _builder.Host
                .ConfigureAppConfiguration(builder =>
                {
                    builder.AddJsonFile(ConfigurationFileName);
                    builder.AddUserSecrets<OrderHost>(true, true);
                });

            return this;
        }

        public OrderHost ConfigureSwaggerService()
        {
            _builder.Services.AddEndpointsApiExplorer();
            _builder.Services.AddSwaggerGen();
            return this;
        }

        public OrderHost ConfigureServices()
        {
            _builder.Services.AddControllers();
            _builder.Services
                .AddScoped(typeof(DbContext), typeof(DatabaseContext))
                .AddDbContext<DatabaseContext>(x => { x.UseNpgsql(_builder.Configuration.GetConnectionString("db")); })
                .AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()))
                .AddInfrastructure()
                .AddApplication();

            _builder.Services.AddHostedService<InitCachesService>();
            _builder.Services.AddTransient<IOrderService, OrderService>();
            return this;
        }

        private MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<TaxiOrderOrder.Application.Mappings.OrderAllowanceMappingProfile>();
                cfg.AddProfile<OrderAllowanceMappingProfile>();
                cfg.AddProfile<TaxiOrderOrder.Application.Mappings.OrderMappingProfile>();
                cfg.AddProfile<OrderMappingProfile>();
                //cfg.AddProfile<TaxiOrderCore.Application.Mappings.OrderStatusMappingProfile>();
                //cfg.AddProfile<OrderStatusMappingProfile>();
            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }

        public async Task RunHost()
        {
            var webApplication = _builder.Build();

            var useSwagger = _builder.Configuration.GetValue<bool>("useSwagger");
            if (useSwagger)
                webApplication
                    .UseSwagger()
                    .UseSwaggerUI();

            webApplication
                .UseRouting()
                .UseHttpsRedirection()
                .UseRequestLocalization()
                .UseSwagger()
                .UseSwaggerUI()
                .UseAuthorization();

            webApplication.UseEndpoints(endpoints =>
            {
                endpoints
                    .MapControllers();
            });

            ConfigureMessageBusHandlers(webApplication);
            await webApplication.RunAsync();
        }

        public OrderHost ConfigureMessageBusDependencies()
        {
            var rabbitMQSection = _builder.Configuration.GetSection("RabbitMQ");
            _builder.Services.AddRabbitMQMessageBus
            (
                host: rabbitMQSection["Host"],
                exchangeName: rabbitMQSection["Exchange"],
                queueName: rabbitMQSection["Queue"],
            timeoutBeforeReconnecting: 15
            );

            _builder.Services.AddTransient<FindDriverResponseMessageHandler>();
            _builder.Services.AddTransient<NewOrderMessageHandler>();

            return this;
        }

        private void ConfigureMessageBusHandlers(IApplicationBuilder app)
        {
            var messageBus = app.ApplicationServices.GetRequiredService<IMessageBus>();

            messageBus.Subscribe<FindDriverResponseMessage, FindDriverResponseMessageHandler>();
            messageBus.Subscribe<ClientStartRequestMessage, NewOrderMessageHandler>();

        }
    }
}
