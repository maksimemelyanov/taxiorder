﻿namespace TaxiOrderAddress.Application.Contracts;

public class BaseAddressDto
{
    public int Id { get; set; }
    public string Name { get; set; }
}   
