﻿
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderAddress.Application.Contracts;
public class HouseDto : IEntity<int>
{
    public int Id { get ; set ; }

    public string Name { get; set; } = null!;

    public int StreetId { get; set; }

    public int Longitude { get; set; }

    public int Latitude { get; set; }
}

