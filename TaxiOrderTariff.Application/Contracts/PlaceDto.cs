﻿
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderAddress.Application.Contracts;
public class PlaceDto : IEntity<int>
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;

    public int RegionId { get; set; }

    public int PlaceTypeId { get; set; }

    public int IsHaveStreet { get; set; }

    public int Longitude { get; set; }

    public int Latitude { get; set; }
}
