﻿namespace TaxiOrderAddress.Application.Contracts;

public class PlaceToPointDto : BaseAddressDto
{
    public BaseAddressDto PlaceType { get; set; }
}
