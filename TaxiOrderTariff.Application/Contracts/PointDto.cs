﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxiOrderAddress.Application.Contracts
{
    public class PointDto
    {
        public BaseAddressDto Region { get; set; }
        public PlaceToPointDto Place { get; set; }
        public BaseAddressDto? Address { get; set; }
        public StreetToPointDto? Street { get; set; }
        public BaseAddressDto? House { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
