﻿namespace TaxiOrderAddress.Application.Contracts;

public class StreetToPointDto : BaseAddressDto
{
    public BaseAddressDto StreetType { get; set; }
}
