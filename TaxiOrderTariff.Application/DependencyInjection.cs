using Microsoft.Extensions.DependencyInjection;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Application.Services.Implementations;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Infrastructure.Entities;
using TaxiOrderAddress.Application.Services.PointService.Interfaces;
using TaxiOrderAddress.Application.Services.PointService.Implementations;

namespace TaxiOrderAddress.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplication(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddTransient<ICrudService<HouseDto, int>, CrudService<HouseDto, House, int>>();
        serviceCollection.AddTransient<ICrudService<PlaceDto, int>, CrudService<PlaceDto, Place, int>>();
        serviceCollection.AddTransient<ICrudService<PlaceTypeDto, int>, CrudService<PlaceTypeDto, PlaceType, int>>();
        serviceCollection.AddTransient<ICrudService<ShortAddressDto, int>, CrudService<ShortAddressDto, ShortAddress, int>>();
        serviceCollection.AddTransient<ICrudService<StreetDto, int>, CrudService<StreetDto, Street, int>>();
        serviceCollection.AddTransient<ICrudService<StreetTypeDto, int>, CrudService<StreetTypeDto, StreetType, int>>();
        serviceCollection.AddTransient<ICrudReadService<HouseDto, int>, CrudReadService<HouseDto, House, int>>();
        serviceCollection.AddTransient<ICrudReadService<PlaceDto, int>, CrudReadService<PlaceDto, Place, int>>();
        serviceCollection.AddTransient<ICrudReadService<PlaceTypeDto, int>, CrudReadService<PlaceTypeDto, PlaceType, int>>();
        serviceCollection.AddTransient<ICrudReadService<ShortAddressDto, int>, CrudReadService<ShortAddressDto, ShortAddress, int>>();
        serviceCollection.AddTransient<ICrudReadService<StreetDto, int>, CrudReadService<StreetDto, Street, int>>();
        serviceCollection.AddTransient<ICrudReadService<StreetTypeDto, int>, CrudReadService<StreetTypeDto, StreetType, int>>();
        serviceCollection.AddTransient<IPointProvider, PointProvider>();
        serviceCollection.AddTransient<IAddressParseService, AddressParseService>();
        //serviceCollection.AddTransient<ICrudReadService<BaseDto, int>, CrudReadService<BaseDto, Base, int>>();
        //serviceCollection.AddTransient<ICrudReadService<RegionDto, int>, CrudReadService<RegionDto, Region, int>>();

        //serviceCollection.AddSingleton<ICalculationDistanceService, CalculationDistanceService>();
        //serviceCollection.AddSingleton<ICalculationPriceProvider, CalculationPriceProvider>();
        //serviceCollection.AddSingleton<ICalculationPriceStrategy, CalculationPriceWithDriverTariffStrategy>();
        //serviceCollection.AddSingleton<ICalculationPriceStrategy, CalculationPriceWithoutDriverTariffStrategy>();
        //serviceCollection.AddSingleton<ICalculationPriceClientService, CalculationPriceClientService>();
        //serviceCollection.AddSingleton<ICalculationPriceDriverService, CalculationPriceDriverService>();

        return serviceCollection;
    }
}