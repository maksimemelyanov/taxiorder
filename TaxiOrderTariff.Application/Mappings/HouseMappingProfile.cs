﻿using AutoMapper;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Infrastructure.Entities;

namespace TaxiOrderAddress.Application.Mappings;

public class HouseMappingProfile : Profile
{
    public HouseMappingProfile()
    {
        CreateMap<HouseDto, House>();
        CreateMap<House, HouseDto>();
    }
}