﻿using AutoMapper;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Infrastructure.Entities;

namespace TaxiOrderAddress.Application.Mappings;

public class PlaceTypeMappingProfile : Profile
{
    public PlaceTypeMappingProfile()
    {
        CreateMap<PlaceTypeDto, PlaceType>();
        CreateMap<PlaceType, PlaceTypeDto>();
    }
}