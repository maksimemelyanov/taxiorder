﻿using AutoMapper;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Infrastructure.Entities;

namespace TaxiOrderAddress.Application.Mappings;

public class StreetMappingProfile : Profile
{
    public StreetMappingProfile()
    {
        CreateMap<StreetDto, Street>();
        CreateMap<Street, StreetDto>();
    }
}