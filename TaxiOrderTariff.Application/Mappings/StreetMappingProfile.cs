﻿using AutoMapper;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Infrastructure.Entities;

namespace TaxiOrderAddress.Application.Mappings;

public class ShortAddressMappingProfile : Profile
{
    public ShortAddressMappingProfile()
    {
        CreateMap<ShortAddressDto, ShortAddress>();
        CreateMap<ShortAddress, ShortAddressDto>();
    }
}