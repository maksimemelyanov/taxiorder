﻿using AutoMapper;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Infrastructure.Entities;

namespace TaxiOrderAddress.Application.Mappings;

public class StreetTypeMappingProfile : Profile
{
    public StreetTypeMappingProfile()
    {
        CreateMap<StreetTypeDto, StreetType>();
        CreateMap<StreetType, StreetTypeDto>();
    }
}