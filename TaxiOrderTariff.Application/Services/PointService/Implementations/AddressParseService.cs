﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiOrderAddress.Application.Services.PointService.Interfaces;

namespace TaxiOrderAddress.Application.Services.PointService.Implementations
{
    public class AddressParseService : IAddressParseService
    {
        private int _houseNumberLength;

        public Tuple<string, string> ParseAddress(List<string> addressParts)
        {
            var house = GetHouse(addressParts);
            var street = GetStreet(addressParts);
            
            return Tuple.Create(street, house);            
        }

        private string GetHouse(List<string> addressParts) {
            if (addressParts.Count == 1) {
                return null;
            }
            var house = addressParts.Last();
            _houseNumberLength = 1;
            if (HasFrame(house))
            {
                house = addressParts[addressParts.Count - 2] + house;
                _houseNumberLength++;
            }
            return house.TrimStart('.', ' ', ',', '_').TrimEnd('.', ' ', ',', '_').ToLower();
        }

        private string GetStreet(List<string> addressParts)
        {
            string street = string.Join(' ', addressParts.Take(addressParts.Count - _houseNumberLength));
            return street.TrimStart('.',' ',',','_').TrimEnd('.', ' ', ',', '_').ToLower();
        }

        private bool HasFrame(string house)
        {
            return char.ToLower(house[0]) == 'к';
        }

    }
}
