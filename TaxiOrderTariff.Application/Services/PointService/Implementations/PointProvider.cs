﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;
using TaxiOrderAddress.Application.Contracts;
using TaxiOrderAddress.Application.Services.PointService.Interfaces;
using TaxiOrderAddress.Infrastructure.Entities;
using TaxiOrderAddress.Infrastructure.Repositories.Interfaces;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderAddress.Application.Services.PointService.Implementations
{
    public class PointProvider : IPointProvider
    {
        private IAddressRepository _repository;
        private IAddressParseService _addressParseService;
        public PointProvider(IAddressRepository repository, IAddressParseService addressParseService) { 
            _repository = repository;
            _addressParseService = addressParseService;
        }
        public async Task<List<PointDto>> GetPoints(int baseId, string addressName)
        {
            try
            {
                var region = await _repository.GetRegion(baseId);
                var addressParts = addressName.Split(new char[]{ ' ',','}).Where(s => s != "").ToList();
                int? placeTypeId = await _repository.GetPlaceTypeByNameAsync(addressParts);
                int? placeId = await _repository.GetPlaceByNameAsync(addressParts, region.Id, placeTypeId);

                int? streetTypeId = _repository.GetStreetTypeByNameAsync(addressParts).Result;

                Tuple<string, string> parsedAddress = _addressParseService.ParseAddress(addressParts);
                string streetName = parsedAddress.Item1;
                string houseName = parsedAddress.Item2;
                
                var places = (await _repository.GetPlacesAsync(baseId)).Where(p=> placeId == null || p.Id==placeId).ToList();
                var placeTypes = await _repository.GetPlaceTypesAsync(places.Select(p => p.PlaceTypeId).Distinct().ToList());
                var points = new List<PointDto>();
                if (houseName is null)
                {
                    var shortAddresses = await _repository.GetShortAddressAsync(streetName, places.Select(p => p.Id).ToList());
                    points = MakePointsFromFullAddressFromShortAddress(shortAddresses, places, placeTypes, region);
                }
                if (points.Count < 10)
                {                    
                    var streets = await _repository.GetStreetsAsync(streetName, places.Select(p => p.Id).Distinct().ToList(), streetTypeId);
                    var streetTypes = await _repository.GetStreetTypesAsync(streets.Select(s => s.StreetTypeId).Distinct().ToList());
                    var houses = await _repository.GetHousesAsync(houseName, streets.Select(s => s.Id).ToList());
                    points.AddRange(MakePointsFromFullAddress(houses, streets, streetTypes, places, placeTypes, region));
                }
                return points;               

            }
            catch (Exception ex)
            {
                return GetPointsDto(baseId, addressName);
            }
        }

        private List<PointDto> MakePointsFromFullAddress(ICollection<House> houses, ICollection<Street> streets, ICollection<StreetType> streetTypes, ICollection<Place> places, ICollection<PlaceType> placesTypes, Region region)
        {
            var points = new List<PointDto>();
            foreach (var house in houses)
            {
                var street = streets.FirstOrDefault(s => s.Id == house.StreetId);
                var streetType = streetTypes.FirstOrDefault(st => st.Id == street.StreetTypeId);
                var place = places.FirstOrDefault(p => p.Id == street.PlaceId);
                var placeType = placesTypes.FirstOrDefault(pt => pt.Id == place.PlaceTypeId);
                points.Add(new() {
                    Region = new()
                    {
                        Id = region.Id,
                        Name = region.Name
                    },
                    Place = new()
                    {
                        Id = place.Id,
                        Name = place.Name,
                        PlaceType = new()
                        {
                            Id = placeType.Id,
                            Name = placeType.Name
                        }
                    },
                    Street = new()
                    {
                        Id = street.Id,
                        Name = street.Name,
                        StreetType = new()
                        {
                            Id = streetType.Id,
                            Name = streetType.Name
                        }
                    },
                    House = new()
                    {
                        Id = house.Id,
                        Name = house.Name,
                    },
                    Latitude = house.Latitude,
                    Longitude = house.Longitude
                });
            }
            return points;
        }

        private List<PointDto> MakePointsFromFullAddressFromShortAddress(ICollection<ShortAddress> shortAddresses, ICollection<Place> places, ICollection<PlaceType> placesTypes, Region region)
        {
            var points = new List<PointDto>();
            foreach (var address in shortAddresses)
            {
                var place = places.FirstOrDefault(p => p.Id == address.PlaceId);
                var placeType = placesTypes.FirstOrDefault(pt => pt.Id == place.PlaceTypeId);
                points.Add(new()
                {
                    Region = new()
                    {
                        Id = region.Id,
                        Name = region.Name
                    },
                    Place = new()
                    {
                        Id = place.Id,
                        Name = place.Name,
                        PlaceType = new()
                        {
                            Id = placeType.Id,
                            Name = placeType.Name
                        }
                    },
                    Address = new()
                    { 
                        Id = address.Id,
                        Name = address.Name,
                    },
                    Latitude = address.Latitude,
                    Longitude = address.Longitude
                });
            }
            return points;
        }

        private List<PointDto> GetPointsDto(int baseId, string addressName)
        {
            var Kladbishe = new PointDto()
            {
                Region = new()
                {
                    Id = 2,
                    Name = "Курганская область"
                },
                Place = new()
                {
                    Id = 1,
                    Name = "Курган",
                    PlaceType = new()
                    {
                        Id = 4,
                        Name = "город"
                    }
                },
                Address = new()
                {
                    Id = 10,
                    Name = "Кладбище"
                },
                Latitude = 50.0,
                Longitude = 60.0
            };
            var Lenina13 = new PointDto()
            {
                Region = new()
                {
                    Id = 2,
                    Name = "Курганская область"
                },
                Place = new()
                {
                    Id = 1,
                    Name = "Курган",
                    PlaceType = new()
                    {
                        Id = 4,
                        Name = "город"
                    }
                },
                Street = new()
                {
                    Id = 12,
                    Name = "Ленина",
                    StreetType = new()
                    {
                        Id = 13,
                        Name = "улица"
                    }
                },
                House = new()
                {
                    Id = 22,
                    Name = "13",
                },
                Latitude = 51.0,
                Longitude = 61.0
            };
            var Kurgan = new PointDto()
            {
                Region = new()
                {
                    Id = 2,
                    Name = "Курганская область"
                },
                Place = new()
                {
                    Id = 1,
                    Name = "Курган",
                    PlaceType = new()
                    {
                        Id = 4,
                        Name = "город"
                    }
                },
                Latitude = 52.1,
                Longitude = 62.1
            };


            return new List<PointDto>() { Kladbishe, Lenina13, Kurgan };
        }
    }
}
