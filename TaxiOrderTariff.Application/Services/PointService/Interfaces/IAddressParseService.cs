﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxiOrderAddress.Application.Services.PointService.Interfaces
{
    public interface IAddressParseService
    {
        public Tuple<string, string> ParseAddress(List<string> addressParts);
    }

}
