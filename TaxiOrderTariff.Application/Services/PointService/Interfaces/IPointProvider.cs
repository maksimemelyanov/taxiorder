﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiOrderAddress.Application.Contracts;

namespace TaxiOrderAddress.Application.Services.PointService.Interfaces
{
    public interface IPointProvider
    {
        public Task<List<PointDto>> GetPoints(int baseId, string addressName);
    }
}
