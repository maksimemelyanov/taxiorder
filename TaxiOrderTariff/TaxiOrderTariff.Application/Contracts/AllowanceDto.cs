﻿using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderTariff.Application.Contracts;

public class AllowanceDto : IEntity<int>
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public int AllowanceTypeId { get; set; }

    public double Price { get; set; }

    public double TariffId { get; set; }
}