﻿namespace TaxiOrderTariff.Application.Contracts;

public class CalculationPriceDto
{
    public double ClientPrice { get; set; }

    public double DriverPrice { get; set; }
}