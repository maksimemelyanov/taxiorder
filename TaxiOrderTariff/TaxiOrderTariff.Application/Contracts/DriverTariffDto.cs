﻿using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderTariff.Application.Contracts;

public class DriverTariffDto : IEntity<int>
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public int BaseId { get; set; }

    public double Percent { get; set; }

    public int DriverTariffId { get; set; }
}