﻿namespace TaxiOrderTariff.Application.Contracts;

public class PriceAllowanceDto
{
    public int Id { get; set; }

    public int Count { get; set; }
}