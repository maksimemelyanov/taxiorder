using Microsoft.Extensions.DependencyInjection;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Application.Services.Implementations;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderTariff.Application.Contracts;
using TaxiOrderTariff.Application.Services.CalculationDistance.Implementations;
using TaxiOrderTariff.Application.Services.CalculationDistance.Interfaces;
using TaxiOrderTariff.Application.Services.CalculationPrice.Implementations;
using TaxiOrderTariff.Application.Services.CalculationPrice.Interfaces;
using TaxiOrderTariff.Infrastructure.Entities;

namespace TaxiOrderTariff.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplication(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddTransient<ICrudService<AllowanceDto, int>, CrudService<AllowanceDto, Allowance, int>>();
        serviceCollection.AddTransient<ICrudService<DriverTariffDto, int>, CrudService<DriverTariffDto, DriverTariff, int>>();
        serviceCollection.AddTransient<ICrudService<TariffDto, int>, CrudService<TariffDto, Tariff, int>>();
        serviceCollection.AddTransient<ICrudReadService<TariffDto, int>, CrudReadService<TariffDto, Tariff, int>>();
        serviceCollection.AddTransient<ICrudReadService<AllowanceTypeDto, int>, CrudReadService<AllowanceTypeDto, AllowanceType, int>>();
        serviceCollection.AddTransient<ICrudReadService<AutoCategoryDto, int>, CrudReadService<AutoCategoryDto, AutoCategory, int>>();
        serviceCollection.AddTransient<ICrudReadService<BaseDto, int>, CrudReadService<BaseDto, Base, int>>();
        serviceCollection.AddTransient<ICrudReadService<DriverTariffTypeDto, int>, CrudReadService<DriverTariffTypeDto, DriverTariffType, int>>();
        serviceCollection.AddTransient<ICrudReadService<RegionDto, int>, CrudReadService<RegionDto, Region, int>>();
        serviceCollection.AddTransient<ICrudReadService<TariffTypeDto, int>, CrudReadService<TariffTypeDto, TariffType, int>>();
        
        serviceCollection.AddSingleton<ICalculationDistanceService, CalculationDistanceService>();
        serviceCollection.AddSingleton<ICalculationPriceProvider, CalculationPriceProvider>();
        serviceCollection.AddSingleton<ICalculationPriceStrategy, CalculationPriceWithDriverTariffStrategy>();
        serviceCollection.AddSingleton<ICalculationPriceStrategy, CalculationPriceWithoutDriverTariffStrategy>();
        serviceCollection.AddSingleton<ICalculationPriceClientService, CalculationPriceClientService>();
        serviceCollection.AddSingleton<ICalculationPriceDriverService, CalculationPriceDriverService>();

        return serviceCollection;
    }
}