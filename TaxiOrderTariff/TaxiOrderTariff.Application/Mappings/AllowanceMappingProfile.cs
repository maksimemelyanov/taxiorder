﻿using AutoMapper;
using TaxiOrderTariff.Application.Contracts;
using TaxiOrderTariff.Infrastructure.Entities;

namespace TaxiOrderTariff.Application.Mappings;

public class AllowanceMappingProfile : Profile
{
    public AllowanceMappingProfile()
    {
        CreateMap<AllowanceDto, Allowance>();
        CreateMap<Allowance, AllowanceDto>();
    }
}