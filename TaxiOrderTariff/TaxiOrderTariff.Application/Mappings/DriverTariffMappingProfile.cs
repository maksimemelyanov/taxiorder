﻿using AutoMapper;
using TaxiOrderTariff.Application.Contracts;
using TaxiOrderTariff.Infrastructure.Entities;

namespace TaxiOrderTariff.Application.Mappings;

public class DriverTariffMappingProfile : Profile
{
    public DriverTariffMappingProfile()
    {
        CreateMap<DriverTariffDto, DriverTariff>();
        CreateMap<DriverTariff, DriverTariffDto>();
    }
}