﻿using AutoMapper;
using TaxiOrderTariff.Application.Contracts;
using TaxiOrderTariff.Infrastructure.Entities;

namespace TaxiOrderTariff.Application.Mappings;

/// <summary>
///     Профиль маппинга тарифов
/// </summary>
public class TariffMappingProfile : Profile
{
    /// <summary>
    ///     Конструктор
    /// </summary>
    public TariffMappingProfile()
    {
        CreateMap<TariffDto, Tariff>();
        CreateMap<Tariff, TariffDto>();
    }
}