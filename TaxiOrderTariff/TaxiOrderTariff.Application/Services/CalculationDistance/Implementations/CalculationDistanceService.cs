﻿using TaxiOrderTariff.Application.Services.CalculationDistance.Interfaces;

namespace TaxiOrderTariff.Application.Services.CalculationDistance.Implementations;

public class CalculationDistanceService : ICalculationDistanceService
{
    private const int RadiusEarth = 6371008;

    public double Calculate(double firstPointLatitude, double firstPointLongitude, double secondPointLatitude,
        double secondPointLongitude)
    {
        var latitudeFirst = firstPointLatitude * Math.PI / 180;
        var longitudeFirst = firstPointLongitude * Math.PI / 180;
        var latitudeSecond = secondPointLatitude * Math.PI / 180;
        var longitudeSecond = secondPointLongitude * Math.PI / 180;

        var cosFirstLatitude = Math.Cos(latitudeFirst);
        var cosSecondLatitude = Math.Cos(latitudeSecond);
        var sinFirstLatitude = Math.Sin(latitudeFirst);
        var sinSecondLatitude = Math.Sin(latitudeSecond);

        var delta = longitudeSecond - longitudeFirst;
        var cosDelta = Math.Cos(delta);
        var sinDelta = Math.Sin(delta);

        var y = Math.Sqrt(Math.Pow(cosSecondLatitude * sinDelta, 2) +
                          Math.Pow(cosFirstLatitude * sinSecondLatitude -
                                   sinFirstLatitude * cosSecondLatitude * cosDelta, 2));
        var x = sinFirstLatitude * sinSecondLatitude + cosFirstLatitude * cosSecondLatitude * cosDelta;

        var arcTan = Math.Atan2(y, x);

        return arcTan * RadiusEarth;
    }
}