﻿namespace TaxiOrderTariff.Application.Services.CalculationDistance.Interfaces;

public interface ICalculationDistanceService
{
    double Calculate(double firstPointLatitude, double firstPointLongitude,
        double secondPointLatitude, double secondPointLongitude);
}