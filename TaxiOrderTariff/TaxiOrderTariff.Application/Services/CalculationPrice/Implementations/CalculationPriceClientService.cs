﻿using TaxiOrderCore.Infrastructure.Caches.Interfaces;
using TaxiOrderTariff.Application.Contracts;
using TaxiOrderTariff.Application.Services.CalculationDistance.Interfaces;
using TaxiOrderTariff.Application.Services.CalculationPrice.Interfaces;
using TaxiOrderTariff.Infrastructure.Entities;

namespace TaxiOrderTariff.Application.Services.CalculationPrice.Implementations;

public class CalculationPriceClientService : ICalculationPriceClientService
{
    private readonly ICache<Tariff, int> _tariffCache;
    private readonly ICache<Allowance, int> _allowanceCache;
    private readonly ICalculationDistanceService _calculationDistanceService;

    public CalculationPriceClientService(ICache<Tariff, int> tariffCache, ICache<Allowance, int> allowanceCache,
        ICalculationDistanceService calculationDistanceService)
    {
        _tariffCache = tariffCache;
        _allowanceCache = allowanceCache;
        _calculationDistanceService = calculationDistanceService;
    }

    public double Calculate(int tariffId, double startLatitude, double startLongitude,
        double endLatitude, double endLongitude, List<PriceAllowanceDto>? allowances)
    {
        var clientPrice = 0.0;
        var allowancePrice = 0.0;
        var tariff = _tariffCache.FindByKey(tariffId);
        if (tariff != null)
        {
            var distance = _calculationDistanceService.Calculate(startLatitude, startLongitude,
                endLatitude, endLongitude) / 1000;
            if (tariff.MinimalDistance == 0 || distance <= tariff.MinimalDistance)
            {
                clientPrice = GetRound10Price(tariff.Price * tariff.Coefficient);
            }
            else
            {
                clientPrice = GetRound10Price(tariff.Price * tariff.Coefficient * distance / tariff.MinimalDistance);
            }

            if (allowances != null)
            {
                foreach (var allowance in allowances)
                {
                    var currentAllowance = _allowanceCache.FindByKey(allowance.Id);
                    if (currentAllowance != null && currentAllowance.TariffId == tariffId)
                    {
                        allowancePrice += currentAllowance.Price;
                    }
                }

            }
        }

        return clientPrice + allowancePrice;
    }

    private double GetRound10Price(double price)
    {
        return (long) Math.Round(price / 10) * 10;
    }
}