﻿using TaxiOrderCore.Infrastructure.Caches.Interfaces;
using TaxiOrderTariff.Application.Services.CalculationPrice.Interfaces;
using TaxiOrderTariff.Infrastructure.Entities;

namespace TaxiOrderTariff.Application.Services.CalculationPrice.Implementations;

public class CalculationPriceDriverService : ICalculationPriceDriverService
{
    private readonly ICache<DriverTariff, int> _tariffCache;

    public CalculationPriceDriverService(ICache<DriverTariff, int> tariffCache)
    {
        _tariffCache = tariffCache;
    }

    public double Calculate(int? driverTariffId, double clientPrice)
    {
        var driverPrice = 0.0;
        if (driverTariffId == null || clientPrice == 0)
        {
            return driverPrice;
        }
        
        var tariff = _tariffCache.FindByKey((int) driverTariffId);
        if (tariff == null)
        {
            return driverPrice;
        }
        driverPrice = Math.Round(tariff.Percent / 100 * clientPrice);

        return driverPrice;
    }
}