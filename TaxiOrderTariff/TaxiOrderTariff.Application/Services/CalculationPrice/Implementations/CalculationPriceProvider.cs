﻿using TaxiOrderTariff.Application.Contracts;
using TaxiOrderTariff.Application.Services.CalculationPrice.Interfaces;

namespace TaxiOrderTariff.Application.Services.CalculationPrice.Implementations;

public class CalculationPriceProvider : ICalculationPriceProvider
{
    private readonly List<ICalculationPriceStrategy> _strategies;

    public CalculationPriceProvider(IEnumerable<ICalculationPriceStrategy> strategies)
    {
        _strategies = strategies.ToList();
    }

    public CalculationPriceDto Calculate(int tariffId, double startLatitude, double startLongitude,
        double endLatitude, double endLongitude, int? driverTariffId, List<PriceAllowanceDto>? allowances)
    {
        var strategy = _strategies.Find(strategy => strategy.IsValid(driverTariffId))!;
        var result = strategy.Calculate(tariffId, startLatitude, startLongitude, endLatitude, endLongitude,
            driverTariffId, allowances);

        return result;
    }
}