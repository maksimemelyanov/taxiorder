﻿using TaxiOrderTariff.Application.Contracts;
using TaxiOrderTariff.Application.Services.CalculationPrice.Interfaces;

namespace TaxiOrderTariff.Application.Services.CalculationPrice.Implementations;

public class CalculationPriceWithDriverTariffStrategy : ICalculationPriceStrategy
{
    private readonly ICalculationPriceClientService _calculationPriceClientService;
    private readonly ICalculationPriceDriverService _calculationPriceDriverService;

    public CalculationPriceWithDriverTariffStrategy(ICalculationPriceClientService calculationPriceClientService,
        ICalculationPriceDriverService calculationPriceDriverService)
    {
        _calculationPriceClientService = calculationPriceClientService;
        _calculationPriceDriverService = calculationPriceDriverService;
    }

    public bool IsValid(int? driverTariffId)
    {
        return driverTariffId != null;
    }

    public CalculationPriceDto Calculate(int tariffId, double startLatitude, double startLongitude,
        double endLatitude, double endLongitude, int? driverTariffId, List<PriceAllowanceDto>? allowances)
    {
        var clientPrice = _calculationPriceClientService.Calculate(tariffId, startLatitude, startLongitude, endLatitude, endLongitude, allowances);
        var driverPrice = _calculationPriceDriverService.Calculate(driverTariffId, clientPrice);
        return new CalculationPriceDto { ClientPrice = clientPrice, DriverPrice = driverPrice };
    }
}