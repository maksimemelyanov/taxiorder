﻿using TaxiOrderTariff.Application.Contracts;
using TaxiOrderTariff.Application.Services.CalculationPrice.Interfaces;

namespace TaxiOrderTariff.Application.Services.CalculationPrice.Implementations;

public class CalculationPriceWithoutDriverTariffStrategy : ICalculationPriceStrategy
{
    private readonly ICalculationPriceClientService _calculationPriceClientService;

    public CalculationPriceWithoutDriverTariffStrategy(ICalculationPriceClientService calculationPriceClientService)
    {
        _calculationPriceClientService = calculationPriceClientService;
    }

    public bool IsValid(int? driverTariffId)
    {
        return driverTariffId == null;
    }

    public CalculationPriceDto Calculate(int tariffId, double startLatitude, double startLongitude, double endLatitude, double endLongitude,
        int? driverTariffId, List<PriceAllowanceDto>? allowances)
    {
        var clientPrice = _calculationPriceClientService.Calculate(tariffId, startLatitude, startLongitude, endLatitude, endLongitude, allowances);
        return new CalculationPriceDto { ClientPrice = clientPrice, DriverPrice = 0 };
    }
}