﻿using TaxiOrderTariff.Application.Contracts;

namespace TaxiOrderTariff.Application.Services.CalculationPrice.Interfaces;

public interface ICalculationPriceClientService
{
    double Calculate(int tariffId, double startLatitude, double startLongitude,
        double endLatitude, double endLongitude, List<PriceAllowanceDto>? allowances);
}