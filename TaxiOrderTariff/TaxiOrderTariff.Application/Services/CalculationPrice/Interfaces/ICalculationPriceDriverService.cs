﻿namespace TaxiOrderTariff.Application.Services.CalculationPrice.Interfaces;

public interface ICalculationPriceDriverService
{
    double Calculate(int? driverTariffId, double clientPrice);
}