﻿using TaxiOrderTariff.Application.Contracts;

namespace TaxiOrderTariff.Application.Services.CalculationPrice.Interfaces;

public interface ICalculationPriceStrategy
{
    bool IsValid(int? driverTariffId);

    CalculationPriceDto Calculate(int tariffId, double startLatitude, double startLongitude,
        double endLatitude, double endLongitude, int? driverTariffId, List<PriceAllowanceDto>? allowances);
}