﻿using System.Collections.Concurrent;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TaxiOrderCore.Infrastructure.Caches.Implementations;
using TaxiOrderTariff.Infrastructure.Context;
using TaxiOrderTariff.Infrastructure.Entities;
using TaxiOrderTariff.Infrastructure.Repositories.Implementations;

namespace TaxiOrderTariff.Infrastructure.Caches.Implementations;

public class DriverTariffCache : GenericCache<DriverTariff, int>
{
    private readonly IServiceProvider _provider;
    private readonly ILogger<GenericCache<DriverTariff, int>> _logger;

    public DriverTariffCache(ILogger<GenericCache<DriverTariff, int>> logger, IServiceProvider provider)
    {
        _provider = provider;
        _logger = logger;
    }

    public override async Task Init()
    {
        try
        {
            var scope = _provider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            await using var context = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
            var repository = new DriverTariffReadRepository(context);
            var result = await repository.GetAllAsync(CancellationToken.None);
            Cache = new ConcurrentDictionary<int, DriverTariff>(result.ToDictionary(item => item.Id));
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, InitCacheError);
            Cache = new ConcurrentDictionary<int, DriverTariff>();
            throw;
        }
    }
}