﻿using System.Collections.Concurrent;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using TaxiOrderCore.Infrastructure.Caches.Implementations;
using TaxiOrderTariff.Infrastructure.Context;
using TaxiOrderTariff.Infrastructure.Entities;
using TaxiOrderTariff.Infrastructure.Repositories.Implementations;

namespace TaxiOrderTariff.Infrastructure.Caches.Implementations;

public class TariffCache : GenericCache<Tariff, int>
{
    private readonly IServiceProvider _provider;
    private readonly ILogger<GenericCache<Tariff, int>> _logger;

    public TariffCache(ILogger<GenericCache<Tariff, int>> logger, IServiceProvider provider)
    {
        _provider = provider;
        _logger = logger;
    }

    public override async Task Init()
    {
        try
        {
            var scope = _provider.GetRequiredService<IServiceScopeFactory>().CreateScope();
            await using var context = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
            var repository = new TariffReadRepository(context);
            var result = await repository.GetAllAsync(CancellationToken.None);
            Cache = new ConcurrentDictionary<int, Tariff>(result.ToDictionary(item => item.Id));
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, InitCacheError);
            Cache = new ConcurrentDictionary<int, Tariff>();
            throw;
        }
    }
}