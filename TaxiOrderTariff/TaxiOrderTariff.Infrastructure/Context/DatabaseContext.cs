﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Entities;
using TaxiOrderTariff.Infrastructure.Entities;

namespace TaxiOrderTariff.Infrastructure.Context;

public class DatabaseContext : DbContext
{
    public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
    {
    }

    public DbSet<Allowance> Allowances { get; set; }

    public DbSet<DriverTariff> DriverTariffs { get; set; }

    public DbSet<Tariff> Tariffs { get; set; }

    public DbSet<AllowanceType> AllowanceTypes { get; set; }

    public DbSet<AutoCategory> AutoCategories { get; set; }

    public DbSet<Base> Bases { get; set; }

    public DbSet<DriverTariffType> DriverTariffTypes { get; set; }

    public DbSet<Region> Regions { get; set; }

    public DbSet<TariffType> TariffTypes { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
    }
}