using Microsoft.Extensions.DependencyInjection;
using TaxiOrderCore.Infrastructure;
using TaxiOrderCore.Infrastructure.Caches;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderTariff.Infrastructure.Caches.Implementations;
using TaxiOrderTariff.Infrastructure.Entities;
using TaxiOrderTariff.Infrastructure.Repositories.Implementations;

namespace TaxiOrderTariff.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddInfrastructureCore();
        serviceCollection
            .AddTransient<IRepository<Allowance, int>, AllowanceRepository>()
            .AddTransient<IRepository<DriverTariff, int>, DriverTariffRepository>()
            .AddTransient<IRepository<Tariff, int>, TariffRepository>()
            .AddTransient<IReadRepository<Allowance, int>, AllowanceReadRepository>()
            .AddTransient<IReadRepository<DriverTariff, int>, DriverTariffReadRepository>()
            .AddTransient<IReadRepository<Tariff, int>, TariffReadRepository>();

        serviceCollection.AddCache<int, Allowance, AllowanceCache>();
        serviceCollection.AddCache<int, Tariff, TariffCache>();
        serviceCollection.AddCache<int, DriverTariff, DriverTariffCache>();

        return serviceCollection;
    }
}