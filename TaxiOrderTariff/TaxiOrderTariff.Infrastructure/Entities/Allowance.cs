﻿using System.ComponentModel.DataAnnotations.Schema;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderTariff.Infrastructure.Entities;

[Table("allowance", Schema = "main")]
public class Allowance : IEntity<int>
{
    [Column("id")]
    public int Id { get; set; }

    [Column("name")]
    public string Name { get; set; } = null!;

    [Column("id_allowance_type")]
    public int AllowanceTypeId { get; set; }

    [Column("price")]
    public double Price { get; set; }

    [Column("id_tariff")]
    public int TariffId { get; set; }
}