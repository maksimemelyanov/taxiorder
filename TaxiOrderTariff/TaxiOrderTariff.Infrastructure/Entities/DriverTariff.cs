﻿using System.ComponentModel.DataAnnotations.Schema;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderTariff.Infrastructure.Entities;

[Table("driver_tariff", Schema = "main")]
public class DriverTariff : IEntity<int>
{
    [Column("id")]
    public int Id { get; set; }

    [Column("name")]
    public string Name { get; set; } = null!;

    [Column("id_base")]
    public int BaseId { get; set; }

    [Column("percent")]
    public double Percent { get; set; }

    [Column("id_driver_tariff_type")]
    public int DriverTariffId { get; set; }
}