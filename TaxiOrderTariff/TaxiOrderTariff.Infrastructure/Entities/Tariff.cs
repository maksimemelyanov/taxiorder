﻿using System.ComponentModel.DataAnnotations.Schema;
using TaxiOrderCore.Infrastructure.Entities;

namespace TaxiOrderTariff.Infrastructure.Entities;

/// <summary>
///     Тариф
/// </summary>
[Table("tariff", Schema = "main")]
public class Tariff : IEntity<int>
{
    /// <summary>
    ///     Наименование тарифа
    /// </summary>
    [Column("name")]
    public string Name { get; set; } = null!;

    /// <summary>
    ///     Минимальная стоимость
    /// </summary>
    [Column("price")]
    public double Price { get; set; }

    /// <summary>
    ///     Коэффициент
    /// </summary>
    [Column("coefficient")]
    public double Coefficient { get; set; }

    /// <summary>
    ///     Минимальное расстояние
    /// </summary>
    [Column("min_distance")]
    public double MinimalDistance { get; set; }

    /// <summary>
    ///     Идентификатор типа тарифа
    /// </summary>
    [Column("id_tariff_type")]
    public int TariffTypeId { get; set; }

    /// <summary>
    ///     Идентификатор организации
    /// </summary>
    [Column("id_base")]
    public int BaseId { get; set; }

    /// <summary>
    ///     Идентификатор
    /// </summary>
    [Column("id")]
    public int Id { get; set; }
}