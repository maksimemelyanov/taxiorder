﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderTariff.Infrastructure.Entities;

namespace TaxiOrderTariff.Infrastructure.Repositories.Implementations;

public class AllowanceReadRepository : ReadRepository<Allowance, int>, IReadRepository<Allowance, int>
{
    public AllowanceReadRepository(DbContext context) : base(context)
    {
    }
}