﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderTariff.Infrastructure.Entities;

namespace TaxiOrderTariff.Infrastructure.Repositories.Implementations;

public class AllowanceRepository : Repository<Allowance, int>, IRepository<Allowance, int>
{
    public AllowanceRepository(DbContext context) : base(context)
    {
    }
}