﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderTariff.Infrastructure.Entities;

namespace TaxiOrderTariff.Infrastructure.Repositories.Implementations;

public class DriverTariffReadRepository : ReadRepository<DriverTariff, int>, IReadRepository<DriverTariff, int>
{
    public DriverTariffReadRepository(DbContext context) : base(context)
    {
    }
}