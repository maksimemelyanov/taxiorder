﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderTariff.Infrastructure.Entities;

namespace TaxiOrderTariff.Infrastructure.Repositories.Implementations;

public class DriverTariffRepository : Repository<DriverTariff, int>, IRepository<DriverTariff, int>
{
    public DriverTariffRepository(DbContext context) : base(context)
    {
    }
}