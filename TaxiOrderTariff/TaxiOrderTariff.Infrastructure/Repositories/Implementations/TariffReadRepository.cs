﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderTariff.Infrastructure.Entities;

namespace TaxiOrderTariff.Infrastructure.Repositories.Implementations;

public class TariffReadRepository : ReadRepository<Tariff, int>, IReadRepository<Tariff, int>
{
    public TariffReadRepository(DbContext context) : base(context)
    {
    }
}