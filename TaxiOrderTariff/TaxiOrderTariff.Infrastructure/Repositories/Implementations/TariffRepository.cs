﻿using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Infrastructure.Repositories.Implementations;
using TaxiOrderCore.Infrastructure.Repositories.Interfaces;
using TaxiOrderTariff.Infrastructure.Entities;

namespace TaxiOrderTariff.Infrastructure.Repositories.Implementations;

/// <summary>
///     Репозиторий тарифов
/// </summary>
public class TariffRepository : Repository<Tariff, int>, IRepository<Tariff, int>
{
    /// <summary>
    ///     Конструктор
    /// </summary>
    /// <param name="context">Контекст</param>
    public TariffRepository(DbContext context) : base(context)
    {
    }
}