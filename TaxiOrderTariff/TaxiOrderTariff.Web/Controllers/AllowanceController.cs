﻿using AutoMapper;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderTariff.Application.Contracts;
using TaxiOrderTariff.Web.Models;

namespace TaxiOrderTariff.Web.Controllers;

public class AllowanceController : GenericCrudController<AllowanceDto, AllowanceModel, int>
{
    public AllowanceController(ICrudService<AllowanceDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}