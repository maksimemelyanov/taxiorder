﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderTariff.Web.Controllers;

public class AllowanceTypeController : GenericCrudReadController<AllowanceTypeDto, AllowanceTypeModel, int>
{
    public AllowanceTypeController(ICrudReadService<AllowanceTypeDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}