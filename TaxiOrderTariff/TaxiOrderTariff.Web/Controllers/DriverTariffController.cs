﻿using AutoMapper;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderTariff.Application.Contracts;
using TaxiOrderTariff.Web.Models;

namespace TaxiOrderTariff.Web.Controllers;

public class DriverTariffController : GenericCrudController<DriverTariffDto, DriverTariffModel, int>
{
    public DriverTariffController(ICrudService<DriverTariffDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}