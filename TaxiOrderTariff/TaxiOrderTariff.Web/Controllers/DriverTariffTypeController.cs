﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderTariff.Web.Controllers;

public class DriverTariffTypeController : GenericCrudReadController<DriverTariffTypeDto, DriverTariffTypeModel, int>
{
    public DriverTariffTypeController(ICrudReadService<DriverTariffTypeDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}