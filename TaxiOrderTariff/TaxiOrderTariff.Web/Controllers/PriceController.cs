﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using TaxiOrderCore.Web;
using TaxiOrderTariff.Application.Contracts;
using TaxiOrderTariff.Application.Services.CalculationPrice.Interfaces;
using TaxiOrderTariff.Web.Models;

namespace TaxiOrderTariff.Web.Controllers;

[ApiController]
[Route("api/price")]
public class PriceController : ControllerBase
{
    private readonly ICalculationPriceProvider _provider;
    private readonly IMapper _mapper;

    public PriceController(ICalculationPriceProvider provider, IMapper mapper)
    {
        _provider = provider;
        _mapper = mapper;
    }

    [HttpPost("calculation")]
    public CalculationPriceModel Get([FromQuery(Name = ApiConstants.TariffId)][BindRequired] int tariffId,
        [FromQuery(Name = ApiConstants.StartLatitude)][BindRequired] double startLatitude,
        [FromQuery(Name = ApiConstants.StartLongitude)][BindRequired] double startLongitude,
        [FromQuery(Name = ApiConstants.EndLatitude)][BindRequired] double endLatitude,
        [FromQuery(Name = ApiConstants.EndLongitude)][BindRequired] double endLongitude,
        [FromQuery(Name = ApiConstants.DriverTariffId)] int? driverTariffId,
        [FromBody] List<PriceAllowanceModel>? allowances)
    {
        return _mapper.Map<CalculationPriceModel>(_provider.Calculate(tariffId,
            startLatitude, startLongitude, endLatitude, endLongitude, driverTariffId, 
            _mapper.Map<List<PriceAllowanceDto>?>(allowances)));
    }
}