﻿using AutoMapper;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderTariff.Application.Contracts;
using TaxiOrderTariff.Web.Models;

namespace TaxiOrderTariff.Web.Controllers;

public class TariffController : GenericCrudController<TariffDto, TariffModel, int>
{
    public TariffController(ICrudService<TariffDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}