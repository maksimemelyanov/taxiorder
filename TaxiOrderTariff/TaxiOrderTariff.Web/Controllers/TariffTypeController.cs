﻿using AutoMapper;
using TaxiOrderCore.Application.Contracts;
using TaxiOrderCore.Application.Services.Interfaces;
using TaxiOrderCore.Web.Controllers;
using TaxiOrderCore.Web.Models;

namespace TaxiOrderTariff.Web.Controllers;

public class TariffTypeController : GenericCrudReadController<TariffTypeDto, TariffTypeModel, int>
{
    public TariffTypeController(ICrudReadService<TariffTypeDto, int> service, IMapper mapper) : base(service, mapper)
    {
    }
}