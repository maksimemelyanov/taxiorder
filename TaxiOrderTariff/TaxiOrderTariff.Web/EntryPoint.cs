namespace TaxiOrderTariff.Web;

public class EntryPoint
{
    public static async Task Main(string[] args)
    {
        var host = TariffHost.CreateHost(args);
        host.ConfigureHost()
            .ConfigureSwaggerService()
            .ConfigureServices();

        await host.RunHost();
    }
}