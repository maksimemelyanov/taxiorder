﻿using AutoMapper;
using TaxiOrderTariff.Application.Contracts;
using TaxiOrderTariff.Web.Models;

namespace TaxiOrderTariff.Web.Mappings;

public class AllowanceMappingProfile : Profile
{
    public AllowanceMappingProfile()
    {
        CreateMap<AllowanceDto, AllowanceModel>();
        CreateMap<AllowanceModel, AllowanceDto>();
    }
}