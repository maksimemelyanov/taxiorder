﻿using AutoMapper;
using TaxiOrderTariff.Application.Contracts;
using TaxiOrderTariff.Web.Models;

namespace TaxiOrderTariff.Web.Mappings;

public class CalculationPriceMappingProfile : Profile
{
    public CalculationPriceMappingProfile()
    {
        CreateMap<CalculationPriceDto, CalculationPriceModel>();
        CreateMap<CalculationPriceModel, CalculationPriceDto>();
    }
}