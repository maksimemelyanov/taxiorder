﻿using AutoMapper;
using TaxiOrderTariff.Application.Contracts;
using TaxiOrderTariff.Web.Models;

namespace TaxiOrderTariff.Web.Mappings;

public class DriverTariffMappingProfile : Profile
{
    public DriverTariffMappingProfile()
    {
        CreateMap<DriverTariffDto, DriverTariffModel>();
        CreateMap<DriverTariffModel, DriverTariffDto>();
    }
}