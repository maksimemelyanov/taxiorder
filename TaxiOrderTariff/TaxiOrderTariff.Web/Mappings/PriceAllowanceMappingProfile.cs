﻿using AutoMapper;
using TaxiOrderTariff.Application.Contracts;
using TaxiOrderTariff.Web.Models;

namespace TaxiOrderTariff.Web.Mappings;

public class PriceAllowanceMappingProfile : Profile
{
    public PriceAllowanceMappingProfile()
    {
        CreateMap<PriceAllowanceDto, PriceAllowanceModel>();
        CreateMap<PriceAllowanceModel, PriceAllowanceDto>();
    }
}