﻿using AutoMapper;
using TaxiOrderTariff.Application.Contracts;
using TaxiOrderTariff.Web.Models;

namespace TaxiOrderTariff.Web.Mappings;

/// <summary>
///     Профиль маппинга тарифов
/// </summary>
public class TariffMappingProfile : Profile
{
    /// <summary>
    ///     Конструктор
    /// </summary>
    public TariffMappingProfile()
    {
        CreateMap<TariffDto, TariffModel>();
        CreateMap<TariffModel, TariffDto>();
    }
}