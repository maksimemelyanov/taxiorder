﻿namespace TaxiOrderTariff.Web.Models;

public class AllowanceModel
{
    public int? Id { get; set; }

    public string Name { get; set; } = null!;

    public int AllowanceTypeId { get; set; }

    public double Price { get; set; }

    public double TariffId { get; set; }
}