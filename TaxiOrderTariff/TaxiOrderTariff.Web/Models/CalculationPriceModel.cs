﻿namespace TaxiOrderTariff.Web.Models;

public class CalculationPriceModel
{
    public double ClientPrice { get; set; }

    public double DriverPrice { get; set; }
}