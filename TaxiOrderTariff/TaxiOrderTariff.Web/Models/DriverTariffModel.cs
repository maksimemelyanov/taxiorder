﻿namespace TaxiOrderTariff.Web.Models;

public class DriverTariffModel
{
    public int? Id { get; set; }

    public string Name { get; set; } = null!;

    public int BaseId { get; set; }

    public double Percent { get; set; }

    public int DriverTariffId { get; set; }
}