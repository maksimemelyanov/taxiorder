﻿namespace TaxiOrderTariff.Web.Models;

/// <summary>
///     Тариф
/// </summary>
public class TariffModel
{
    /// <summary>
    ///     Идентификатор
    /// </summary>
    public int? Id { get; set; }

    /// <summary>
    ///     Наименование тарифа
    /// </summary>
    public string Name { get; set; } = null!;

    /// <summary>
    ///     Минимальная стоимость
    /// </summary>
    public double Price { get; set; }

    /// <summary>
    ///     Коэффициент
    /// </summary>
    public double Coefficient { get; set; }

    /// <summary>
    ///     Минимальное расстояние
    /// </summary>
    public double MinimalDistance { get; set; }
    /// <summary>
    ///     Идентификатор типа тарифа
    /// </summary>
    public int TariffTypeId { get; set; }

    /// <summary>
    ///     Идентификатор организации
    /// </summary>
    public int BaseId { get; set; }
}