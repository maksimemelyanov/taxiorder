using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TaxiOrderCore.Web.Mapping;
using TaxiOrderTariff.Application;
using TaxiOrderTariff.Application.Services.InitCaches.Implementations;
using TaxiOrderTariff.Infrastructure;
using TaxiOrderTariff.Infrastructure.Context;
using TaxiOrderTariff.Web.Mappings;

namespace TaxiOrderTariff.Web;

public class TariffHost
{
    private const string ConfigurationFileName = "appsettings.json";
    private readonly WebApplicationBuilder _builder;

    private TariffHost(string[] args)
    {
        _builder = WebApplication.CreateBuilder(args);
    }

    public static TariffHost CreateHost(string[] args)
    {
        return new TariffHost(args);
    }

    public TariffHost ConfigureHost()
    {
        _builder.Host
            .ConfigureAppConfiguration(builder =>
            {
                builder.AddJsonFile(ConfigurationFileName);
                builder.AddUserSecrets<TariffHost>(true, true);
            });

        return this;
    }

    public TariffHost ConfigureSwaggerService()
    {
        _builder.Services.AddEndpointsApiExplorer();
        _builder.Services.AddSwaggerGen();
        return this;
    }

    public TariffHost ConfigureServices()
    {
        _builder.Services.AddControllers();
        _builder.Services
            .AddScoped(typeof(DbContext), typeof(DatabaseContext))
            .AddDbContext<DatabaseContext>(x => { x.UseNpgsql(_builder.Configuration.GetConnectionString("db")); })
            .AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()))
            .AddInfrastructure()
            .AddApplication();

        _builder.Services.AddHostedService<InitCachesService>();
        return this;
    }
    
    private MapperConfiguration GetMapperConfiguration()
    {
        var configuration = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<TaxiOrderTariff.Application.Mappings.AllowanceMappingProfile>();
            cfg.AddProfile<AllowanceMappingProfile>();
            cfg.AddProfile<TaxiOrderTariff.Application.Mappings.DriverTariffMappingProfile>();
            cfg.AddProfile<DriverTariffMappingProfile>();
            cfg.AddProfile<TaxiOrderTariff.Application.Mappings.TariffMappingProfile>();
            cfg.AddProfile<TariffMappingProfile>();
            cfg.AddProfile<TaxiOrderCore.Application.Mappings.AllowanceTypeMappingProfile>();
            cfg.AddProfile<AllowanceTypeMappingProfile>();
            cfg.AddProfile<TaxiOrderCore.Application.Mappings.AutoCategoryMappingProfile>();
            cfg.AddProfile<AutoCategoryMappingProfile>();
            cfg.AddProfile<TaxiOrderCore.Application.Mappings.BaseMappingProfile>();
            cfg.AddProfile<BaseMappingProfile>();
            cfg.AddProfile<TaxiOrderCore.Application.Mappings.DriverTariffTypeMappingProfile>();
            cfg.AddProfile<DriverTariffTypeMappingProfile>();
            cfg.AddProfile<TaxiOrderCore.Application.Mappings.RegionMappingProfile>();
            cfg.AddProfile<RegionMappingProfile>();
            cfg.AddProfile<TaxiOrderCore.Application.Mappings.TariffTypeMappingProfile>();
            cfg.AddProfile<TariffTypeMappingProfile>();
            cfg.AddProfile<CalculationPriceMappingProfile>();
            cfg.AddProfile<PriceAllowanceMappingProfile>();
        });
        configuration.AssertConfigurationIsValid();
        return configuration;
    }

    public async Task RunHost()
    {
        var webApplication = _builder.Build();

        var useSwagger = _builder.Configuration.GetValue<bool>("useSwagger");
        if (useSwagger)
            webApplication
                .UseSwagger()
                .UseSwaggerUI();

        webApplication
            .UseRouting()
            .UseHttpsRedirection()
            .UseRequestLocalization()
            .UseSwagger()
            .UseSwaggerUI()
            .UseAuthorization();

        webApplication.UseEndpoints(endpoints =>
        {
            endpoints
                .MapControllers();
        });

        await webApplication.RunAsync();
    }
}